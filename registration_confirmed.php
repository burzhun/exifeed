<?php
session_start();

require_once('Views.php');
require_once('classes/Page.php');
require_once('classes/Resource.php');
require_once('models/user.php');
if(isset($_SESSION['registration_confirmed'])&&$_SESSION['registration_confirmed']==1){
    $form="
        <div id='registration_confirmed'>
            <p>Account has been created.</p>
            <p>Now you can return to the main page.</p>
            <p>Use your credentials to log in.</p>
            <a href='index.php'>Main Page</a>
        </div>
    ";
    $_SESSION['registration_confirmed']=0;
    $header=Views::header_form();
    $page=new Page();
    $page->add_css(array('styles.css',))->add_javascript(array())->set_title('Main Page')->mainbody($form);
    $html=$page->CreatePage();
    echo $html;
}else{
    header("Location:index.php");
}