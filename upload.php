<?php
require_once('models/user.php');
require_once('functions/isMobile.php');
session_start();
//imagejpeg($_FILES['image']['tmp_name']);
//for complex posts
if(isset($_GET['captcha'])){
   // exit;
    ob_start();

    // generate the byte stream
    include("includes/simple-php-captcha/simple-php-captcha.php");
    // and finally retrieve the byte stream
    $rawImageBytes = ob_get_clean();
    echo "<div class='captcha'> <div>Type in symbols on image</div>
                <img  width=200 src='data:image/jpeg;base64," . base64_encode( $rawImageBytes ) . "' />
                <br><input type='text' id='captcha_input'>
        </div>";
    exit;
}

function show($image,$index,$type) {

    // Begin capturing the byte stream
    ob_start();

    // generate the byte stream
    imagejpeg($image, NULL, 100);

    // and finally retrieve the byte stream
    $rawImageBytes = ob_get_clean();
    if($type=='jpeg'||$type=='gif'){
        echo "<p class='image_container'><img  width=300 src='data:image/jpeg;base64," . base64_encode( $rawImageBytes ) . "'  index='{$index}'/></p>
              ";
    }


}
//for long posts
function show2($image,$index,$type) {

    // Begin capturing the byte stream
    ob_start();

    // generate the byte stream
    imagejpeg($image, NULL, 60);
    // and finally retrieve the byte stream
    $rawImageBytes = ob_get_clean();
    if($type=='jpeg'||$type=='gif'){
        echo "<img  width=100% src='data:image/jpeg;base64," . base64_encode( $rawImageBytes ) . "'  index='{$index}'/>";
    }


}
if(isset($_POST['number'])){
    $type="";
    switch(exif_imagetype($_FILES['image']['tmp_name']))
    {
        case IMAGETYPE_JPEG: $image=imagecreatefromjpeg($_FILES['image']['tmp_name']);$type='jpeg'; break;
        case IMAGETYPE_GIF: $image=imagecreatefromgif($_FILES['image']['tmp_name']);$type='gif'; break;
        case IMAGETYPE_PNG: $image=imagecreatefrompng($_FILES['image']['tmp_name']);$type='jpeg'; break;
    }
    if($_POST['number']==4){
        show($image,$_POST['image_index'],$type);
    }
    if($_POST['number']==3){
        show2($image,$_POST['image_index'],$type);
    }
}

if(isset($_GET['giphy'])&&isset($_POST['query'])&&User::isLogin()){
    $offset=isset($_POST['offset']) ? $_POST['offset'] : 0;
    $text=str_replace(' ','+',$_POST['query']);
    $url = "http://api.giphy.com/v1/gifs/search?q={$text}&api_key=dc6zaTOxFJmzC&limit=15&offset=".$offset;
    $data=json_decode(file_get_contents($url))->data;
    $html="";
    $m=isMobile();
    foreach($data as $t){
        $image=$t->images->original_still->url;
        $video = $t->images->original->mp4;
        $width=$t->images->original_still->width;
        $name=$t->slug;
//         echo $image;
//         echo $video;
        //echo $name;
        if($m){
            $html.="<div style='position:relative'><img class='gif_play_button2' src='/images/icons/play_icon.png'><img class='giphy_video_mobile' src='{$image}' width_param='{$width}' width='100%'><button video_src='{$video}' img_src='{$image}' class='giphy_video_add'>Add this image to comment</button></div>";
        }else{
            $html.="<div style='display:inline-block;position:relative;max-height:200px;min-width:100px;'><img class='gif_play_button2' src='/images/icons/play_icon.png'><img class='giphy_video_image' src='{$image}' style='max-width:600px;height:200px' video_src='{$video}'></div>";
          //  $html.="<video  preload='none' loop='none'  webkit-playsinline  class='giphy_video'>
                //        <source img_src='{$image}' width_param='{$width}' src='{$video}' type='video/mp4;' /></video>";
        }
        
    }
    $page=floor($offset/15);
    $client_id='c6d0b68ae9a67b0';
    $client_secret='6ef7fd61da5af922a422863429143b9f2af3e700';
    $c_url = curl_init();
    curl_setopt($c_url, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($c_url, CURLOPT_RETURNTRANSFER, true);
   curl_setopt($c_url, CURLOPT_URL,"https://api.imgur.com/3/gallery/search/score/all/page/{$page}?scrolled&q=lol");
//     curl_setopt($c_url, CURLOPT_URL,"https://api.imgur.com/3/image/h6UcroP");
    curl_setopt($c_url, CURLOPT_HTTPHEADER, array('Authorization: Client-ID ' . $client_id));
    $result=curl_exec($c_url);
    
    $json_array = json_decode($result, true);
    //var_dump($json_array);
      $master = curl_multi_init();
  
    $curl_arr = array();
    $i=0;
    
    foreach($json_array['data'] as $id){
        //print_r($id);
        $curl_arr[$i]=curl_init();
        curl_setopt($curl_arr[$i], CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_arr[$i], CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_arr[$i], CURLOPT_HTTPHEADER, array('Authorization: Client-ID ' . $client_id));
        curl_setopt($curl_arr[$i], CURLOPT_URL,"https://api.imgur.com/3/image/".$id['id']);
        curl_multi_add_handle($master, $curl_arr[$i]);
        $i++;
    }
    if($i>0){
        do {
            curl_multi_exec($master,$running);
        } while($running > 0);

        $t=$i;
        for($i = 0; $i < $t; $i++)
        {
            $results = json_decode(curl_multi_getcontent  ( $curl_arr[$i]  ));
            if($results->success){
    //             print_r($results->data);
                if($results->data->animated){ 
                    $video=$results->data->mp4;
                    $image='http://i.imgur.com/'.$results->data->id.'l.jpeg';
                    $width=$results->data->width;
                    if($m){
                        $html.="<div style='position:relative'><img class='gif_play_button2' src='/images/icons/play_icon.png'><img class='giphy_video_mobile' src='{$image}' width_param='{$width}' width='100%'><button video_src='{$video}' img_src='{$image}' class='giphy_video_add'>Add this image to comment</button></div>";
                    }else{
                        $html.="<div style='display:inline-block;position:relative;max-height:200px;min-width:100px;'><img class='gif_play_button2' src='/images/icons/play_icon.png'><img class='giphy_video_image' src='{$image}' style='max-width:600px;height:200px' video_src='{$video}'></div>";
                      //  $html.="<video  preload='none' loop='none'  webkit-playsinline  class='giphy_video'>
                            //        <source img_src='{$image}' width_param='{$width}' src='{$video}' type='video/mp4;' /></video>";
                    }
//                     echo "<video><source src='{$results->data->mp4}'></video><br>";
                }else{
                     $image='http://i.imgur.com/'.$results->data->id.'l.jpeg';
                    $width=$results->data->width;
//                     echo "<img src='http://i.imgur.com/".$results->data->id."l.jpg'><br>";
                    if($m){
                        $html.="<div ><img class='imgur_mobile' src='{$image}' width_param='{$width}' width='100%'><button video_src='' img_src='{$image}' class='giphy_video_add'>Add this image to comment</button></div>";
                    }else{
                        $html.="<div style='display:inline-block;position:relative;max-height:200px;min-width:100px;'><img class='imgur_image' src='{$image}' video_src='' style='max-width:600px;height:200px'></div>";
                      //  $html.="<video  preload='none' loop='none'  webkit-playsinline  class='giphy_video'>
                            //        <source img_src='{$image}' width_param='{$width}' src='{$video}' type='video/mp4;' /></video>";
                    }
                }

            }
        }
    }
    echo $html;
    exit;
    
    curl_close($c_url);
// set_time_limit(0); 
// $file = file_get_contents('http://i.imgur.com/h6UcroP.mp4');
// file_put_contents('videos/1.mp4', $file);


}

/*

<img src='http://i.imgur.com/h6UcroPl.jpeg'>
<div>
    <blockquote class="imgur-embed-pub" lang="en" data-id="4ZDD4FG">
    <a href="//imgur.com/4ZDD4FG">I tried to add script in post on some website</a>
</blockquote>
<script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>
    
</div>
<div>
    
<blockquote class="imgur-embed-pub" lang="en" data-id="h6UcroP">
    <a href="//imgur.com/h6UcroP">I tried to add script in post on some website</a>
</blockquote>
<script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>
</div>
*/