<?php
if ($status==0) {
?>

<div class="tags_view">
    <span class="title">Posts with tag <span class="tag">#<?=urldecode($tag);?></span></span><br>
    <button class="subscribe_tag" tag="<? printtext($tag);?>">Subscribe</button>
    <button class="block_tag" tag="<? printtext($tag);?>">Do not show me posts with this tag</button>
</div>
<?php } ?>

<?php if($status==1) { ?>
    <div class="tags_view">
        <span class="title">Posts with tag  <span class="tag">#<?=urldecode($tag);?></span>. You subscribed to this tag.</span><br>
        <button class="unsubscribe_tag" tag="<? printtext($tag);?>">Subscribe</button>
        <button class="block_tag" tag="<? printtext($tag);?>">Do not show me posts with this tag</button>
    </div>
<?php } ?>

<?php if($status==-1) { ?>
    <div class="tags_view">
        <span class="title">Posts with tag <span class="tag">#<?=urldecode($tag);?></span>. You blocked posts with this block.</span><br>
        <button class="subscribe_tag" tag="<? printtext($tag);?>">Subscribe</button>
        <button class="unblock_tag" tag="<? printtext($tag);?>">Do not show me posts with this tag</button>
    </div>
<?php } ?>