<?php
if($user->image!=''){
    if($user->provider==''){
        $image="/images/user_images/".$user->image;
    }
    else{
        $image=$user->image;
    }
}
else{
    $image="/images/icons/no_image_user.png";
}

?>
<div class="user_view" >
    <div class="left_column">
        <img src="<?php echo $image; ?>" >
        <?php if (User::isLogin()&&$user->id!=$_SESSION['user']['id']){ ?>
            <?php if(!$isSubscribed){?>
                <span class="subscribe_button" source_id='<?=$user->id; ?>'>Subscribe</span>
            <?php }else{?>
                <span class="unsubscribe_button" source_id='<?=$user->id; ?>'>Unsubscribe</span>
            <?php }?>
        <?php }?>
    </div>


    <div class="user_info">
        <span class="username"><?=$user->username;?></span>
        <span><?=$user->Get_Subscribers_Count();?> subscribers</span>
        <span><?=$user->Get_Subscriptions_Count();?> subscriptions</span>
        <span class="post_count"><?=$user->PostCount();?> posts</span>
        <span>Registered <?=substr($user->date,0,10);?></span>
    </div>
    <?if($user->id==User::id()){?>
        <br><a style="text-decoration: none;color: black;background-color: white;border: 1px solid grey;padding: 5px;" href='/settings'>Settings</a>
    <?}?>
</div>