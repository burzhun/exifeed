<?php
$path=Router::pathtofolder();
$html="<div class='post'>";
$div_rating="";
$isSaved="";
if($user_id!=''){
    $vote="";
    $text="Upvote";
    switch($post->user_rating) {
        case 1:
            $vote="upvoted";
            $text="Upvoted";
            break;
        case -1:
            $vote="downvoted";
            $text="Downvoted";
            break;
    }
    if(Post::isSaved($user_id,$post->id)){
        $isSaved="saved";
    }
}

$t=0;
  if($t){?>
    <style type="text/css">
        img{display: none !important;}
        .post_complex_text  img{
            display: block !important;
        }
    </style>
<?}?>
<div class="post" id="post<?=$post->id?>">
    <div class="post_data">
        <div class='title'>
            <a href='/post/id<?php echo $post->id; ?>' target="_blank" >
                <h1 style="display: inline-block;"><?php printtext($post->title); ?></h1>
            </a>
            <?if(User::id()==$post->user_id){?>
            <a href='/post/update/id<?php echo $post->id; ?>' target="_blank">
            <img src="/images/icons/text-edit.png" width="25px">
            </a>
            <?}?>
            <?if(User::isAdmin()){?>
                <img src='/images/icons/delete.png' width="20px" class='post_delete' post_id=<?=$post->id?>>
            <?}?>
        </div>
        <div class="post_info" post_id="<?=$post->id;?>">
            <img src="<?php echo $post->GetUserImageLink(); ?>" >
            <a href='/user/id<?php echo $post->user_id; ?>' class="author">
                <?php printtext($post->author_username); ?>
            </a><br>
            <span class="datediff"><?php echo $post->elapsed_time;?></span>
            <?php if($user_id){ ?>
                <span class="save_button <?=$isSaved;?>"></span>
            <?php }?>
        </div>
        
        <div class="post_container" post_id="<?=$post->id;?>">
        <div class="tags">
            <?php foreach($post->tags as $tag){?>
                <?php if($tag!=''){?><a href='/tags/<? urlencode(printtext($tag));?>'><? printtext($tag);?></a><?php }?>
            <?php }?>
        </div>
            <div class='description'><? printtext($post->description); ?></div>
            
            <? switch($post->type){
                 case 'image':?>
                     <?if(preg_match('/([0-9]+).gif/',$post->image)){?>
                         <div class='post_image '>
                             <img class="gif_play_button" src="/images/icons/play_icon.png" gif_src="/images/post/<?=$post->image; ?>">
                             <a href='/post/id<?=$post->id; ?>'><img class="gif_image" src='/images/post/<?=$post->image; ?>.jpg'></a>
                         </div>
                     <?}else{?>
                    <div class='post_image '><a href='/post/id<?=$post->id; ?>'><img src='/images/post/<?=$post->image; ?>'></a></div>
                    <?}?>
                <? break;
                 case 'text':?>
                    <div class='post_text '><?php printtext_linebreak($post->text); ?></div>
                <? break;
                 case 'video':?>
                     <div class='post_video '><?=$post->text;?></div>
                 <? break;
                case 'complex':?>
                    <div class='post_complex_text '><?=stripslashes($post->text); ?></div>
                <? break;?>
            <?}?>            
            <?//$post->LoadAddition();?>
            <? if(!$separately){?>
                <div class='show_full_button'>
                    Show entire post
                </div>                
            <?}?>
        </div>
        <?php if($user_id){ ?>
            <div class="post_rating <?=$vote;?>" >
                 <span class="upvote" post_id="<?php echo $post->id;?>">
                    <span class='rating'><?php echo $post->rating;?></span>
                    <span class='text'><?=$text;?></span>
                 </span>
                <?/*<span class='downvote' post_id='<?=$post->id;?>'>Downvote</span>*/?>
            </div>
        <?php }?>
        <?php if($user_id&&!isMobile()&&$post->user_id!=$user_id){ ?>
            <div class="repost_button" post_id="<?=$post->id;?>">
                <img src="../images/icons/repost-icon.jpg">
                <span  >Repost it</span>
            </div>
        <?php }?>
        <? if(!$separately){?>
            <div class='post_comments'>
                <a href='/post/id<?=$post->id; ?>#comments' target="_blank"><?=$post->comments_number; ?><img class='comment_number_image' width="20px" src="../images/icons/comments.png"></a>
            </div>
        <?}?>
        <? if(!isMobile()){?>
            <div class="share_buttons_list">
                <div class="share_facebook_button <?=$post->type=='video' ? 'video' : ''?>" image_url="<?=$post->image;?>" title="<?php printtext($post->title); ?>" desc="<? printtext($post->description);?>"
                     post_id="<?=$post->id;?>">
                    <img class="share_button_image" src="/images/icons/facebook-icon2.png">
                </div>
                <a class="share_twitter_button" title="<?php printtext($post->title); ?>"
                   href="http://exifeed.com/post/id<?=$post->id;?>" target="_blank">
                    <img class="share_button_image" src="http://exifeed.com/images/icons/twitter-icon2.png" style="top: -2px;">
                </a>
                <?if($separately){?>
                  <su:badge layout="5"></su:badge>
                <?}?>
            </div>
        <?} else{?>
            <div class="mobile_share">
                <img class='mobile_share_image' src="../images/icons/share_button.png">
                <span>Share</span>
            </div>
            <div class="share_buttons_list_mobile">

                <div  class="share_button_mobile copy_link" data-link="http://exifeed.com/post/id<?=$post->id;?>">
                    <img src="/images/icons/link.png">   
                    <input type="text" value="" style="display: none;vertical-align: top;height: 50px;position: relative;top:-60px;font-size: 21px;width:70%;left:55px">
                    <span style="">Copy link</span>
                </div>
            <? if($user_id&&$post->user_id!=$user_id){?>
                
                <div class="repost_button share_button_mobile" post_id="<?=$post->id;?>">
                    <img src="../images/icons/repost-icon.jpg">
                    <span  >Repost it</span>
                </div>
            <? }?>
            <div class="share_facebook_button share_button_mobile" image_url="<?=$post->image;?>" title="<?php printtext($post->title); ?>" desc="<? printtext($post->description);?>"
                 post_id="<?=$post->id;?>">
                <img src="../images/icons/facebook-icon.png">
                <span>Facebook</span>
            </div>
            <a class="share_twitter_button share_button_mobile" title="<?php printtext($post->title); ?>"
               href="http://exifeed.com/post/id<?=$post->id;?>" target="_blank">
                <img src="../images/icons/twitter-icon.png" style="top: -2px;">
                <span>Tweet this!</span>
            </a>
            <a class="share_button_mobile" href="whatsapp://send?text=<?php printtext($post->title); ?>http://exifeed.com/post/id<?=$post->id;?>" data-action="share/whatsapp/share">
                <img src="../images/icons/whatsapp_icon.png">
                <span>Whatsapp</span>
            </a>
            

            
        </div>
        <?}?>
    </div>
</div>