<?php
$html="<div class='post'>";
$div_rating="";
$isSaved="";
if($user_id!=''){
    $vote="";
    $text="Upvote";
    switch($post->user_rating) {
        case 1:
            $vote="upvoted";
            $text="Upvoted";
            break;
        case -1:
            $vote="downvoted";
            $text="Downvoted";
            break;
    }
    if(Post::isSaved($user_id,$post->id)){
        $isSaved="saved";
    }
}
?>
<div class="mini_post">
	<div class="mini_post_data">
		<? switch($post->type){
                 case 'image':?>
                     <?if(preg_match('/([0-9]+).gif/',$post->image)){?>
                         <div class='post_image '>
                             <img class="gif_play_button" src="/images/icons/play_icon.png" gif_src="/images/post/<?=$post->image; ?>">
                             <a href='/post/id<?=$post->id; ?>'><img class="gif_image" src='/images/post/<?=$post->image; ?>.jpg'></a>
                         </div>
                     <?}else{?>
                    <div class='post_image '><a href='/post/id<?=$post->id; ?>'><img src='/images/post/<?=$post->image; ?>'></a></div>
                    <?}?>
                <? break;
                 case 'text':?>
                    <div class='post_text '><?php printtext_linebreak($post->text); ?></div>
                <? break;
                 case 'video':?>
                     <div class='post_video '><?=$post->text;?></div>
                 <? break;
                case 'complex':?>
                    <div class='post_complex_text '><?=stripslashes($post->text); ?></div>
                <? break;?>
            <?}?>
	</div>
	<div class="mini_post_info">
		<div class="mini_post_title"><?=$post->title?></div>
		<div class="minin_post_buttons">
			<div class="mini_post_rating <?=$vote;?>" >
                 <span class="upvote" post_id="<?php echo $post->id;?>">
                    <span class='rating'><?php echo $post->user_rating;?></span>
                    <span class='text'><?=$text;?></span>
                 </span>
            </div>
		</div>
	</div>
</div>