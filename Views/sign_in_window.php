<?php
$prev_page="";
if(isset($_SESSION['previous_page'])){
    $prev_page="&prev_page=".$_SESSION['previous_page'];
}
?>
<div class="sign_in_window">
    <div id="form_container">
        <div class="sign_in_form">
            <div class="register_title">Register with email</div>
            <form action='' method='post'>
                <input type='text' class="name" name='name' placeholder='Full name'> <br>
                <input type='email' class="email" name='email' placeholder='E-mail'><br>
                <input type='password' class="password"  name='password' placeholder='Password'> <br>
                <input type='password' class="password2" name='password' placeholder='Confirm password'> <br>
                <p class="error" ></p><br>
                <span class="button">Create an account</span>
            </form>
        </div>
        <div id='login_form'>
            <div class="login_title">Log in or use existing account</div>
            <form action='' method='post'>
                <input type='text' class='text' name='email' placeholder='E-Mail '><br>
                <input type='password' class='password' name='password' placeholder='password'>
                <p class='error' >Your login or password is incorrect</p>
                <span class='button'>Ok</span>
            </form>
            <div class="social_buttons">
                <a class="facebook_link" href ="/social_login.php?login=Facebook<?=$prev_page;?>"><img src="/images/icons/facebook.png">Sign up with Facebook</a>
                <a class="twitter_link" href ="/social_login.php?login=Twitter<?=$prev_page;?>"><img src="/images/icons/twitter.png">Sign up with Twitter</a>
            </div>
        </div>
        <a class="close_button"></a>
    </div>
</div>
