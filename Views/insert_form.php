<?php
?>
<div id="upload_post_loader_layer">
</div>
<div id="upload_post_loader">
  <img src="/images/icons/loading.gif" />
  <span>Loading</span>
</div>

<div id='insert_form'>
    <div id='post_type'>
        <div class="post_type_container">
            <?/*<span class='post_type_item post_type_item_selected' >Text</span>
            <span class='post_type_item' >Image</span>
            <span class='post_type_item' >Video</span>*/?>
            <span class='post_type_item post_type_item_selected' >Composite</span>
            <span class='post_type_item complex_item_link' >Custom</span>
        </div>
    </div>
    <?/*<div class='insert_form_item' id='text_form'>
        <form action='upload_post.php?type=text' method='post' data-ajax="false">
            <div id='title_confirmed'  class="error_message"></div>
            <input type='text' name='title' placeholder='Title' class='Title' >
            <span id='text_confirmed'  class="error_message"></span>
            <textarea name='Text' placeholder='Text' class='Text' style='height: 250px;'></textarea><br>
            <p><span id='tags_confirmed' style='color: red'></span></p>
            <input type='text' name='tags' placeholder='tags' class='tags'><br>
            <button type='submit' onclick='return CheckTextForm()'>Save</button>
        </form>
    </div>
    <div class='insert_form_item' id="image_form"  style='display:none;'>
        <form action='upload_post.php?type=image' method='post' enctype='multipart/form-data' data-ajax="false">
            <div id='title_confirmed'  class="error_message"></div>
            <input type='text' name='title' placeholder='Title' class="image_title">
            <textarea name='description' placeholder='Description' class="image_description"></textarea><br>
            <span id='file_confirmed'  class="error_message"></span>
            <input type='file' class="image_file" name='image' accept="image/*">
            <input type='text' name='tags' placeholder='tags' class="image_tags"><br>
            <button type='submit' onclick='return CheckImageForm()'>Save</button>
        </form>
    </div>
    <div class='insert_form_item' id="video_form" style="display: none;">
        <form action="upload_post.php?type=link" method="post" data-ajax="false">
            <div style="">You can post video from youtube or vimeo</div>
            <div id='title_confirmed'  class="error_message"></div>
            <input type='text' name='title' placeholder='Title' class="video_title"><br>
            <textarea name='Description' placeholder='Description' height='100px' class='video_title'></textarea><br>
            <span id='link_confirmed'  class="error_message"></span>
            <input type="text" name="url" class="video_link" placeholder="Video url"><br>            
            <input type='text' name='tags' placeholder='tags' class="video_tags"><br>
            <button type="submit" onclick='return CheckLinkForm()'>Save</button>
        </form>
    </div>*/?>
    <div class="insert_form_item" id="long_post" style="font-family:'Proxima Nova Regular',Arial,sans-serif">
        <form action="/upload_post.php?type=longpost" method="post" enctype="multipart/form-data" data-ajax="false">
            <div id='title_confirmed'  class="error_message"></div>
            <input type="hidden" name="parent_id" value="<?=isset($_GET['parent_id']) ? $_GET['parent_id']:0;?>"></input>
            <input type='text' name='title' placeholder='Title' class='Title' >
            <input type="text" name="description" placeholder="Description">
            <div style="height:0;border-bottom:1px solid rgb(221, 221, 221);"></div>
            <div id="long_post_empty">You haven't added anything yet</div>
            <div id="long_post_data"><?=$post_html;?></div>
            <div class="loading" style="display: none;text-align: center"><img src="../images/icons/loading.gif" width="64"></div>
            <div id='data_confirmed'  class="error_message"></div>
            <div id="long_post_text_form">
                <div id="long_post_text" contenteditable="true"></div>
                <button type="button" id="add_text_long_post">OK</button>
            </div>
            <div id="long_post_video_form">
                <div style="font-size: 18px; margin-bottom: 5px; color: rgb(36, 36, 36);">Upload video</div>
                <input type="file" accept="video/*" id="upload_video_input" style="display:none;">
                <button type="button" id="upload_video_long_post">OK</button><br><br>
                <div style="font-size: 18px; margin-bottom: 5px; color: rgb(36, 36, 36);">or insert  url link from YouTube, Vimeo or Coub</div>
                <input id="long_post_video" type="text">
                <button type="button" id="add_video_long_post">OK</button>
            </div>
            <div id="long_post_buttons">
                <div class="long_post_button add_text_button">Add text</div>
                <div class="long_post_button add_image_button">Add image</div>
                <div class="long_post_button add_video_button">Add video</div>
            </div>
            <input type='text' name='tags' placeholder='tags' class="video_tags"><br>
            <textarea name="long_post_data" id="long_post_data_field" style="display: none;"></textarea>
            <button type="submit" >Save</button>
        </form>
        
    </div>
    <div class="insert_form_item" id="complex_form" style="display: none;">
        <form action="upload_post.php?type=complex" method="post" enctype="multipart/form-data" data-ajax="false">
            <input type='text' name='title' placeholder='Title' class='complex_form_title' style='margin-bottom: 1px;'>
            <div id='title_confirmed'  class="error_message"></div><br>
            <input type="text" name="description" placeholder="Description">
            <div style="height:0;border-bottom:1px solid rgb(221, 221, 221);"></div>
            <div id="text_change_buttons">
                <button type="button" class="text_transform_buttons" id="bold_text_button" class_value="bold">B</button>
                <button type="button" class="text_transform_buttons" id="italic_text_button" class_value="italic">I</button>
                <button type="button" class="text_transform_buttons image_transform_button" id="left_text_button" class_value="left"></button>
                <button type="button" class="text_transform_buttons " id="center_text_button" class_value="center"></button>
                <button type="button" class="text_transform_buttons image_transform_button" id="right_text_button" class_value="right"></button>
                <button type="button"  id="add_image_button">Add Image</button>
                <button type="button"  id="add_link_button">Add Link</button>
            </div>
            <div contenteditable="true" id="complex_text"></div>
            <span id='complex_text_confirmed'  class="error_message"></span>
            <span class="sel_text">
                    <span class="center_text_button">Center</span>
                    <span class="bold_text_button">Bold</span>
                </span>
            <textarea name="complex_text" id="complex_text_area" style="display: none;"></textarea><br>
            <input type='text' name='tags' placeholder='tags' class="complex_form_tags"><br>
            <button type="submit" onclick="return CheckComplexForm()">Save</button>
        </form>
        <div id="add_link_window">
            <span>Insert the title and url of your link</span>
            <input type="text" placeholder="Link title" id="link_title"><br>
            <input type="text" placeholder="Link url" id="link_url"><br>
            <button type="button" id="add_link">Ok</button>
        </div>
    </div>
    <div class="insert_form_item" style="display: none">
        <form data-ajax="false">
            <textarea name="editor1" id="editor1" rows="10" cols="80">
                This is my textarea to be replaced with CKEditor.
            </textarea>
            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                //CKEDITOR.replace( 'editor1' );
            </script>
        </form>
    </div>
</div>

