<?php
if($user->image!=''){
    if($user->provider==''){
        $image="/images/user_images/".$user->image;
    }
    else{
        $image=$user->image;
    }
}
else{
    $image="";
}
?>
<span id="settings_header">My settings</span>
<div id="settings">
    <div id="user_image_form">
        <?php if($user->provider==''){ ?>
            <form action="" method="post" enctype="multipart/form-data" >
                <?php if($image==''){?>
                    <img src="/images/icons/no_image_user.png" width="200px">
                <?php } else{?>
                    <img src="<?php echo $image; ?>?p=12" width="200px">
                <?php }?>
                <input type="file" name="user_image" style="display: none" accept="image/*" >
                <button type="submit" style="display: none">Ok</button>
                <?if($user->image==''){?>
                    <button type="button" id="change_image_button">Add image</button>
                <?}else{?>
                    <button type="button" id="change_image_button">Change image</button>
                    <button type="button" id="delete_image_button">Delete image</button>
                <?php }?>
            </form>
        <?php }else{?>
            <img src="<?php echo $image; ?>" width="150px">
        <?php }?>
    </div>
    <? $user->reload_user_data();?>
    <div id="user_data_form">
        <form method="post" data-ajax="false">
            <div class="username">
                <span>Username:</span>
                <input type="text" name="username" value="<?=$user->username;?>" />
                <div class="error"></div>
            </div>

            <div class="user_settings">
                <span class="label">Shorten long posts</span>

                <div class="custom_checkbox <?=$user->shorten_longpost ? 'yes' : 'no';?>">
                    <input name="shorten_longpost" type="hidden" class="checkbox_val" value="<?=$user->shorten_longpost ? '1' : '0';?>">
                    <span class="circle"></span>
                </div>
            </div>
            <button type="submit">Save</button>
        </form>


    </div>


    <?php if($user->provider==''){ ?>
    <div id="change_password">
        <span class="head">Change Password</span>
        <p><span class="label">Old password</span><input type="text" name="old_password" class="old_password"></p>
        <p><span class="label">New password</span><input type="text" name="new_password" class="new_password"></p>
        <p><span class="label">Confirm new password</span><input type="text" name="new_password2" class="new_password2"></p>
        <span class="error"></span><span class="success"></span>
        <span id="save_password_button" email="<?php echo $email ?>">Save password</span>
    </div>
    <?php }?>
</div>