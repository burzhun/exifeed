<?php
$path=Router::pathtofolder();
$html="<div class='post'>";
$div_rating="";
$isSaved="";
if($user_id!=''){
    $vote="";
    $text="Upvote";
    switch($post->user_rating) {
        case 1:
            $vote="upvoted";
            $text="Upvoted";
            break;
        case -1:
            $vote="downvoted";
            $text="Downvoted";
            break;
    }
    if(Post::isSaved($user_id,$post->id)){
        $isSaved="saved";
    }
}
$image="/images/icons/no_image_user.png";
if($post->author_image!=''){
    if($post->author_provider==''){
        $image="/images/user_images/".$post->author_image;
    }
    else{
        $image=$post->author_image;
    }
}
?>
<link rel='stylesheet' type='text/css' href='/css<?=isMobile() ? '/mobile': ''?>/post.css'/>
<div class="post_info" post_id="<?=$post->id;?>">
            <img src="<?php echo $image; ?>" >
            <a href='/user/id<?php echo $post->user_id; ?>' target='_blank' class="author">
                <?php printtext($post->author_username); ?>
            </a><br>
            <span class="datediff"><?php echo $post->elapsed_time;?></span>
        </div>
<div class='post' it="<?=$post->id;?>">
  <? switch($post->type){
                 case 'image':?>
                     <?if(preg_match('/([0-9]+).gif/',$post->image)){?>
                         <div class='post_image '>
                             <img class="gif_play_button" src="/images/icons/play_icon.png" gif_src="/images/post/<?=$post->image; ?>">
                             <a href='<?=$path; ?>post/id<?=$post->id; ?>'><img class="gif_image" src='/images/post/<?=$post->image; ?>.jpg'></a>
                         </div>
                     <?}else{?>
                    <div class='post_image '><a href='<?=$path; ?>post/id<?=$post->id; ?>'><img src='/images/post/<?=$post->image; ?>'></a></div>
                    <?}?>
                <? break;
                 case 'text':?>
                    <div class='post_text '><?php printtext_linebreak($post->text); ?></div>
                <? break;
                 case 'video':?>
                     <div class='post_video '><?=$post->text;?></div>
                 <? break;
                case 'complex':?>
                    <div class='post_complex_text '><?=stripslashes($post->text); ?></div>
                <? break;?>
            <?}?>
    <div class='show_full_button'>
                    <a style='text-decoration:none;color:white;' target='_blank' href='http://www.exifeed.com/post/id<?=$post->id;?>'>Show entire post</a>
                </div>  
</div>
<style>
    .post .show_full_button {
        position: absolute;
        top: 563px !important;
        display: block;
        width: 100%;
        left: 0;
        text-align: center;
        font-size: 24px;
        background-color: rgb(58, 111, 171);
        color: white;
        cursor: pointer;
        padding: 5px 0px;
    }
    .post{
        max-width: 600px !important;
        max-height: 600px;
        position: relative;
        display: block;
        overflow: hidden;
    }
</style>