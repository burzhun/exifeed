<?php
?>

<a class='menu'  href='/'><span>Home</span></a>
<a class='menu'  href='/best'><span>Best</span></a>
<a class='menu'  href='/new'><span>New</span></a>
<a class='menu'  href='/feed'><span>My feed</span></a>
<a class='menu'  href='/comments'><span>My comments</span></a>
<a class='menu'  href='insert.php'"><span>Add post</span></a>
<?php if($isLogged){ ?>
    <a class="menu mobile_menu" href="/settings">Settings</a>
    <a class='menu mobile_menu' href='logout.php'><span>Logout</span></a>
<?php }  ?>
<?php if(!$isLogged) { ?>
    <span class='menu mobile_menu login_mobile'>Log in</span>
    <span class='menu mobile_menu registration_mobile'>Registration</span>
<?php  } ?>