<div id="comments">
    <? foreach ($list as $comment) { ?>

        <?if($comment->parent_comment_id){?>
        <div class="comment has_parent" id="comment<?=$comment->id;?>" style="">
         <? $p_com=$list[$comment->parent_comment_id];?>
         <div class="parent_comment">
             <a class="parent_comment_info" href="/user/id<?=$p_com->user_id;?>">
                 <img class='parent_comment_user_image' src='<?=User::getCommentImage($p_com->user_id);?>'>
                 <span><?=$p_com->username;?></span>
             </a>
             <div class="parent_comment_container" is_opened="0">
                 <div class="parent_comment_text"><?=$p_com->text;?></div>
                 <?if($p_com->image_url!=''){?>
                     <? if($p_com->video_url!=''){ ?>
                        <img class='gif_play_button' src='../images/icons/play_icon.png' video_src='<?=$p_com->video_url;?>' gif_src='<?=$p_com->video_url;?>'>
                         <img class="parent_comment_image gif_image" src="<?=$p_com->image_url;?>">
                     <?}else{?>
                         <img class="parent_comment_image" src="<?=$p_com->image_url;?>">
                     <?}?>
                 <?}?>
             </div> 
         </div>
        
        <?}else{?>
        <div class="comment" id="comment<?=$comment->id;?>">
        <?}?>
            <a href="/user/id<?=$comment->user_id;?>" class="comment_user_image">
                <img src='<?=User::getCommentImage($comment->user_id);?>'>
            </a>
            <div class="comment_container">
                <a href="/user/id<?=$comment->user_id;?>" class="comment_author">
                    <span><?=$comment->username;?></span>
                </a>
                <span class='comment_add_date'><?=$comment->ElapsedTime();?></span><br>
                <div class="comment_text"><?=$comment->text;?></div>
                <?if($comment->image_url!=''){?>
                <div class="comment_image_container">
                    <? if($comment->video_url!=''){ ?>
                        <img class='gif_play_button' src='../images/icons/play_icon.png' video_src='<?=$comment->video_url;?>' gif_src='<?=$comment->video_url;?>'>
                        <img class="comment_image gif_image" src="<?=$comment->image_url;?>">
                    <?}else{?>
                        <img class="comment_image" src="<?=$comment->image_url;?>">
                    <?}?>
                </div>
                <?}?>
                <?if($user_id){?>
                    <div class="comment_buttons" >
                        <a style="display: none;" class="upvote" comment_id="<?=$comment->id;?>" href="#">Upvote</a>
                        <span class="comment_respond" is_opened="0" comment_id="<?=$comment->id;?>" post_id="<?=$comment->post_id;?>" level="<?=$comment->level;?>">Add comment</span>
                    </div>
                <?}?>
            </div>
            
        </div>
    <?}?>
</div>