<?php
function registration_mobile_form(){
    $html=<<<eof
    <div id="registration_mobile_form">
        <form action="" method="post" data-ajax="false">
            <span class="label">Full name</span>
            <div class="input">
                <input class="text username" type="text" name="username">
            </div>
            <span id="username_error" class='error'></span>
            <span class="label">Email</span>
            <div class="input">
                <input class="text email" type="text" name="email">
            </div>
            <span id="email_error" class='error'></span>
            <span class="label">Password</span>
            <div class="input">
                <input class="password" type="password" name="password">
            </div>
            <span class="label">Confirm password</span>
            <div class="input" >
                <input class="password password2" type="password" name="password">
            </div>
            <span id="password_error" class='error'></span>
            <div class="button">
                <span>Create account</span>
            </div>
            <span class="error">Wrong email or name or  password</span>
        <span class="back">Back</span>
        </form>

    </div>
eof;
    return $html;
}