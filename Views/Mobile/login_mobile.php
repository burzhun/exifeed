<?php
function login_mobile_form(){
    $html=<<<eof
    <div id="login_mobile_form">
        <form action="" method="post" data-ajax="false">
            <span class="label">Username or email</span>
            <div class="input">
                <input class="text" type="text" name="email">
            </div>
            <span class="label">Password</span>
            <div class="input">
                <input class="password" type="password" name="password">
            </div>
            <div class="button">
                <span>Log in</span>
            </div>
            <span class="error">Wrong email or name or  password</span>
        <span class="back">Back</span>
        </form>

    </div>
eof;
    return $html;
}