<?php

?>
	<div id="about_div" style="font-size: 17px;font-family: Tahoma;padding:0px 20px;">
		<p><a href='http://exifeed.com' style="color: rgba(11, 93, 200, 0.94); font-weight: bold; text-decoration: none;">Exifeed.com</a> is a place, where anyone can share some interesting article, image, video with other people and comment it.</p>
		<p>
			If you have any suggestions,questions you can always email me <a href="mailto:exifeed@gmail.com">exifeed@gmail.com</a> or use this form below
		</p>
		<p id="about_form">
			<input type="text" placeholder="Type your questions or suggestions here" style="height: 30px;width: 400px;">
			<button style="height: 34px;border: none;color: white; width: 77px;font-size: 16px;padding-top: 0px;background-color: #1BA00A;">Send</button>
		</p>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#about_div button").click(function() {
				var text = $("#about_div input").val();
				if (text != '') {
					$.post('/ajax_submit.php?suggestion=1', {
						suggestion: text
					}, function(data) {
						if (data == 'ok') {
							$("#about_div #about_form").html('Thanks for your contribution');
						}
					});
				}
			});
		});
	</script>
	<style>
		#about_form {
			color: #4ABD05;
			font-weight: bold;
			font-size: 20px;
		}
	</style>