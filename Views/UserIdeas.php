<?php
?>
<div id="user_ideas">
    <div class="title">
        <span>Add your idea how to make this site better, or upvote the existing idea, if you think that it is good</span>
    </div>
    <?php if(User::isLogin()){?>
    <div id="idea_add_form">
        <textarea></textarea>
        <button>Add</button>
    </div>
    <?}?>
    <? while($row=$res->fetch_assoc()){?>
        <div class="user_idea">
            <span><?=htmlentities($row['idea']);?></span>
            <button class="idea_upvote" idea_id="<?=$row['id'];?>">+<?=$row['rating'];?></button>
        </div>
    <?}?>
</div>