<?php
$path=Router::pathtofolder();
$html="<div class='post'>";
$div_rating="";
if($user_id!=''){
    $vote="";
    $text="Upvote";
    switch($post->user_rating) {
        case 1:
            $vote="upvoted";
            $text="Upvoted";
            break;
        case -1:
            $vote="downvoted";
            $text="Downvoted";
            break;
    }
    if(Post::isSaved($user_id,$post->id)){
        $isSaved="saved";
    }
}
$image="/images/icons/no_image_user.png";
if($post->author_image!=''){
    if($post->author_provider==''){
        $image="/images/user_images/".$post->author_image;
    }
    else{
        $image=$post->author_image;
    }
}
?>
<div class="post">
    <div class="post_data">
        <?php if ($separately){ ?>
            <div class='title'>
                <a href=''>
                    <span ><?php printtext($post->title); ?></span>
                </a>
            </div>
            <div class="post_info">
                <img src="<?php echo $image; ?>" >
                <a href='<?php echo $path; ?>user/id<?php echo $post->user_id; ?>' class="author">
                    <?php printtext($post->author_username); ?>
                </a>
                <span class="datediff"><?php echo $post->elapsed_time;?></span>
                <?php if($user_id){ ?>
                    <span class="save_button <?=$isSaved;?>"></span>
                <?php }?>
            </div>
            <div class="tags">
                <?php foreach($post->tags as $tag){?>
                    <?php if($tag!=''){?><a href='/tags/<?=$tag;?>'>#<?=$tag;?></a><?php }?>
                <?php }?>
            </div>
            <div class='post_complex_text'><?=$post->text;?></div>
            <div class="share_buttons">
                <span class="share_button" clicked="0">Share</span>
                <div class="share_buttons_list">

                    <div class="share_facebook_button" image_url="" title="<?php printtext($post->title); ?>" desc="<? printtext($post->text);?>"
                         post_id="<?=$post->id;?>">
                        <img src="../images/icons/facebook-icon.png">
                        <span>Facebook</span>
                    </div><br>
                    <a class="share_twitter_button" title="<?php printtext($post->title); ?>"
                       href="http://exifeed.com/post/id<?=$post->id;?>" target="_blank">
                        <img src="../images/icons/twitter-icon.png" style="top: -2px;">
                        <span>Tweet this!</span>
                    </a><br>
                    <?php if($user_id){ ?>
                        <div class="repost_button" post_id="<?=$post->id;?>">
                            <img src="../images/icons/repost-icon.jpg">
                            <span  >Repost it</span>
                        </div>
                    <?php }?>
                </div>
            </div>
        <?php } else{ ?>
            <div class='title'>
                <a href='<?php echo $path; ?>post/id<?php echo $post->id; ?>' target="_blank">
                    <span ><?php printtext($post->title); ?></span>
                </a>
            </div>
            <div class="post_info">
                <img src="<?php echo $image; ?>" >
                <a href='<?php echo $path; ?>user/id<?php echo $post->user_id; ?>' class="author">
                    <?php printtext($post->get_user_name()); ?>
                </a>
                <span class="datediff"><?php echo $post->elapsed_time;?></span>
                <?php if($user_id){ ?>
                    <span class="save_button"></span>
                <?php }?>
            </div>
            <div class="tags">
                <?php foreach($post->tags as $tag){?>
                    <?php if($tag!=''){?><a href='/tags/<? printtext($tag);?>'>#<? printtext($tag);?></a><?php }?>
                <?php }?>
            </div>
            <div class='post_complex_text'><?=$post->text; ?></div>
            <?php if($user_id){ ?>
                <div class="post_rating <?php echo $vote;?>" >
                     <span class="upvote" post_id="<?php echo $post->id;?>">
                        <span class='rating'><?php echo $post->user_rating;?></span>
                        <span class='text'><?=$text;?></span>
                     </span>
                    <?/*<span class='downvote' post_id='<?php echo $post->id;?>'>Downvote</span>*/?>
                </div>
            <?php }?>
            <?php if($user_id){ ?>
                <div class="repost_button" post_id="<?=$post->id;?>">
                    <img src="../images/icons/repost-icon.jpg">
                    <span  >Repost it</span>
                </div>
            <?php }?>
            <div class="share_buttons_list">

                <div class="share_facebook_button" image_url="" title="<?php printtext($post->title); ?>" desc=""
                     post_id="<?=$post->id;?>">
                    <img src="../images/icons/facebook-icon.png">
                    <span>Facebook</span>
                </div>
                <a class="share_twitter_button" title="<?php printtext($post->title); ?>" tags="<?=$post->getTagsList();?>"
                   href="http://exifeed.com/post/id<?=$post->id;?>" target="_blank">
                    <img src="../images/icons/twitter-icon.png" style="top: -2px;">
                    <span>Tweet this!</span>
                </a>

            </div>
            <div class='comments'>
                <a href='<?=$path; ?>post/id<?=$post->id; ?>#comments' target="_blank"><?=$post->comments_number; ?><img width="20px" src="../images/icons/comments.png"></a>
            </div>
        <?php } ?>

    </div>

</div>