<?php
$time=date('Y-m-d H:i:s');
if($type=='post'){
    $t=Router::getUrlArray();
    $post_id=substr($t[1],2);
}
$prev_page="";
if(isset($_SESSION['previous_page'])){
    $prev_page="&prev_page=".$_SESSION['previous_page'];
}
$footer_attr="";
if($type=='user'){
    $t=Router::getUrlArray();
    $footer_attr="user_id=".substr($t[1],2);
}
?><!DOCTYPE HTML>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#" ><meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1' />
<meta name="keywords" content="fun,funny,lol,meme,GIF,wtf,posts" />
<LINK href="/images/logo/favicon.ico" rel="SHORTCUT ICON">
<link rel="icon" href="/images/logo/favicon.ico">
<link rel="apple-touch-icon" sizes="57x57" href="/images/logo/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/images/logo/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/images/logo/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/images/logo/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/images/logo/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/images/logo/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/images/logo/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/images/logo/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/images/logo/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/images/logo/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/logo/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/images/logo/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/logo/favicon-16x16.png">
<link rel="manifest" href="/images/logo/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/images/logo/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<meta name="description" content="Exifeed is a place, where anyone can share some interesting article, image, video with other people and comment it.Something between 9GAG and Medium. " />
<?php if($type=='post'){?>
     <meta property="fb:app_id" content="119062525124829" /> 
  <meta property="og:type"   content="article" /> 
  <meta property="og:url"    content="http://exifeed.com/post/id<?=$post_id;?>" /> 
  <meta property="og:title"  content="<?=MyDatabase::GetParameter('post',$post_id,'title');?>" /> 
  <meta property="og:image"  content="<?=Post::getPostPreviewImage($post_id);?>" /> 
  <meta property="og:image:url"  content="<?=Post::getPostPreviewImage($post_id);?>" /> 
  <meta name="twitter:card" content="summary" >
  <meta name="twitter:site" content="@exifeed">
  <meta name="twitter:creator" content="@exifeed">
  <meta name="twitter:title" content="<?=MyDatabase::GetParameter('post',$post_id,'title');?>">
  <meta name="twitter:description" content="<?=MyDatabase::GetParameter('post',$post_id,'description');?>">
  <meta name="twitter:url" content="http://www.exifeed.com/post/id<?=$post_id;?>">
  <meta name="twitter:image" content="<?=Post::getPostPreviewImage($post_id);?>">
<?php } ?>
    <title>
        <?=$this->title;?>
    </title>
    <?=$this->css;?>
    <?=$this->javascript;?>
    <script src='/js/jquery.form.js'></script>
    <script src='//code.jquery.com/ui/1.11.4/jquery-ui.min.js'></script>
</head>
<body>
  <?/*
  <img src="http://www.exifeed.com/images/post/201/1.gif.jpg">
  exit; */?>
<?=$google_analytics; ?>
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId : '119062525124829',
            status : true, // check login status
            cookie : true, // enable cookies to allow the server to access the session
            xfbml : true // parse XFBML
        });
    };
    (function() {
        var e = document.createElement('script');
        e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
        e.async = true;
        document.getElementById('fb-root').appendChild(e);
    }());
</script><!-- Place this snippet wherever appropriate -->
<script type="text/javascript">
  (function() {
    var li = document.createElement('script'); li.type = 'text/javascript'; li.async = true;
    li.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + '//platform.stumbleupon.com/1/widgets.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(li, s);
  })();
</script>

<div id="social_buttons_container">
</div>
<img id="quotes" src="/images/icons/quotes.png">
<?if(User::isLogin()){
    $shorten=$_SESSION['user']['shorten_longpost'];
    if($shorten==0||$type=='post'){?>
    <style>
        .post .post_data .post_container {
            max-height: 100000px;
        }
        .post .show_full_button{
            display: none;
        }
    </style>
    <?}?>
<?}?>
<script>
    var isMobile=false;
    <? if($type=='post'){ ?>
        var post_id=<?=$post_id;?>;
    <?}else{?>
        var post_id=null;
    <?}?>
    var time='<?=$time;?>'
    var isLogin=<?=User::isLogin()==1 ? 'true' : 'false';?>;
</script>
<div id='layer'></div>
<div id='layer2'></div>
<div id="image_view"><img src=""> </div>
<div id="add_image_popup">
    <div id="image_popup_buttons">
        <button id="upload_image_popup">Upload Image</button>
        <button id="load_from_giphy">Find image on Giphy or Imgur</button>
    </div>
    <div id="giphy_images" style="display:none;">
        <div id="giphy_images_form">
            <input type="text"> <button>Find</button>
        </div>
        <div id="giphy_images_blok">
        </div>
    </div>
</div>
<div id="post_pc_view">
    <div class="post"></div>
</div>

<div id='header_container'>
    <div id='header'>
        <? if(!User::isLogin()){?>
            <a href='/about' class='header_button'>About</a>
            <span class='header_button login'>Log in/Sign Up</span>
        <?}else{?>
            <a href='/logout.php' class='header_button'>Logout</a>
            <a href='/about' class='header_button'>About</a>            
        <?}?>
        <div id='find'>
            <a href="/" id="title">Exifeed</a>
            <form action='/find' method='get' id='search_form'>
                <span id='find_button'></span>
                <input type='text' name='text' id='find_text'>
                
            </form>
        </div>
    </div>
</div>
<div id='container'>
    <div id='left_menu'>
        <a class='menu'  href='/'><span>Home</span></a>
        <a class='menu'  href='/best'><span>Best</span></a>
        <a class='menu'  href='/new'><span>New</span></a>
        <a class='menu'  href='/add'><span>Add post</span></a>
        <a class="menu" href="/feed"><span>My feed</span></a>
        <a class="menu" href="/comments"><span>My comments</span></a>
        <a class="menu" href="/saved"><span>Saved posts</span></a>
    </div>
</div>
<div class="sign_in_window">
    <div id="form_container">
        <div class="sign_in_form">
            <div class="register_title">Register with email</div>
            <form action='/registration.php' method='post'>
                <input type='text' class="name" name='name' placeholder='Full name'> <br>
                <input type='email' class="email" name='email' placeholder='E-mail'><br>
                <input type='password' class="password"  name='password' placeholder='Password'> <br>
                <input type='password' class="password2" name='password' placeholder='Confirm password'> <br>
                <p class="error" style="display:block;font-siz:18px;"></p>
                <span class="button">Create an account</span>
            </form>
        </div>
        <div id='login_form'>
            <div class="login_title">Log in or use existing account</div>
            <form action='' method='post'>
                <input type='text' class='text' name='email' placeholder='E-Mail '><br>
                <input type='password' class='password' name='password' placeholder='password'>
                <p class='error' >Your login or password is incorrect</p>
                <span class='button'>Ok</span>
            </form>
            <div class="social_buttons">
                <a class="facebook_link" href ="/social_login.php?login=Facebook<?=$prev_page;?>"><img src="/images/icons/facebook.png">Sign up with Facebook</a>
                <a class="twitter_link" href ="/social_login.php?login=Twitter<?=$prev_page;?>"><img src="/images/icons/twitter.png">Sign up with Twitter</a>
            </div>
        </div>
        <a class="close_button"></a>
    </div>
</div>
<div id='mainbody'>
    <?=$this->mainbody;?>
    <div id='footer' page_type='<?=$type;?>' <?=$footer_attr;?>>
        <?if($type=='index'||$type=='new'||$type=='feed'||$type=='best'){?>
        <div id="loading_line" style="text-align:center;display: none">
            <img src="/images/icons/loading_line.gif">
        </div>
        <?}?>
        <?=$this->footer;?>
    </div>
</div>
<div id='left' offset=''></div>
<div id='right'>
    <? if(false&&$type!='insert'){?>
    <div id="popular_tags">
        <div>Popular tags in last 24 hours</div>
        <? echo Post::Popular_tags();?>
    </div>
    <?}?>
    
    <div id="user_profile_block">
      <?if(User::isLogin()){?>
        <div  class="user_profile_image_container"><a href='/my' ><img src="<?=User::GetUserImage();?>"></a></div>
        <div>
            <a href='/my' style="position: relative;" class=' my_link'>                
                <?=User::GetUsername();?>
            </a><br>
            <a href="/settings" class="settings_link">Settings</a><br>
            <a href='/comments'>Comments</a>
        </div>
        <?}else{?>
            <div class="social_buttons">
                <a class="facebook_link" href ="/social_login.php?login=Facebook<?=$prev_page;?>"><img src="/images/icons/facebook.png">Sign up with Facebook</a>
                <a class="twitter_link" href ="/social_login.php?login=Twitter<?=$prev_page;?>"><img src="/images/icons/twitter.png">Sign up with Twitter</a>
            </div>
       <?}?>
      
    </div>
  
    <?=$this->rightside;?>
</div>

<div id="message_notification">
<div id="message_notification_title">
        <span>Comments</span>
        <div class='notifications_menu'>
            <div id="last_comments_menu" class="menu selected">Last comments</div>
            <div id="responds" class="menu">Comments responds</div>
        </div>
    </div>
    <div class="container" >
        <?=Comment::Last_comments();?>
    </div>
    <div class="container" style="display: none;">
    <? if(User::isLogin()){?>
        <?=Comment::Last_comments_responds(User::id());?>
    <?}?>
    </div>
</div>
<?=$this->javascript_bottom;?>
</body>
</html>