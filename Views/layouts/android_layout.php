<?php

 
$time=date('Y-m-d H:i:s');
$t=Router::getUrlArray();
$post_id=substr($t[1],2);
$prev_page="";
if(isset($_SESSION['previous_page'])){
    $prev_page="&prev_page=".$_SESSION['previous_page'];
}
$footer_attr="";
if($type=='user'){
    $t=Router::getUrlArray();
    $footer_attr="user_id=".substr($t[1],2);
}
?>
<!DOCTYPE HTML>
<head><meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1' />
    <meta name='viewport' content='width=device-width, initial-scale=1.0' >
    <meta name="keywords" content="fun,funny,lol,meme,GIF,wtf,posts" />
    <LINK href="/images/logo/favicon.ico" rel="SHORTCUT ICON">
    <link rel="icon" href="/images/logo/favicon.ico">
    <link rel="manifest" href="/images/logo/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/images/logo/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="Exifeed is a place, where anyone can share some interesting article, image, video with other people and comment it. " />
    <title><?=$this->title;?></title>
        <link rel="stylesheet" href="/css/android/framework7.ios.min.css">
    <!-- Path to Framework7 iOS related color styles -->
    <link rel="stylesheet" href="/css/android/framework7.ios.colors.min.css">
        <?=$this->css;?><?//=$this->javascript;?>
        <script src='/js/jquery-2.0.3.min.js'></script>
        <script type="text/javascript" src="/js/jquery.touchSwipe.min.js"></script>
        
<?/*
    <script async="true" src='file:///android_asset/js/jquery-form.js'></script>
    <script async="true" src='file:///android_asset/js/jquery-ui.js'></script>
    <script src='file:///Assets/android.js'></script>*/?>
</head>
<body>
<div id="fb-root"></div>
<script>

    window.fbAsyncInit = function() {
        FB.init({
            appId : '342011659317393',
            status : true, // check login status
            cookie : true, // enable cookies to allow the server to access the session
            xfbml : true // parse XFBML
        });
    };
    (function() {
        var e = document.createElement('script');
        e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
        e.async = true;
        document.getElementById('fb-root').appendChild(e);
    }());
</script>
<div style='display:none;'>
    <!--for copy text -->
    <input  id="post-shortlink" value="">
    <button class="button" style="display: none;" id="copy-button" data-clipboard-target="#post-shortlink">Copy</button>
</div>
<div id="social_buttons_container">
</div>
<?if(User::isLogin()){
    $shorten=$_SESSION['user']['shorten_longpost'];
    if($shorten==0||$type=='post'){?>
        <style>
            .post .post_data .post_container {
                max-height: 10000px;
            }
            .post .show_full_button{
                display: none;
            }
        </style>
    <?}?>
<?}else{?>
    <style>
        <?if($type=='post'){?>
        .post .post_data .post_container {
            max-height: 10000px !important;
        }
        <?}?>
        .share_buttons_list_mobile {
            height: 231px;
            top: calc(50% - 115px);
        }
    </style>
<?}?>
<script>
    var isMobile=true;
    <?php if($type=='post'){?>
    var post_id=<?=$post_id;?>;
    <?}else{?>
    var post_id=null;
    <?}?>
    var time='<?=$time;?>';
</script>
<div id='layer'></div>
<div id="add_image_popup">
    <div id="image_popup_buttons">
        <button id="upload_image_popup">Upload Image</button>
        <button id="load_from_giphy">Find image on Giphy or Imgur</button>
    </div>
    <div id="giphy_images" style="display:none;">
        <div id="giphy_images_form">
            <input type="text"> <button>Find</button>
        </div>
        <div id="giphy_images_blok">
        </div>
    </div>
</div>
<div id='header_container'>
    <div id='header'>
        <? if(!User::isLogin()){?>
            <span class='header_button login'>Log in/Sign Up</span>
        <?}else{?>
            <a href='/logout.php' class='header_button'>Logout</a>
            <a href='/settings' class='header_button'>Settings</a>
        <?}?>
            <div id='show_header_mobile'>
            <svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'  version='1.1' x='0px' y='0px' width='100%' height='100%' viewBox='0 0 33 26' overflow='visible' enable-background='new 0 0 33 26' xml:space='preserve'>
        <defs>
        </defs>
                <rect fill='#464646' width='100%' height='20%'/>
                <rect y='30%' fill='#464646' width='100%' height='20%'/>
                <rect y='60%' fill='#464646' width='100%' height='20%'/>
        </svg>
        </div>
        <div id='find_mobile'>
            <form action='/find' method='get' id='search_form'>
                <span id='find_button'></span>
                <input type='text' name='text' id='find_text'>                
            </form>
        </div>
    </div>
</div>
    <?/*

*/?>
<?php if(!User::isLogin()){ ?>
    <div id="login_mobile_form">
        <form action="" method="post" data-ajax="false">
            <div class="input">
                <input class="text" type="text" name="email" placeholder="Username or email">
            </div>
            <div class="input">
                <input class="password" type="password" name="password" placeholder="password">
            </div>
            <span class="error">Wrong email,name or  password</span>
            <div class="button">
                <span>Log in</span>
            </div>
            <div class="social_buttons">
                <a class="facebook_link" href ="/social_login.php?login=Facebook<?=$prev_page;?>"><img src="/images/icons/facebook.png">Sign up with Facebook</a>
                <a class="twitter_link" href ="/social_login.php?login=Twitter<?=$prev_page;?>"><img src="/images/icons/twitter.png">Sign up with Twitter</a>
            </div>
            <a href='#' class="back">Back</a>
        </form>
    </div>
    <div id="registration_mobile_form">
        <form action="registration.php" method="post" data-ajax="false">
            <div class="input">
                <input class="text username" type="text" name="name" placeholder="Full name">
            </div>
            <span id="username_error" class='error'></span>
            <div class="input">
                <input class="text email" type="text" name="email" placeholder="Email">
            </div>
            <span id="email_error" class='error'></span>
            <span class="label"></span>
            <div class="input">
                <input class="password" type="password" name="password" placeholder="Password">
            </div>
            <span id="password_error" class='error'></span>
            <span class="ajax-error error"></span><br>
            <div class="button">
                <span>Create account</span>
            </div>
            <div class="social_buttons">
                <a class="facebook_link" href ="/social_login.php?login=Facebook<?=$prev_page;?>"><img src="/images/icons/facebook.png">Sign up with Facebook</a>
                <a class="twitter_link" href ="/social_login.php?login=Twitter<?=$prev_page;?>"><img src="/images/icons/twitter.png">Sign up with Twitter</a>
            </div>
            <span class="back">Back</span>
        </form>
    </div>
<?}?>
    <div class="statusbar-overlay"></div>
    <!-- Views -->
    <div class="views">
      <!-- Your main view, should have "view-main" class -->
      <div class="view view-main">
        <!-- Top Navbar-->
        <div class="pages navbar-fixed">
          <div data-page="home" class="page with-subnavbar">
            <div class="navbar">
              <div class="navbar-inner">
                <div class="center">Swipeable Tabs</div>
                <div class="subnavbar">
                  <!-- Buttons row as tabs controller in Subnavbar-->
                  <div class="buttons-row">
                    <!-- Link to 1st tab, active -->
                    <a href="#tab1" class="button  tab-link">Menu</a>
                    <!-- Link to 2nd tab -->
                    <a href="#tab2" class="button  tab-link">Top</a>
                    <!-- Link to 3rd tab -->
                    <a href="#tab3" class="button tab-link">Tab 3</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- Tabs swipeable wrapper, required to switch tabs with swipes -->
            <div class="tabs-swipeable-wrap">
              <!-- Tabs, tabs wrapper -->
              <div class="tabs">
                <!-- Tab 1, active by default -->
                <div id="tab1" class="page-content tab">
                  <div class="">
                      <?if(User::isLogin()){?>
                      <div class="" style='padding-top:10px;padding-left:10px;'>
                                <div class="" style='display:inline-block;vertical-align:top;'>
                                    <img width='100px' src="<?=User::GetUserImage();?>">
                                  </div>
                                <span class="" style='margin-left:20px;'>
                                  <div class="" style='display:inline-block;vertical-align:top;'>
                                      <div class="item-title">
                                          <a href='/my'><?=MyDatabase::GetParameter('user',User::id(),'username');?></a>
                                      </div>
                                  </span>
                                  <div class="item-text">
                                      <a class="" href="/settings">Settings</a><br>
                                      <a class='' href='logout.php'><span>Logout</span></a>
                                  </div>
                                </div>
                        </div>
                      <?}?>
                      <div class="list-block">
                        <ul>                           
                            <li class="item-content"><a class='menu menu_link item-inner'  href='/'><span>Home</span></a></li>
                            <li class="item-content"><a class='menu menu_link item-inner'  href='/best'><span>Best</span></a></li>
                            <li class="item-content"><a class='menu menu_link item-inner'  href='/new'><span>New</span></a></li>
                            <li class="item-content"><a class='menu menu_link item-inner'  href='/feed'><span>My feed</span></a></li>
                            <li class="item-content"><a class='menu menu_link item-inner'  href='/comments'><span>Comments</span></a></li>
                            <li class="item-content"><a class='menu menu_link item-inner'  href='/add'><span>Add post</span></a></li>
                            <?php if(User::isLogin()){ ?>
                            
                            <?php }else{  ?>
                            <li class="item-content"><a class='menu menu_link'  href='/about'><span>About</span></a></li>
                            <li class="item-content"><a href='#' class='menu mobile_menu login_mobile'>Log in</a></li>
                            <li class="item-content"><a href='#' class='menu mobile_menu registration_mobile'>Registration</a></li>
                            <?php  } ?> 
                        </ul>
                    </div>
                        
                  </div>
                </div>
                <!-- Tab 2 -->
                <div id="tab2" class="page-content tab ">
                  <div class="content-block">
                      <?=$this->mainbody;?>
                    <div id='footer' page_type='<?=$type;?>' <?=$footer_attr;?>>
                        <?if($type=='index'||$type=='new'||$type=='feed'||$type=='best'){?>
                            <div id="loading_line" style="text-align:center;display: none">
                                <img src="/images/icons/loading_line.gif">
                            </div>
                        <?}?>
                        <?=$this->footer;?>        
                    </div>
                  </div>
                </div>
                <!-- Tab 3 -->
                <div id="tab3" class="page-content tab">
                  <div class="content-block">
                    ... Tab 3 content ...
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
               
      </div>
    </div>
    <!-- Path to Framework7 Library JS-->
    <script type="text/javascript" src="/js/framework7.min.js"></script>
    <script src='/js/android.js'></script>
    <!-- Path to your app js-->


<div id='post_view_div'>
    <div id="post_view_container">
        <div class='post'></div>
    </div>
    <div id='back_button'>
    </div>
</div>
<?=$this->javascript_bottom;?>
</body>
</html>