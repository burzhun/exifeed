<?php
function subcomments($comment,$post_id,$post_author,$user_id=''){
    $path=Router::pathtofolder();
    $is_author="";
    $vote="";
    if($user_id!=''){
        $user_rating=Comment::user_rating($user_id,$comment->id);
        //$user_rating=0;

        switch($user_rating) {
            case 0:
                $vote="";
                break;
            case 1:
                $vote="upvoted";
                break;
            case -1:
                $vote="downvoted";
                break;
        }
    }
    if($comment->user_id==$post_author){ 
        $is_author="comment_post_author";
    }
    if($comment->level==1){
        $html="<div class='root_comment $vote' id='comment{$comment->id}' level='{$comment->level}' rating='{$comment->rating}'>
            <img class='comment_user_image' src='".User::getCommentImage($comment->user_id)."'><div class='comment_container'>";
    } 
    else{
        $html="<div class='comment $vote' id='comment{$comment->id}' level='{$comment->level}' rating='{$comment->rating}'>
            <img class='comment_user_image' src='".User::getCommentImage($comment->user_id)."'><div class='comment_container'>";
    }
    $html.="<span class='comment_author {$is_author}'><a href='../user/id{$comment->user_id}'>{$comment->username()}</a></span>
            <span class='comment_add_date'>{$comment->ElapsedTime()}</span>
            <div class='comment_text'>{$comment->text}</div>";
    if($comment->image_url!=''){
        if($comment->video_url!=''){
            $html.="<div class='comment_image_container'>
                    <img class='gif_play_button' src='../images/icons/play_icon.png' video_src='{$comment->video_url}'>
                    <img class='comment_image gif_image' src='{$comment->image_url}' style='max-width: 300px;'>
                </div>";
        
        }else{
            if(preg_match("/[0-9]+\.gif/",$comment->image_url)){
             $gif_src=str_replace('.jpg','',$comment->image_url);
             $html.="<div class='comment_image_container'>
                    <img class='gif_play_button' src='../images/icons/play_icon.png' gif_src='{$gif_src}'>
                    <img class='comment_image' src='{$comment->image_url}' style='max-width: 200px;'>
                </div>";
           }else{
             $html.="<div class='comment_image_container'>
                    <img class='comment_image' src='{$comment->image_url}' style='max-width: 200px;'>
                </div>";
           }
        } 
        
    }
    if($user_id!=''){
        $html.="<div class='comment_buttons'>
                    <span class='comment_rating {$vote}' comment_id='{$comment->id}'>
                        <span class='comment_upvote'><v>{$comment->rating}</v> <t>Upvote</t></span>
                        <span class='comment_downvote'>Downvote</span>
                    </span>
                    <span class='comment_respond' comment_id='{$comment->id}' post_id='{$post_id}' level={$comment->level} is_opened='0'>Add comment</span>";
        if(User::isAdmin()&&!isMobile()){
            $html.="<span class='comment_delete' comment_id='{$comment->id}'  is_opened='0'>Delete comment</span>";
        }
            $html.="</div>";
    }
        $html.="</div><div class='subcomment' style=''>
    ";


    if($comment->children_comments){
        $comments=$comment->children_comments;
        foreach($comments as $comment1){
            $html.=subcomments($comment1,$post_id,$post_author,$user_id);
        }
    }
    $html.="</div></div>";
    return $html;
}
function comments($comments,$post_id,$post_author,$user_id='')
{
    $html = "<div id='comments'>";
    if (isset($comments[0])) {
        foreach ($comments as $comment) {
            $html .= subcomments($comment,$post_id,$post_author,$user_id);
        }
    }
    $html.="</div>";
    return $html;
}
function comment_ajax_pc($comment,$post_author,$user_id){
    $path=Router::pathtofolder();
    $is_author="";
    $vote="";
    if($user_id!=''){
        $user_rating=Comment::user_rating($user_id,$comment->id);
        //$user_rating=0;

        switch($user_rating) {
            case 0:
                $vote="";
                break;
            case 1:
                $vote="upvoted";
                break;
            case -1:
                $vote="downvoted";
                break;
        }
    }
    if($comment->user_id==$post_author){
        $is_author="comment_post_author";
    }
    if($comment->level==1){
        $html="<div class='root_comment ajax_comment $vote' id='comment{$comment->id}' level='{$comment->level}' rating='{$comment->rating}'>
            <img class='comment_user_image' src='".User::getCommentImage($comment->user_id)."'><div class='comment_container'>";
    }
    else{
        $html="<div class='comment ajax_comment $vote' id='comment{$comment->id}' level='{$comment->level}' rating='{$comment->rating}'>
            <img class='comment_user_image' src='".User::getCommentImage($comment->user_id)."'><div class='comment_container'>";
    }
    $html.="<span class='comment_author {$is_author}'><a href='../user/id{$comment->user_id}'>{$comment->username()}</a></span>
            <span class='comment_add_date'>{$comment->ElapsedTime()}</span>
            <div class='comment_text'>{$comment->text}</div>";
    if($comment->image_url!=''){
         if($comment->video_url!=''){ 
            $html.="<div class='comment_image_container'>
                    <img class='gif_play_button' src='../images/icons/play_icon.png' video_src='{$comment->video_url}' gif_src='{$comment->image_url}'>
                    <img class='comment_image gif_image' src='{$comment->image_url}' style='max-width: 300px;'>
                </div>";
        }else{
           if(preg_match("/[0-9]+\.gif/",$comment->image_url)){
               $gif_src=str_replace('.jpg','',$comment->image_url);
             $html.="<div class='comment_image_container'>
                    <img class='gif_play_button' src='../images/icons/play_icon.png' gif_src='{$gif_src}'>
                    <img class='comment_image' src='{$comment->image_url}' style='max-width: 200px;'>
                </div>";
           }else{
             $html.="<div class='comment_image_container'>
                    <img class='comment_image' src='{$comment->image_url}' style='max-width: 200px;'>
                </div>";
           }
            
        }

    }
    if($user_id!=''){
        $html.="<div class='comment_buttons'>
                    <span class='comment_rating {$vote}' comment_id='{$comment->id}'>
                        <span class='comment_upvote'><v>{$comment->rating}</v> <t>Upvote</t></span>
                        <span class='comment_downvote'>Downvote</span>
                    </span>
                    <span class='comment_respond' comment_id='{$comment->id}' post_id='{$post_id}' level={$comment->level} is_opened='0'>Add comment</span>";
        if(User::isAdmin()&&!isMobile()){
            $html.="<span class='comment_delete' comment_id='{$comment->id}'  is_opened='0'>Delete comment</span>";
        }
            $html.="</div>";
    }
    return $html;

}
function single_comment($id,$user_id,$parent_comment_id,$text,$image,$video,$post_id){
    if(User::id()==$user_id){
        $time=Comment::elapsed_time($id);
        $html="<div class='single_comment'>
            <div class='comment_text'>{$text}</div>";
        if($image!=''){
            if($video!=''){
                $html.="<div class='comment_image_container'>
                    <img class='gif_play_button' src='../images/icons/play_icon.png' video_src='{$video}' gif_src='{$image}'>
                    <img class='comment_image gif_image' src='{$image}' style='max-width: 300px;'>
                </div>";
            }else{
                if(preg_match("/[0-9]+\.gif/",$image)){
                     $gif_src=str_replace('.jpg','',$image);
               $html.="<div class='comment_image_container'>
                      <img class='gif_play_button' src='../images/icons/play_icon.png'  gif_src='{$gif_src}'>
                      <img class='comment_image' src='{$image}.jpg' style='max-width: 200px;'>
                  </div>";
              }else{
                  $html.="<div class='comment_image_container'>
                      <img class='comment_image' src='{$image}' style='max-width: 200px;'>
                  </div>";
                }
              
             }

        }
        $html.="<div class='comment_link'><a href='/post/id{$post_id}#comment{$id}'>Link</a>
                <span class='comment_add_date'>{$time}</span>
            </div>  </div>";
        return $html;
    }else{
        $p_com=new Comment($parent_comment_id);
        $time=Comment::elapsed_time($id);
        $user_id=MyDatabase::GetParameter('comments',$id,'user_id');
        $user=new User($user_id);
        $html1=single_comment($p_com->id,$p_com->user_id,$p_com->parent_comment_id,$p_com->text,$p_com->image_url,$p_com->video_url,$p_com->post_id);
        $html="<div class='single_comment' style='margin-left: 20px;'>
            <div class='comment_username' style='color: rgb(85, 145, 236);'>{$user->username}</div>
            <div class='comment_text'>{$text}</div>";
        if($image!=''){
            if($video!=''){
                $html.="<div class='comment_image_container'>
                    <img class='gif_play_button' src='../images/icons/play_icon.png' video_src='{$video}' gif_src='{$image}'>
                    <img class='comment_image gif_image' src='{$image}' style='max-width: 300px;'>
                </div>";
            }else{
                if(preg_match("/[0-9]+\.gif/",$image)){
                    $gif_src=str_replace('.jpg','',$image);
               $html.="<div class='comment_image_container'>
                      <img class='gif_play_button' src='../images/icons/play_icon.png' gif_src='{$gif_src}'>
                      <img class='comment_image' src='{$image}' style='max-width: 200px;'>
                  </div>";
              }else{
                  $html.="<div class='comment_image_container'>
                      <img class='comment_image' src='{$image}' style='max-width: 200px;'>
                  </div>";
                }
            }

        }
        $html.="<div class='comment_link'><a href='/post/id{$post_id}#comment{$id}'>Link</a>
                <span class='comment_add_date'>{$time}</span>
            </div>  </div>";
        return $html1.$html;
    }

}
function unresponded_comments($comment){
    $username=MyDatabase::GetParameter('user',$comment->user_id,'username');
    $image="";
    $html1="";
    $class="";
    if($comment->parent_comment_id){
        $class="has_parent";
        $comment2=new Comment($comment->parent_comment_id);
        $username2=MyDatabase::GetParameter('user',$comment2->user_id,'username');
        $image2='';
        if($comment2->image_url!=''){
            $video=$comment2->video_url;
            if($video!=''){
                $image2="<div class='comment_image_container'>
                    <img class='gif_play_button' src='/images/icons/play_icon.png' video_src='{$video}'>
                    <img class='comment_image gif_image' src='{$comment2->image_url}' style=''>
                </div>";
            }else{
                $image2="<div class='comment_image_container'>
                    <img class='comment_image' src='{$comment2->image_url}' style=''>
                </div>";
            }
        }
        $html1=<<<end
            <div class="notification_parent_comment">
                <div class="comment_author">{$username2}</div>
                <a class="comment_text" target="_blank" href="/post/id{$comment2->post_id}#comment{$comment2->id}">{$comment2->text}{$image2}</a>
            </div>
end;
    }
    if($comment->image_url!=''){
        $video=$comment->video_url;
        if($video!=''){
            $image="<div class='comment_image_container'>
                <img class='gif_play_button' src='/images/icons/play_icon.png' video_src='{$video}' gif_src='{$comment->image_url}'>
                <img class='comment_image gif_image' src='{$comment->image_url}' style=''>
            </div>";
        }else{
          if(preg_match("/[0-9]+\.gif/",$image)){
              $gif_src=str_replace('.jpg','',$image);
               $image="<div class='comment_image_container'>
                      <img class='gif_play_button' src='/images/icons/play_icon.png' gif_src='{$gif_src}'>
                      <img class='comment_image' src='{$comment->image_url}' style='max-width: 200px;'>
                  </div>";
          }else{
              $image="<div class='comment_image_container'>
                  <img class='comment_image' src='{$comment->image_url}' style='max-width: 200px;'>
              </div>";
            }          
        
        }
    }
    $html=<<<end
    {$html1}
    <div class="notification_comment {$class}">
        <div class="comment_author">{$username}</div>
        <a class="comment_text" target="_blank" href="/post/id{$comment->post_id}#comment{$comment->id}">{$comment->text}{$image}</a>
    </div>
end;
    return $html;

}