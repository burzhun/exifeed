 $(document).ready(function() {
    var h=0;
    $("body").on("click",".comment_image_container .comment_image",function(){
        if(!$(this).hasClass('gif_image')){
             $("#layer").show();
            var src=$(this).attr('src');
            $("#image_view img").attr('src',src);
            $("#image_view").show();
            return false;
        }
       
    });
     //setTimeout(function(){$(".login").trigger('click');},2000);
    $("body").on('click','.login_link',function(){
        $("#header .login").click();
    });
    $("body").on('click','.register_link',function(){
        $("#header .login").click();
    });
    var post_pc_view_interval=null;
    var pc_post_opened=false;
     var ignore_classes=['gif_image','comment_number_image','gif_video','gif_playing','youtube_play','coub_play'];
    if(!post_id){        
        $("body").on("click",".post_container",function(e){
            if(isTextSelected()){
                return false;
            }
            var tag=e.target.tagName.toLowerCase(); 
            if(tag!='a'&&tag!='video'&&tag!='span'&&e.target.className!='show_full_button'&&e.target.className!='share_button_image'&&e.target.className!='gif_play_button'){
                var class1=' '+$(e.target).attr('class')+' ';
                for(var i=0;i<=ignore_classes.length;i++){
                    if(class1.indexOf(ignore_classes[i])>0){
                        return true;
                    }
                }
                 $("#layer").show();
                 var html=$(this).parent().parent().html();
                 $("#post_pc_view .post").html(html);
                 var post_id=$(this).attr('post_id');
               // return;
                 console.log(post_id);
                 $.ajax({
                    url:'/ajax_submit.php?ajax_comments_div=1',
                    type:"post",
                    data:{
                        post_id:post_id
                    },
                    success:function (data)
                    {
                        $("<script> var post_id="+post_id+"</script>").appendTo("#post_pc_view");
                        $('#post_pc_view .post').append(data);
                        if (data.length > 0) {
                            loading = false;
                        }
                        post_pc_view_interval=setInterval(function(){LoadPostCommentsAjaxPc(time,post_id,'#post_pc_view')},10000);
                    }
                });
                 $("#post_pc_view").show();
                 pc_post_opened=true;
             }
        });
    }
    $("#layer").click(function(){
        $("#image_view").hide();
        $("#layer").hide();
        $("#post_pc_view").hide();
        $('#post_pc_view .post').html('');
        clearInterval(post_pc_view_interval);
    });
    $("#layer2").click(function(){
        $("#add_image_popup").hide();
        $(this).hide();
    });
    $(".sign_in_window").click(function(e){
        if(e.target.className=="sign_in_window"){
            $(".sign_in_window").hide();
            $("#layer").hide();
        }
    });
    $("#left").click(function(){
        if($(window).scrollTop()>0){
            $("#left").attr('offset',$(window).scrollTop());
            $('html,body').animate({ scrollTop: 0 }, 'slow');
        }
        else{
            if($("#left").attr('offset')!=''){
                var offset=$("#left").attr('offset');
                $('html,body').animate({ scrollTop: offset }, 'slow');
            }
        }
    });
   if(post_id){
     var loading2=true;
     var offset2=0;
     $(window).ready(function(e){
         loading2=true;
          $.post("/ajax_submit.php?other_posts=1",{post_id:post_id,offset:offset2},function(data){
            if(data!=''){
              $("#additional_posts").append(data);              
              offset2+=10;
            }
            //loading2=false;
          });
     })
   }
    
    $('#post_pc_view').on('DOMMouseScroll mousewheel', function(ev) {
        var $this = $(this),
            scrollTop = this.scrollTop,
            scrollHeight = this.scrollHeight,
            height = $this.height(),
            delta = (ev.type == 'DOMMouseScroll' ?
                ev.originalEvent.detail * -40 :
                ev.originalEvent.wheelDelta),
            up = delta > 0;
        var prevent = function() {
            ev.stopPropagation();ev.preventDefault();
            ev.returnValue = false; return false;
        }
        if (!up && -delta > scrollHeight - height - scrollTop) {
            $this.scrollTop(scrollHeight);
            return prevent();
        } else if (up && delta > scrollTop) {
            $this.scrollTop(0);
            return prevent();
        }
    });
    $(window).resize(function(){
    });
    var loading=false;
    var offset=10;
    var recent_posts_loading=false;
    $(window).scroll(function(e){
        if(recent_posts_loading){
            if(!loading&&($('#footer').offset().top<$(window).scrollTop()+$(window).height())) {
                loading = true;
                var type=$("#footer").attr('page_type');
                var url=window.location.href;
                var n=url.indexOf('?');
                var s="";
                if(n>0){
                    s=url.substring(n);
                }
                $("#loading_line").show();
                $.ajax({
                    url:'/ajax_submit.php'+s,
                    type:"post",
                    data:{
                        pagetype: type,
                        offset: offset
                    },                  
                    success:function (data)
                    {
                        $('#mainbody #content').append(data);                 
                        if (data.length == 0) {
                            $('#mainbody #content').append("<div style='text-align:center;font-size:20px;'> No more posts</div>");
                        }else{
                            loading = false;
                        }
                        $("#loading_line").hide();
                        offset+=10;
                    }
                });
            }
        }
    });
    var recent_posts_offset=10;
    if($('#recent_posts_footer').length){
        $(window).scroll(function(){
            if(!recent_posts_loading&&($('#recent_posts_footer').offset().top<$(window).scrollTop()+$(window).height())) {
                recent_posts_loading = true;
                var type="recent_posts";
                $.post('/ajax_submit.php', {offset: recent_posts_offset, type: type}, function (data) {
                    $('#mainbody #recent_posts').append(data);                 
                    if(data.length>0){
                        recent_posts_loading = false;
                        recent_posts_offset+=10;
                    }
                });
            }
        });
    }
    else{
        recent_posts_loading=true;
    }
    $("#find_button").click(function(){
        $("#search_form").submit();
    });
    $("#login_form .password").keypress(function(e){
        if(e.which == 13) {
            $("#login_form form .error").hide();
            var name=$("#login_form form .text").val();
            var password=$("#login_form form  .password").val();
            $.post('/ajax_submit.php?check_user=1',{email:name,password:password},function(data){
                if(data=='Ok'){
                    $("#login_form form").submit();
                }
                else{
                    $("#login_form form .error").show();
                }
            });
        }
    });
    $("#login_form .button").click(function(){
        $("#login_form form .error").hide();
        var email=$("#login_form form .text").val();
        var password=$("#login_form form  .password").val();
        var captcha='';
        if($("#login_form form .captcha").length){
            captcha=$("#login_form form .captcha input").val();
        }
        $.post('/ajax_submit.php?check_user=1',{email:email,password:password,captcha:captcha},function(data){
            if(data=='Ok'){
                location.reload();
            }
            else{
                $("#login_form form .captcha").remove();
                if(data=='captcha'){
                    $.post('/upload.php?captcha=1',{},function(data){                        
                        $("#login_form form .error").after(data);
                    });
                }else{
                    $("#login_form form .error").show();
                }
                
            }
        });
    });
    $(".login").click(function(){
        $("#layer").show();
        $(".sign_in_window").show();
    });
    $(".sign_in_window .close_button").click(function(){
        $("#layer").hide();
        $(".sign_in_window").hide();
    });
    $(".sign_in_form form .button").click(function(){
        var email=$(".sign_in_form form .email").val();
        var name=$(".sign_in_form form .name").val();
        var password=$(".sign_in_form form .password").val();
        var password2=$(".sign_in_form form .password2").val();
        var email_reg = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i;
        $(".sign_in_form form .error").text("");
        if(!email_reg.test(email)){
            $(".sign_in_form form .error").text("Your email is incorrect");
        }
        if(name.length<3||name.length>25){
            $(".sign_in_form form .error").text("Username must contain form 3 to 25 characters");
            return;
        }
        if(!/^[a-zA-Z][a-zA-Z0-9_ ]+$/i.test(name)){
            $(".sign_in_form form .error").text("Username may contain only letters,digits,'_' and must start with letter");
            return;
        }
        if(password!=password2){
            $(".sign_in_form form .error").text("Your passwords are not equal");
            return;
        }
        if(password.length<5){
            $(".sign_in_form form .error").text("Your password should contain at least 5 characters");
            return ;
        }
        $.post('/ajax_submit.php?user_exists=1',{email:email,name:name,password:password}, function (data) {
            if(data=='Ok'){
                $(".sign_in_form form").submit();
            }
            else{
                $(".sign_in_form form .error").text(data);
            }
        })
    });
    $("#message_notification .comment_text").click(function(e){
        if(e.target.className=='gif_play_button'){
            return false;
        }
    });
   var offset1=10;
   var offset2=10;
    var comments_loaded=true;
    function LoadCommentsAjax(time1){
        if(comments_loaded){
            comments_loaded=false;
            $.post('/ajax_submit.php?ajax_pc_new_comments=1',{time:time1},function(data){
                //alert(data);
                comments_loaded=true;
                if(data!='No comments'){
                    $("#message_notification").show();
                    data=JSON.parse(data);
                    var array=data.array;
                    time=data.time;
                    offset1+=array.length;
                    //alert(array[0].html);
                    for(var i=0;i<array.length;i++){
                        //var parent_comment_id=array[i].parent_comment_id;
                        $("#message_notification .container").prepend(array[i].html);
                    }
                }
            });
        }
    }
    if(isLogin){
        setInterval(function(){LoadCommentsAjax(time)},5000);
    }
   var comms_index=0;
   $("#message_notification .notifications_menu .menu").click(function(){
        $(this).parent().find('.menu').removeClass('selected');
        $(this).addClass("selected");
        $("#message_notification .container").hide();
        $("#message_notification .container").eq($(this).index()).show();
        comms_index = $(this).index();
    });
   var h1=$("#message_notification").height();   
    $("#message_notification .container").on('DOMMouseScroll mousewheel',function(){
      if(h1-$(this).height()+200>$(this).position().top){
        var t=$(this);
        var offset = comms_index === 0 ? offset1 : offset2;
        $.post('/ajax_submit.php?ajax_new_comments=1',{index:comms_index,offset:offset},function(data){
          t.append(data);
          if(comms_index == 0) offset1+=10;
          else offset2 +=10;
        });
      }
    });
});
$(window).load(function(){
   /* $(".post").each(function(){
        h=$(this).height();
        if(h>900){
            if(h<1200){
                $(this).find(".post_container").css('max-height','1300px');
            }else{
                $(this).find(".show_full_button").show();
            }
        }
    });*/
});
function isTextSelected() {
        var selecttxt = '';
        if (window.getSelection) {
            selecttxt = window.getSelection();
        } else if (document.getSelection) {
            selecttxt = document.getSelection();
        } else if (document.selection) {
            selecttxt = document.selection.createRange().text;
        }

        if (selecttxt == '') {
            return false;
        }
        return true;

    }
