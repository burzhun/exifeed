$(document).ready(function(){
    $(".custom_checkbox").click(function(){
        if($(this).hasClass('yes')){
            $(this).find('.checkbox_val').val('0');
            $(this).removeClass('yes');
            $(this).addClass('no');
        }
        else{
            if($(this).hasClass('no')){
                $(this).find('.checkbox_val').val('1');
                $(this).removeClass('no');
                $(this).addClass('yes');
            }
        }
    });
    $("#change_image_button").click(function(){
        $(this).parent().find('input').trigger('click');
    });
    $("#delete_image_button").click(function(){
        $.post('/settings?no_image=1',{},function(){
            window.location.reload();
        });
    });
    var old_name=$("#user_data_form .username input").val();
    var user_form_submitted=false;
    $("#user_data_form form").submit(function(e){
        if(user_form_submitted){
            user_form_submitted=false;
            return true;
        }
        e.preventDefault();
        var name=$("#user_data_form .username input").val();
        $(".username .error").html('');
        if(name==old_name){
            user_form_submitted=true;
            $("#user_data_form form").submit();
        }
        $.post('ajax_submit.php?name_used=1',{name:name},function(data){
            if(data=='Ok'){
                user_form_submitted=true;
                $("#user_data_form form").submit();
            }else{
                $(".username .error").html(data);
                return false;
            }
        });
        //return false;
    });
    $("#user_image_form form input").change(function () {
        $("#user_image_form form").submit();

    });
    $("#save_password_button").click(function(){
        $("#settings #change_password .error").text("");
        $("#settings #change_password .success").text("");
        var old_password=$("#settings .old_password").val();
        var new_password=$("#settings .new_password").val();
        var new_password2=$("#settings .new_password2").val();
        if(new_password!=new_password2){
            $("#settings #change_password .error").text("Your passwords are not equal");
            return;
        }
        if(new_password.length<3||new_password.length>15){
            $("#settings #change_password .error").text("Password may contain form 3 to 15 characters");
            return;
        }
        if(!(/[a-z0-9_]+$/i.test(new_password))){
            $("#settings #change_password .error").text("Password may contain only letters,digits,'_' and must start with letter");
            return;
        }

        var email=$("#settings #save_password_button").attr('email');
        $email=$("#save_password_button").attr('email');
        $.post('ajax_submit.php?change_pass=1',{email:email,old_password:old_password,new_password:new_password},function(data){
            if(data=='No'){
                $("#settings #change_password .error").text("Your old password is incorrect");
            }
            if(data=='Changed'){
                $("#settings #change_password .success").text("Your password has been changed");
            }
        });
    });
});