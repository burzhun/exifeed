$(document).ready(function(){
    $("#registration_mobile_form form div.button").click(function(){
        Registration_validation();
    });
    function Registration_validation(){
        var name=$("#registration_mobile_form form div.input .username").val();
        var email=$("#registration_mobile_form form div.input .email").val();
        var password=$("#registration_mobile_form form div.input .password").val();
        var password2=$("#registration_mobile_form form div.input .password2").val();
        if(!/^[a-z][a-z0-9_]+$/i.test(name)){
            $("#registration_mobile_form form #username_error").text("Username may contain only letters,digits,'_' and must start with letter");
            return;
        }
        if(name.length<3||name.length>15){
            $("#registration_mobile_form form #username_error").text("Username may contain form 3 to 15 characters");
            return;
        }
        var reg = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i;
        if(!reg.test(email)){
            $("#registration_mobile_form form #email_error").text("Your email is incorrect");
            return;
        }
        if(!password==password2){
            $("#registration_mobile_form form #password_error").text("Your passwords are not equal");
            return;
        }
        if(!/^[a-z][a-z0-9_]+$/i.test(password)){
            $("#registration_mobile_form form #password_error").text("Pasword may contain only letters,digits,'_' and must start with letter");
            return;
        }
        alert('3412');
    }
});