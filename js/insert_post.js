$(document).ready(function(){
    $(".post_type_item").click(function(){
        if(!$(this).hasClass('post_type_item_selected')){
            var n=$(this).index();
            $('.insert_form_item').hide();
            $('.insert_form_item').eq(n).show();
            $('.post_type_item').removeClass('post_type_item_selected');
            $(this).addClass('post_type_item_selected');
            if(n==3){

            }
            if(n==1){
                $("#complex_text").attr('contenteditable','true');
                var name;
                for(name in CKEDITOR.instances) {
                    var instance = CKEDITOR.instances[name];
                    if(this && this == instance.element.$) {
                        instance.destroy;
                    }
                }

                //CKEDITOR.inline( 'complex_text' );
                $("#complex_text").html("");
            }
        }
    });
    if(window.CKEDITOR!== undefined){
        CKEDITOR.disableAutoInline = true;
    CKEDITOR.config.autoParagraph = false;
    }
    
    var image_index=1;
    
    $("#add_image_button").click(function(){
        var input_file="<input type='file' accept='image/*' class='image_file_input'  image_index="+image_index+" name='image"+image_index+"' id='image_file"+image_index+"'>";
        $("#complex_form form").append(input_file);
        $("#image_file"+image_index).trigger('click');

        image_index++;
    });
    $("body").on('change','.image_file_input',function(){
        var image_index=$(this).attr('image_index');
        var data=new FormData();
        data.append('image',this.files[0]);
        data.append('image_index',image_index);
        data.append('number',4);
        $.ajax({
            url: "upload.php",
            type: "POST",
            data: data,
            processData: false,
            contentType: false,
            success: function(response) {
                pasteHtmlAtCaret(response);
                //$("#complex_text").append(response);
                //$(".image_container").draggable({containment:"parent"}).resizable();
                $("#complex_text .image_container").resizable();
            },
            error: function(jqXHR, textStatus, errorMessage) {
                //console.log(errorMessage);
            }
        });

    });
    function pasteHtmlAtCaret(html) {
        var sel, range;
        if (window.getSelection) {

            // IE9 and non-IE
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                range = sel.getRangeAt(0);
                range.deleteContents();

                // Range.createContextualFragment() would be useful here but is
                // non-standard and not supported in all browsers (IE9, for one)
                var el = document.createElement("div");
                el.innerHTML = html;
                var frag = document.createDocumentFragment(), node, lastNode;
                while ( (node = el.firstChild) ) {
                    lastNode = frag.appendChild(node);
                }
                range.insertNode(frag);

                // Preserve the selection
                if (lastNode) {
                    range = range.cloneRange();
                    range.setStartAfter(lastNode);
                    range.collapse(true);
                    sel.removeAllRanges();
                    sel.addRange(range);
                }
            }
        } else if (document.selection && document.selection.type != "Control") {
            // IE < 9
            document.selection.createRange().pasteHTML(html);
        }
    }
    $("#complex_form form").submit(function(e){
        //e.preventDefault();
        if(CheckComplexForm()){
            $("#complex_text img").attr('src','');
            var container_width=$("#complex_text").width();
            $("#complex_text .image_container").each(function(){
                var width=$(this).width();
                var l=$(this).position().left;
                var p=width/container_width*100;
                $(this).removeClass('ui-resizable');
                //var left=l/container_width;
                $(this).css('width',p+'%');
                //$(this).css('left',l+'%');
            });
            var text=$("#complex_text").html();
            $("#complex_text_area").html(text);
            //$("#complex_text").remove();
            //$(this).submit();
            $(this).ajaxSubmit(function(data){
            });
            return false;
        }
        else{
            return false;
        }

    });
    function d2h(d) {
        return d.toString(16);
    }
    function stringToHex (tmp) {
        var str = '',
            i = 0,
            tmp_len = tmp.length,
            c;
     
        for (; i < tmp_len; i += 1) {
            c = tmp.codePointAt(i);
            str += d2h(c);
        }
        return str;
    }
    $("#long_post form").submit(function(e){
        //e.preventDefault();
        if(CheckLongPostForm()){
            //$("#complex_text img").attr('src','');
            var backup = $("#long_post form #long_post_data").html();
            $(".long_post_part.youtube_video iframe").remove();
            $(".long_post_part.coub_video iframe").remove();
            $(".long_post_part.video img").show();
            $(".long_post_part.video .youtube_name").show();
            $(".long_post_part.video .coub_name").show();
            $(".text_overlay[has_text='0']").remove();
            $(".text_overlay").each(function(){
                $(this).css('transform','initial');
                var width=$(this).width()*105/$(this).parent().width();
                var left=$(this).position().left*100/$(this).parent().width()-width/2;
                var top=$(this).position().top*100/$(this).parent().height();
                var height=$(this).height()*100/$(this).parent().height();
                $(this).css('width',width+'%').css('left',left+'%').css('top',top+'%').css('height',height+'px');
                var font_size=parseInt($(this).css('font-size'))/$(this).parent().width();
                $(this).attr('font-size-param',font_size);
            });
            
            $("#long_post .image_uploaded img:not([old])").attr('src','');
            $("#long_post .image canvas").remove();
            var data=$("#long_post form #long_post_data").html();
            $("#long_post_data_field").val(data);
            //return false;
            //$("#complex_text").remove();
            //$(this).submit();
            $("#upload_post_loader_layer").show();
            $("#upload_post_loader").show();
            $(this).ajaxSubmit(function(data){
                if(data=='post_uploaded'){
                    window.location.href='/new';
                }else{
                    $("#upload_post_loader_layer").hide();
                    $("#upload_post_loader").hide();
                    $("#long_post form #long_post_data").html(backup);
                    alert("Some error occured.");
                }
            });
            return false;
        }
        else{
            return false;
        }

    });
    
    function isTextSelected() {
        var selecttxt = '';
        if (window.getSelection) {
            selecttxt = window.getSelection();
        } else if (document.getSelection) {
            selecttxt = document.getSelection();
        } else if (document.selection) {
            selecttxt = document.selection.createRange().text;
        }

        if (selecttxt == '') {
            return false;
        }
        return true;

    }
    var chosen_image=null;
    $("#complex_text ").click(function(e){
        e.stopPropagation();
        if(e.target.tagName=='IMG'){
            chosen_image= e.target;
        }
        else{
            chosen_image=null;
        }

    });
    $("body").on('click','.cke_button__justifyright',function(e){
        if(chosen_image){
            e.preventDefault();
            //$('#cke_16').trigger('click');
            //alert($(chosen_image).width());
            $(chosen_image).parent().removeClass('image_container_center');
            $(chosen_image).parent().removeClass('image_container');
            $(chosen_image).parent().addClass('image_container_right');
            $(".image_container").each(function(){
                if($(this).find('img').length==0){
                    $(this).remove();
                }
            });
           // $(chosen_image).addClass('float_right');
            return false;
        }
    });
    $("body").on('click','.cke_button__justifycenter',function(e){
        if(chosen_image){
            $(chosen_image).parent().removeClass('image_container_right');
            $(chosen_image).parent().removeClass('image_container');
            $(chosen_image).parent().addClass('image_container_center');
            $(chosen_image).parent().css('width','auto');
            $(chosen_image).parent().css('height','auto');
            $(".image_container").each(function(){
                if($(this).find('img').length==0){
                    $(this).remove();
                }
            });
            return false;
        }
    });
    $("body").on('click','.cke_button__justifyleft',function(e){
        if(chosen_image){
            e.preventDefault();
            //$('#cke_16').trigger('click');
            //alert($(chosen_image).width());
            $(chosen_image).parent().removeClass('image_container_right');
            $(chosen_image).parent().removeClass('image_container_center');
            $(chosen_image).parent().addClass('image_container');
            $(".image_container").each(function(){
                if($(this).find('img').length==0){
                    $(this).remove();
                }
            });
            return false;
        }
    });

    $("#complex_text ").blur(function(){
       // chosen_image=null;
    });

    $("#layer").click(function(){
        $("#add_link_window").hide();
        $("#layer").hide();
    });

    function replaceSelectedText(replacementText) {
        var sel, range;
        if (window.getSelection) {
            sel = window.getSelection();
            if (sel.rangeCount) {
                range = sel.getRangeAt(0);
                range.deleteContents();
                range.insertNode(document.createTextNode(replacementText));
            }
        } else if (document.selection && document.selection.createRange) {
            range = document.selection.createRange();
            range.text = replacementText;
        }
    }
    function getSelectionCoords() {
        var sel = document.selection, range, rect;
        var x = 0, y = 0;
        if (sel) {
            if (sel.type != "Control") {
                range = sel.createRange();
                range.collapse(true);
                x = range.boundingLeft;
                y = range.boundingTop;
            }
        } else if (window.getSelection) {
            sel = window.getSelection();
            if (sel.rangeCount) {
                range = sel.getRangeAt(0).cloneRange();
                if (range.getClientRects) {
                    range.collapse(true);
                    if (range.getClientRects().length>0){
                        rect = range.getClientRects()[0];
                        x = rect.left;
                        y = rect.top;
                    }
                }
                // Fall back to inserting a temporary element
                if (x == 0 && y == 0) {
                    var span = document.createElement("span");
                    if (span.getClientRects) {
                        // Ensure span has dimensions and position by
                        // adding a zero-width space character
                        span.appendChild( document.createTextNode("\u200b") );
                        range.insertNode(span);
                        rect = span.getClientRects()[0];
                        x = rect.left;
                        y = rect.top;
                        var spanParent = span.parentNode;
                        spanParent.removeChild(span);

                        // Glue any broken text nodes back together
                        spanParent.normalize();
                    }
                }
            }
        }
        return { x: x, y: y };
    }


    function Wrap_text(tag,classname){
        var sel = window.getSelection();
        var range=sel.getRangeAt(0);
        var s=document.createElement('span');
        $(s).addClass(classname);
        s.innerHTML=sel;
        range.deleteContents();
        range.insertNode(s);

        range.setStartAfter(s);
        range.setEndAfter(s);
        sel.removeAllRanges();
        sel.addRange(range);
    }
    $('.format').click(function(){
        Wrap_text('bold');
    });
    var shown=false;
    var started=false;
    $('#complex_text').keydown(function(e){

        if ( e.keyCode === 32 )//for space bar
        {
            var node = document.getSelection().anchorNode;
            var p=node.parentNode;
            //var c=p.cloneNode(true);
            if(p.nodeName=='SPAN'){
                var sel = window.getSelection();
                var range=sel.getRangeAt(0);
                range.setStartAfter(p);
                range.setEndAfter(p);
                sel.removeAllRanges();
                sel.addRange(range);
            }

        }
        if (e.keyCode === 9) { // tab key
            e.preventDefault();  // this will prevent us from tabbing out of the editor

            // now insert four non-breaking spaces for the tab key
            var editor = document.getElementById("complex_text");
            var doc = editor.ownerDocument.defaultView;
            var sel = doc.getSelection();
            var range = sel.getRangeAt(0);

            var tabNode = document.createTextNode("\u00a0\u00a0\u00a0\u00a0");
            range.insertNode(tabNode);

            range.setStartAfter(tabNode);
            range.setEndAfter(tabNode);
            sel.removeAllRanges();
            sel.addRange(range);
        }

    });
    $("#cke_21").click(function(){

    });
    $('#complex_text').change(function(){
        //$("#complex_text span:empty").remove();
    });
    if($("#long_post_data").html()!=''){
        $("#long_post_data").sortable();
    }
    var long_post_empty=true;
    var add_click_f_click=true;
    $("#long_post_buttons .add_text_button").click(function(){
        $("#long_post_video_form").hide();
        $("#long_post_text_form").show();
        if(add_click_f_click&&!isMobile){
            $("#long_post_text").attr('contenteditable','true');
            var name;
            for(name in CKEDITOR.instances) {
                var instance = CKEDITOR.instances[name];
                if(this && this == instance.element.$) {
                    instance.destroy();
                }
            }
            if (CKEDITOR.instances['long_post_text']) {
               CKEDITOR.instances['long_post_text'].destroy(true);
            }
            CKEDITOR.inline( 'long_post_text' );
            CKEDITOR.disableAutoInline = true;
            CKEDITOR.config.autoParagraph = false;
            CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
            $("#long_post_text").html("");
            add_click_f_click=false;
        }
        $(".long_post_data").sortable();
    });
    var long_post_image_index=0;
    $("#long_post_data img").each(function(){
        if($(this).get(0).hasAttribute('index')){
            if(parseInt($(this).attr('index'))>long_post_image_index){
                long_post_image_index=parseInt($(this).attr('index'));
            }
        }
        $(this).attr('old',1);
    });
    long_post_image_index++;
    $("#long_post_buttons .add_image_button").click(function(){
        $("#add_image_popup").show();
        if(!isMobile)  $("#layer2").show(); 
    });
    $("#add_image_popup #upload_image_popup").click(function(){
        $("#add_image_popup").hide();
        if(!isMobile)  $("#layer2").hide(); 
        var input_file="<input type='file' accept='image/*' id='long_post_image_file"+long_post_image_index+"' class='long_post_image_file' name='image"+long_post_image_index+"' image_index="+long_post_image_index+" style='display:none;'>";
        $("#long_post form").append(input_file);
        $("#long_post_image_file"+long_post_image_index).trigger('click');
        long_post_image_index++;
    });
    $("#load_from_giphy").click(function(){
         $("#giphy_images").show();
     });
     var giphy_offset=0;
     $("#giphy_images button").click(function(){
         giphy_offset=0;
         var text=$("#giphy_images input").val();
         if(text!=''){
            $.post('/upload.php?giphy=1',{query:text},function(data){
                $("#giphy_images_blok").html(data);
            }); 
         }
     });
    var video_opening=false;
    $("body").on('mouseover','.giphy_video_image',function(){
        if(video_opening){
            return;
        }
        video_opening=true;
        $(".giphy_video2").each(function(){
            $(this).get(0).pause();
        });
        var video=$(this).attr('video_src');
        var img_src=$(this).attr('src');
        var video="<video class='giphy_video2' style='height:200px;z-index:1000;position:absolute;top:0;left:0;'  preload='none' loop='none'  webkit-playsinline  class='giphy_video' autoplay>"+
            "<source img_src='"+img_src+"' src='"+video+"' type='video/mp4;' /></video>";
        //$(this).get(0).play();
        $(this).parent().append(video);
        $(this).attr('class','giphy_video_image_playing');        
        video_opening=false;
        $("#long_post_data").sortable();
    });
    $("body").on('mouseover','.giphy_video2',function(){
        $(this).get(0).play();
    });
    $("body").on('mouseleave','.giphy_video2',function(){
        $(this).get(0).pause();
    });
    $('body').on('click','.imgur_mobile',function(){
        $(this).parent().find('button').show();
    });
    $("body").on('click','.giphy_video_mobile,.gif_play_button2',function(){        
        if($(this).hasClass('playing')){
            $(this).parent().find('img').show();
            $(this).parent().find('.giphy_video_mobile').remove();
            $(this).parent().find('.giphy_video_mobile').removeClass('playing');
            $(this).parent().find('.gif_play_button2').show();
        }else{
            $("#giphy_images_blok").find("video").each(function(){
                $(this).parent().find('img').show();
                $(this).remove();
                $(this).parent().find('button').hide();
                $(this).parent().find('.gif_play_button2').show();
            });
            $(this).parent().find('.gif_play_button2').hide();
            var video_src=$(this).parent().find('button').attr('video_src');
            var video="<video preload autoplay loop='true'  webkit-playsinline  class=''><source src='"+video_src+"' type='video/mp4;' /></video>";
            $(this).parent().prepend(video);
            $(this).parent().find('.giphy_video_mobile').hide();            
            $(this).parent().find('video').get(0).play();
            $(this).parent().find('button').show();
            $(this).parent().find('.giphy_video_mobile').addClass('playing');
        }        
    });
    //mobile
    $("body").on('click','#giphy_images_blok button',function(){
        var img_src=$(this).attr('img_src');
        var video_src=$(this).attr('video_src');
        $("#add_image_popup").hide();
        if(video_src!=''){
            var html="<div class='long_post_part image image_giphy'><span class='long_post_delete_button'></span>"+
            "<img class='pass gif_image'   src='"+img_src+"' />"+
            "<img class='gif_play_button' video_src='"+video_src+"'  src='/images/icons/play_icon.png' />"+            
            " </div>";
        }else{
            var html="<div class='long_post_part image image_giphy'><span class='long_post_delete_button'></span>"+
            "<img class='pass '   src='"+img_src+"' />"+        
            " </div>";
        }
       
        $("#long_post_data").append(html);
        if(long_post_empty){
            $("#long_post_empty").hide();
            long_post_empty=false;
        }
        $("#long_post_data").sortable();
    });
    var giphy_load=false;
    $('#giphy_images_blok').on('scroll', function() {
        if($(this).scrollTop() + $(this).innerHeight() +50>= $(this)[0].scrollHeight) {
        giphy_offset+=15;
         var text=$("#giphy_images input").val();
         if(text!=''){
             if(!giphy_load){
                 giphy_load=true;
                 $.post('/upload.php?giphy=1',{query:text,offset:giphy_offset},function(data){
                    $("#giphy_images_blok").append(data);
                     giphy_load=false;
                }); 
             }            
         }
        }
    });
     $('#giphy_images_blok').on('scrollstop', function() {
        if($(this).scrollTop() + $(this).innerHeight() +50>= $(this)[0].scrollHeight) {
        giphy_offset+=15;
         var text=$("#giphy_images input").val();
         if(text!=''){
             if(!giphy_load){
                 giphy_load=true;
                 $.post('/upload.php?giphy=1',{query:text,offset:giphy_offset},function(data){
                    $("#giphy_images_blok").append(data);
                     giphy_load=false;
                }); 
             }
         }
        }
    });
    //pc
    $("body").on('click','.giphy_video2',function(){
        $("#add_image_popup").hide();
        $("#layer2").hide(); 
        var img_src=$(this).find('source').attr('img_src');
        var video_src=$(this).find('source').attr('src');
        var html="<div class='long_post_part image image_giphy'><span class='long_post_delete_button'></span>"+
            "<img class='pass gif_image'   src='"+img_src+"' />"+
            "<img class='gif_play_button' video_src='"+video_src+"'  src='/images/icons/play_icon.png' />"+            
            " </div>";
        $("#long_post_data").append(html);
        if(long_post_empty){
            $("#long_post_empty").hide();
            long_post_empty=false;
        }
        $("#long_post_data").sortable();
    });
    $("body").on('click','.imgur_image',function(){
        $("#add_image_popup").hide();
        $("#layer2").hide(); 
        var img_src=$(this).attr('src');
        var html="<div class='long_post_part image'><span class='long_post_delete_button'></span>"+
            "<img class='pass '   src='"+img_src+"' />"+        
            " </div>";
        $("#long_post_data").append(html);
        if(long_post_empty){
            $("#long_post_empty").hide();
            long_post_empty=false;
        }
        $("#long_post_data").sortable();
    });
    var long_post_video_index=0;
    $("#long_post_data video").each(function(){
        if($(this).get(0).hasAttribute('index')){
            if(parseInt($(this).attr('index'))>long_post_video_index){
                long_post_video_index=parseInt($(this).attr('index'));
            }
        }
        $(this).attr('old',1);
    });
    long_post_video_index++;
    $("#upload_video_long_post").click(function(){
        var input_file="<input type='file' accept='video/*' id='long_post_video_file"+long_post_video_index+"' class='long_post_video_file' name='video"+long_post_video_index+"' video_index="+long_post_video_index+" style='display:none;'>";
        $("#long_post form").append(input_file);
        $("#long_post_data").sortable();
        $("#long_post_video_file"+long_post_video_index).trigger('click');
        long_post_video_index++;
    });
    $("body").on('change','#long_post .long_post_video_file',function(){
        var video_index=$(this).attr('video_index');
        var html="<div class='long_post_part video_uploaded'><span class='long_post_delete_button' index="+video_index+"></span>"+
            "<video id='video_player"+video_index+"' index="+video_index+" controls ></video>";
        $("#long_post  .loading").hide();
        if(long_post_empty){
            $("#long_post_empty").hide();
            long_post_empty=false;
        }
        $("#long_post form #long_post_data").append(html);
        $("#long_post_data").sortable();
        var file = this.files[0]
        var type = file.type
        var videoNode = document.querySelector('#video_player'+video_index)
        var canPlay = videoNode.canPlayType(type)
        if (canPlay === '') canPlay = 'no'
        var message = 'Can play type "' + type + '": ' + canPlay
        var isError = canPlay === 'no'
        

        var fileURL = URL.createObjectURL(file)
        videoNode.src = fileURL
        return;
    });
    $("body").on('focus',".text_overlay",function(){
        if($(this).attr('has_text')==0){
            $(this).html("");
        }
        
    });
    $("body").on('blur',".text_overlay",function(){
        var html=$(this).html().replace('<br></br>','');
        if($(this).text()==''){
             $(this).css('opacity','0.3');
             $(this).html("Add text");
            $(this).attr('has_text',0);
        }else{
            $(this).css('opacity','0.8');
            $(this).attr('has_text',1);
        }
    });
    
    $("body").on(' mousedown',".text_overlay",function(){
        //$(this).css('left','auto');
        //console.log(left);
    });
    $("body").on('change','#long_post .long_post_image_file',function(){
        var image_index=$(this).attr('image_index');
        var html="<div class='long_post_part image image_uploaded'><span class='long_post_delete_button' index="+image_index+"></span>"+
            "<img   src=''   index="+image_index+" />"+
            "<canvas id='canvas"+image_index+"'   style='background-color: rgb(255, 255, 255);  margin-left: 2px; margin-top: 5px;'></canvas>"+
            "<div class='text_overlay' has_text=0 contenteditable='true'>Add text</div> <div class='text_overlay top' has_text=0 contenteditable='true'>Add text</div> </div>";
        $("#long_post  .loading").hide();
        if(long_post_empty){
            $("#long_post_empty").hide();
            long_post_empty=false;
        }
        $("#long_post form #long_post_data").append(html);
        $("#long_post_data").sortable();
        $(".text_overlay").draggable({
            start: function() {
                $(this).css({ width: 'auto' });
            },
            stop: function() {
                $(this).css({ width: 'auto' });
            }
        });
        var canvas=$("#canvas"+image_index);
        var width=canvas.width();
        console.log(width);
        var context = canvas.get(0).getContext("2d");
        if ( this.files && this.files[0] ) {
            var FR= new FileReader();
            FR.onload = function(e) {
               var img = new Image();
               img.onload = function() {
                console.log(img.width);
                console.log(img.height);
                    //canvas.css('height',650*img.height/img.width);
                    canvas.get(0).height=width*img.height/img.width;
                    canvas.get(0).width=width;
                    context.drawImage(img, 0, 0,width,width*img.height/img.width);
               };
                img.src = e.target.result;
            };       
            FR.readAsDataURL( this.files[0] );
        }
        return;
    });
    function getImgWidth(url) {
        var img = new Image();
        img.src = url;
        img.onload = function() {
            if(this.width<200){
                var src=$(".long_post_part.video").last().find('.youtube_image').attr('src').replace('maxresdefault.jpg','hqdefault.jpg');
                $(".long_post_part.video").last().find('.youtube_image').attr('src',src);
                if(this.width/this.height<1.5){
                    $(".long_post_part.video").last().find('.youtube_image').css('margin','-10% 0').addClass('youtube_image2');
                }
            }
        }
    }
  video_index=0;
    $("#long_post_video_form #add_video_long_post").click(function(){
        var url=$("#long_post_video").val();
        if(url.match(/.mp4$/)||url.match(/.webm$/)){
            video_index++;
            if(long_post_empty){
                $("#long_post_empty").hide();
                long_post_empty=false;
            }
            var html="<div class='long_post_part video_uploaded'><span class='long_post_delete_button' index="+video_index+"></span>"+
            "<video id='video_player"+video_index+"' index="+video_index+" controls src='"+url+"'></video>";
            $("#long_post_data").append(html).sortable();
          return;
        }
        var myId=getYoutubeId(url);
        if(myId!='error'){
//             iframe_code='<div class="long_post_part video"><span class="long_post_delete_button"></span>'+
//             '<iframe width="100%" height="100%" src="//www.youtube.com/embed/' + myId + '" frameborder="0" allowfullscreen></iframe></div>';
            var html='<div class="long_post_part video youtube_video"><span class="long_post_delete_button"></span>'+
                '<img class="youtube_play" v_id='+myId+' src="http://exifeed.com/images/icons/play-button-icon-png-6.png">'+
                '<span class="youtube_name">Youtube name</span>'+
                '<img class="youtube_image"  src="http://img.youtube.com/vi/'+myId+'/maxresdefault.jpg"></div>';
            $("#long_post form #long_post_data").append(html);
            $.get('/ajax_submit.php?youtube_name=1&youtube_id='+myId,{},function(data){
                $(".long_post_part.video").last().find('.youtube_name').text(data);
            });
            getImgWidth("http://img.youtube.com/vi/"+myId+"/maxresdefault.jpg");
            if(long_post_empty){
                $("#long_post_empty").hide();
                long_post_empty=false;
            }
        }
        else{
            var regExp = /^.*(vimeo\.com\/)((channels\/[A-z]+\/)|(groups\/[A-z]+\/videos\/))?([0-9]+)/;
            var match = url.match(regExp);
            if (match){
                iframe_code='<div class="long_post_part video"><span class="long_post_delete_button"></span><iframe width="100%" height="100%" src="http://player.vimeo.com/video/'+match[5]+'" webkitallowfullscreen="webkitallowfullscreen"'+
                'mozallowfullscreen="mozallowfullscreen" allowfullscreen="allowfullscreen" frameborder="0"></iframe></div>';
                $("#long_post form #long_post_data").append(iframe_code);
                if(long_post_empty){
                    $("#long_post_empty").hide();
                    long_post_empty=false;
                } 
            }else{
                var t=0;
                var n=0;
                if(url.indexOf('coub.com/embed/')>0){
                    t=url.indexOf('coub.com/embed/');
                    n='coub.com/embed/'.length;
                }else{
                    if(url.indexOf('coub.com/view/')>0){
                        t=url.indexOf('coub.com/view/');
                        n='coub.com/view/'.length;
                    }
                }
                if(t>0){
                    if(url.indexOf('?')>0){
                        var t1=url.indexOf('?');
                        var s=url.slice(t+n,t1);
                    }else{
                        var s=url.slice(t+n);
                    }
                    if(long_post_empty){
                        $("#long_post_empty").hide();
                        long_post_empty=false;
                    }
                    iframe_code='<div class="long_post_part video coub_video"><span class="long_post_delete_button"></span>'+
                    '<iframe id="coubVideo" src="http://coub.com/embed/'+s+'?muted=false&autostart=false&originalSize=true&hideTopBar=false&startWithHD=false" allowfullscreen="true" frameborder="0" width="100%" height="100%"></iframe>'+
                    '</div>';
                    //$("#long_post form #long_post_data").append(iframe_code);
                    $.post("/ajax_submit.php?get_coub_info",{coub_id:s},function (data) {
                        var array=JSON.parse(data);
                        var img=array['picture'];
                        var html='<div class="long_post_part video coub_video"><span class="long_post_delete_button"></span>'+
                            '<img class="coub_play" v_id='+s+' src="http://www.exifeed.com/images/icons/play-button-icon-png-6.png">'+
                            '<span class="coub_name">Youtube name</span>'+
                            '<img class="coub_image"  src="'+img+'"></div>';
                        $("#long_post form #long_post_data").append(html);
                        $(".long_post_part.video").last().find('.coub_name').text(array['title']);

                    }); 

                }                         
            }
        }
        $("#long_post_data").sortable();

    });
    $("#long_post_buttons .add_video_button").click(function(){
        $("#long_post_video_form").show();
        $("#long_post_text_form").hide();
    });
    $(".long_post_button").click(function(){
        $("#long_post .error_message").hide();
    });
    $("#add_text_long_post").click(function(){
  var autolinker = new Autolinker({className:'autolink',
    replaceFn : function( match ) {
        console.log( "href = ", match.getAnchorHref() );
        console.log( "text = ", match.getAnchorText() );
console.log(match.getType());
        switch( match.getType() ) {
            case 'url' :
                console.log( "url: ", match.getUrl() );
                return true;  // let Autolinker perform its normal anchor tag replacemen              
        }
    }
});
      
        var image_link_replace="</div><div class='long_post_part image'><span class='long_post_delete_button'></span><img src='$1' alt=''></div><div class='long_post_part text'><span class='long_post_delete_button'></span>";
        var text=$("#long_post_text").html().replace(/<br>/,'<br> ').replace(/<div>/gi,'').replace(/<\/div>/gi,'').replace(/(http:\/\/[\w\-\.]+\.[a-zA-Z]{2,5}(?:\/\S*)?(?:[\w])+\.(?:jpg|png|gif|jpeg|bmp))/ig, image_link_replace);     
      var text = autolinker.link( text.replace('&lt;a','<a').replace('&gt;','>').replace('&lt;/a&gt;','</a>') );  
      if(text!=''){
            if(long_post_empty){
                $("#long_post_empty").hide();
                long_post_empty=false;
            }
            $("#long_post form #long_post_data").append("<div class='long_post_part text'><span class='long_post_delete_button'></span>"+text+"</div>");  
        console.log($("#long_post #long_post_data .long_post_part").last().html());
            $("#long_post #long_post_data .long_post_part").last().find('a').each(function(){
              console.log('456');
              //$(this).after('<div class="long_post_part link"><span class="long_post_delete_button"></span><a href="'+$(this).attr('href')+'" class="autolink" target="_blank" rel="noopener noreferrer">'+$(this).text()+'</a>llll</div>');
              var t=$(this).parent().parent();
             // $(this).remove();
              var html=t.html().replace('<div class="long_post_part link">','</p></div><div class="long_post_part link">').replace('llll</div>','</div><div class="long_post_part text"><span class="long_post_delete_button"></span>');
             // t.parent().html(html);
            });
            $("#long_post_data").sortable();
            clearEmptyText();
        }
    });
    $('#long_post').on('click','.long_post_part .long_post_delete_button',function(){
        if($(this).parent().hasClass('image')){
            var i=$(this).attr('index');
            $("#long_post_image_file"+i).remove();
        }
        $(this).parent().remove();
        if($("#long_post form #long_post_data .long_post_part").length==0){
            $("#long_post_empty").show();
            long_post_empty=true;
        }
    });
    function getYoutubeId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }
    var top=$("#complex_text").position().top;
});
function CheckTextForm(){
    var text=$('#text_form .Text').val();
    var description=$('#text_form .Description').val();
    var tags=$('#text_form .tags').val();
    var title=$('#text_form .Title').val();
    var pattern=/[0-9a-zA-Z]+,[0-9a-zA-Z]+[0-9a-zA-Z,]+/g;
    if(title!=''&&text!=''){
        return true;
    }
    else{
        $('#text_form #title_confirmed').text('');
        $('#text_form #text_confirmed').text('');
        $('#text_form #tags_confirmed').text('');

        if(title.length==''){$('#text_form #title_confirmed').text('Title is empty');}
        if(text==''){$('#text_form #text_confirmed').text('Text is empty');}
        //if(!pattern.test(tags)){$('#text_form #tags_confirmed').text('Tags');}
        return false;
    }
}
function CheckImageForm(){
    var title=$('.image_title').val();
    var description=$('.image_description').val();
    var tags=$('.image_tags').val();
    var image=$(".image_file").val();
    if(title!=''&&image){
        return true;
    }
    else{
        $('#text_form #title_confirmed').text('');
        $("#image_form #file_confirmed").text('');

        if(title.length==''){$('#image_form #title_confirmed').text('Title is empty');}
        if(!image){$("#image_form #file_confirmed").text("File is empty");}
        return false;

    }
}
function CheckLinkForm(){
    var title=$('.video_title').val();
    var description=$('.video_description').val();
    var tags=$('.video_tags').val();
    var link=$(".video_link").val();
    if(title!=''&&link!=''){
        return true;
    }
    else{
        $('#text_form #title_confirmed').text('');
        $('#video_form #link_confirmed').text('');
        if(title.length==''){$('#video_form #title_confirmed').text('Title is empty');}
        if(link.length==''){$('#video_form #link_confirmed').text('Video url is empty');}
        return false;

    }
}
function CheckLongPostForm(){
    var text=$('#long_post form #long_post_data').html();
    var title=$('#long_post form .Title').val();
    if(title!=''&&text!=''){
        return true;
    }
    else{
        $('#long_post form #title_confirmed').text('');
        $('#long_post form #data_confirmed').text(''); 

        if(title.length==''){$('#long_post form #title_confirmed').show().text('Title is empty');}
        if(text==''){$('#long_post form #data_confirmed').show().text('Post is empty');}
        //if(!pattern.test(tags)){$('#text_form #tags_confirmed').text('Tags');}
        return false;
    }
}
function CheckComplexForm(){
    $('#complex_form #title_confirmed ').text("");
    $('#complex_text_confirmed').text("");
    var title=$('.complex_form_title').val();
    var tags=$('.complex_form_tags').val();
    var text=$("#complex_text").html();
    if(title!=""&&text!=''){
        return true;
    }
    else{
        if(title.length==0){$('#complex_form #title_confirmed ').text('Title is empty');}
        if(text.length==0){$('#complex_text_confirmed').text('Text is empty');}
        return false;

    }
}

function IsURL(url) {

    var strRegex = "^((https|http|ftp|rtsp|mms)?://)"
        + "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" //ftp的user@
        + "(([0-9]{1,3}\.){3}[0-9]{1,3}" // IP形式的URL- 199.194.52.184
        + "|" // 允许IP和DOMAIN（域名）
        + "([0-9a-z_!~*'()-]+\.)*" // 域名- www.
        + "([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\." // 二级域名
        + "[a-z]{2,6})" // first level domain- .com or .museum
        + "(:[0-9]{1,4})?" // 端口- :80
        + "((/?)|" // a slash isn't required if there is no file name
        + "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
     var re=new RegExp(strRegex);
     return re.test(url);
 }

function clearEmptyText(){
  $(".long_post_part.text").each(function(){
    console.log($(this).text().length);
    if($(this).text()==''||$(this).text().length==0){
      $(this).remove();
    }
  });
}