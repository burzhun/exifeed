$(window).ready(function () {

    $("#idea_add_form button").click(function(){
        var text=$("#idea_add_form textarea").val();
        if(text!=''){
            $.post('/ajax_submit.php',{useridea:text},function(data){
                $("#idea_add_form").after(data);
            });
        }

    });

    $("#user_ideas .idea_upvote").click(function(){
        var id=$(this).attr('idea_id');
        $.post('/ajax_submit.php',{user_idea_id:id},function(data){
            $("#idea_add_form").after(data);
        });
    });
});