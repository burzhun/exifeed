
$(document).bind("mobileinit", function () {
    $.extend(  $.mobile , {
      ajaxFormsEnabled : false,
      ajaxLinksEnabled : false,
      ajaxEnabled : false,
      ignoreContentEnabled : true,
      pushStateEnabled : false
    });
});
$(document).on('mobileinit', function () {
    // settings
    $.mobile.ignoreContentEnabled = true;
  $.mobile.keepNative = "select,input"; 
    $.mobile.defaultPageTransition = "slide";
    $.event.special.swipe.horizontalDistanceThreshold = 10;
// this is the amount of vertical leeway you get before your horizontal swipe is NOT treated as a horizontal swipe - default is 75px. Increase this.
$.event.special.swipe.verticalDistanceThreshold = 100;
// this is the amount of time you get to drag your finger across and make the thresholds - default is 1000 = 1s. Increase this.
$.event.special.swipe.durationThreshold = 2000;
});
$(document).ready(function() {
    
    window.location.hash="";
   // $.mobile.zoom.disable();
    var h=0; 
    $("#post-shortlink").remove();
    $("#show_header_mobile").click(function () {
        if ($("#left_menu").position().left > -10) {
            hide_menu();
            //$('#layer').css('opacity', '0');
            //$('#layer').css('z-index', -10);
        }
        else {
            show_menu();
            //$('#layer').css('opacity', '0.6');
            //$('#layer').css('z-index', 100);
        }
    });
    $('#layer').click(function () {
        $('#layer').css('opacity', '0');
        $('#layer').css('z-index', -10);
        $(".share_buttons_list_mobile").hide();
    });
    $(document).on('swiperight', function (e) {
        if(window.location.hash!='#post'){
            e.stopImmediatePropagation();
            if ($("#left_menu").position().left<-10) {
                show_menu();
                return false;
            }
        }
        
    });
    $("body").on('click','.login_link',function(){
        $(".login_mobile").click();
    });
    $("body").on('click','.register_link',function(){
        $(".registration_mobile").click();
    });
    function show_menu(){
        $("#left_menu").animate({left: '0'}, "slow");
        $("#mainbody").animate({left: '150px'}, "slow");
    }
    function hide_menu(){
        $("#left_menu").animate({left: '-150px'}, "slow");
        $("#mainbody").animate({left: '0'}, "slow");
    }
    $('#left_menu,#mainbody *').on('swipeleft', function (e) {
        if ($("#left_menu").position().left >=0) {
            hide_menu();
        }
    });
    
    $("body").on('click','.parent_comment_container',function(e){
        var is_opened=$(this).attr('is_opened');
        if(is_opened=='0'){
            $(this).css('max-height','500px');
            $(this).find('.parent_comment_image').css('max-height','300px');
            $(this).attr('is_opened','0');
            $(this).find('.gif_play_button').show();
        }
        else{
            $(this).find('.gif_play_button').hide();
            $(this).css('max-height','30px');
            $(this).find('.parent_comment_image').css('max-height','30px');
            $(this).attr('is_opened','1');
        }
    });
    var add_comments_css=true;
    var current_hash="";
    var post_mobile_comments_interval=null;
    /*$('#mainbody ').on('swipeleft','.post', function (e) {
        if (e.swipestart.coords[0] > 250&&$("#left_menu").position().left <-50) {            
            //$("#left_menu").hide();
            $("#post_view_div").animate({left: '0'}, "slow");
            var html=$(this).html();
            $("#post_view_div #post_view_container .post").html(html);
            post_id=$(this).attr('post_id');
            window.location.hash='#post';
            $.post('/ajax_submit.php?ajax_comments_div=1',{post_id:post_id},function(data){
                $("<script> var post_id="+post_id+"</script>").appendTo("#post_view_div");
                $('#post_view_div #post_view_container').after(data);
                if (data.length > 0) {
                    loading = false;
                }

            });
        }
    });*/
    $("body").on('click','.post_data',function(e){
        
        if($("#left_menu").position().left >-40){
            hide_menu();
            return;
        }
        var ignore_classes=['gif_image','comment_number_image','gif_video','gif_playing','youtube_play','coub_play','gif_play'];
        if(post_id==null){
            var tag=e.target.tagName.toLowerCase(); 
            var classname=e.target.className;
            if (e.target.tagName.toLowerCase()!='a'&&tag!='SPAN'&&tag!='mobile_share_image'&&tag!='show_full_button'&&tag!='video') {
                if(!$(e.target).parents('.post_complex_text ').length){
                    return;
                }
                for(var i=0;i<=ignore_classes.length;i++){
                    if(classname.indexOf(ignore_classes[i])>=0){
                        return true;
                    }
                }
                var html=$(this).parent().html();
                $(".not_logged_comments_div").remove();
                $("#post_view_div #post_view_container .post").html(html);
                post_id=$(this).find('.post_container').attr('post_id');
                $("#post_view_div").animate({left: '0'}, "slow",function(){                
                    window.location.hash='#post';
                    $.ajax({
                        url:'/ajax_submit.php?ajax_comments_div=1',
                        type:"post", 
                        data:{
                            post_id:post_id 
                        },   
                        success:function (data)
                        {
                            $("<script> var post_id="+post_id+"</script>").appendTo("#post_view_div");
                            $('#post_view_div #post_view_container').after(data);
                            if (data.length > 0) { 
                                loading = false;
                            }
                            post_mobile_comments_interval=setInterval(function(){LoadPostCommentsAjaxMobile(time,post_id,'#post_view_div')},10000);
                        }
                    });
                });        
                return false;
            }
        }
        
    });
  
  if(post_id){
     var loading2=false;
     var offset2=0;
      $(document).ready(function(){
       
         loading2=true;
          $.post("/ajax_submit.php?other_posts=1",{post_id:post_id,offset:offset2},function(data){
            if(data!=''){
              $("#additional_posts").append(data);              
              offset2+=10;
            }
            loading2=false;
          });
       
     });
   }
    window.onhashchange = function() {
        if(current_hash=="#post"&&window.location.hash==""){
            $("#post_view_div").animate({left: '100%'}, "slow");
            $("#post_view_div #add_comment_div").remove();
            $("#post_view_div #comments").remove();
            var offset=$("#post"+post_id).offset().top;
            $('html,body').animate({ scrollTop: offset }, 'fast');
            post_id=null;
            clearInterval(post_mobile_comments_interval);
           // clearInterval(mobileAjaxComments);
        }
        if(current_hash=="#login"&&window.location.hash==""){
            $("#login_mobile_form").hide();
        }
        if(current_hash=="#register"&&window.location.hash==""){
            $("#registration_mobile_form").hide();
        }
        if(current_hash=='#upload_image'){
            $("#add_image_popup").hide();
            $("#layer2").hide();  
        }
        current_hash=window.location.hash;
    }
    $('#post_view_div ').on('swiperight', function (e) {
        var offset=$("#post"+post_id).offset().top;
        $("#post_view_div").animate({left: '100%'}, "slow",function() {
            $('html,body').animate({ scrollTop: offset }, 'fast');
            window.location.hash="";
            post_id=null;
            clearInterval(post_mobile_comments_interval);
        });
        $("#post_view_div #add_comment_div").remove();
        $("#post_view_div #comments").remove();
        post_id=null;
        
        //clearInterval(mobileAjaxComments);
    });
    $("#back_button").click(function(){
        $("#post_view_div").animate({left: '100%'}, "slow");
    });
    $('body').on('click',".mobile_share",function(){
        $(this).parent().find(".share_buttons_list_mobile").show();
        $('#layer').css('opacity', '0.6');
        $('#layer').css('z-index', 650);
    });
    var recent_posts_offset=10;
    var recent_post_loaded=false;
    if($('#recent_posts_footer').length){
        $(window).scroll(function(){
            if(!recent_posts_loading&&($('#recent_posts_footer').offset().top<$(window).scrollTop()+$(window).height())) {
                recent_post_loaded = true;
                var type="recent_posts";                
                $.post('/ajax_submit.php', {offset: recent_posts_offset, type: type}, function (data) {
                    $('#mainbody #recent_posts').append(data);
                  
                    if(data.length>0){                        
                        recent_post_loaded = false;
                        recent_posts_offset+=10;
                    }
                });
            }
        });
    }else{
        recent_post_loaded=true;
    }
    var loading=false;
    var offset=10;
    $(document).off("scroll").on("scroll",function(){
        console.log("12");
        if(recent_post_loaded){
            if(!loading&&($('#footer').offset().top<$(window).scrollTop()+$(window).height())) {
                loading = true;
                var type=$("#footer").attr('page_type');
                var url=window.location.href;
                var n=url.indexOf('?');
                var s="";
                if(n>0){
                    s=url.substring(n);
                }
                var user_id1=0;
                $("#loading_line").show(); 
                if(type =='user') user_id1 = $("#footer").attr('user_id');
                $.post('/ajax_submit.php'+s,{pagetype: type,offset: offset,ajax_user_id:user_id1},function(data){
                    $('#mainbody #content').append(data);                        
                    if (data.length == 0) {
                        $('#mainbody #content').append("<div style='text-align:center'> No more posts</div>");
                    }else{
                        loading = false;
                    }
                    offset+=10;
                    $("#loading_line").hide();
                });
                
            }
        }
    });
    $("#find_button").click(function(){
        var s=$("#find_mobile #find_text").val();
        window.location.href="/find?text="+s;
    });
    $("#find_mobile #find_text").keypress(function(e) {
        if(e.which == 13) {
            var s=$("#find_mobile #find_text").val();
            window.location.href="/find?text="+s;
        }
    });
    $(".menu_link").click(function(){
        var s=$(this).attr("href");
        window.location.href=s;
        return false;
    });
    $("#find_mobile #find_text").focus(function(){
        $("#find_mobile").css('width','65%');
    });
    $("#find_mobile #find_text").blur(function(){
        $("#find_mobile").css('width','42%');
    });
    $(".login_mobile").click(function(){
        $("#login_mobile_form").show();
        window.location.hash="#login";
    });
    $(".registration_mobile").click(function(){
        $("#registration_mobile_form").show();
        window.location.hash="#register";
    });
    $("#login_mobile_form form div.button").unbind('click').click(function(e){
        e.stopPropagation();
        Validate_user();
    });
    $("#login_mobile_form form div.input .text").keypress(function (e) {
        if(e.which == 13) {
            $("#login_mobile_form form div.input .password").focus();
        }
    });
    $("#login_mobile_form form div.input .password").keypress(function (e) {
        if(e.which == 13) {
            Validate_user();
        }
    });
    $("#login_mobile_form form div.input .text").keyup(function(e){
        $("#login_mobile_form form .error").hide();
    });
    $("#login_mobile_form form .back").click(function(){
        $("#login_mobile_form").hide();
        //window.location.href='';
    });
    var $viewportMeta = $('meta[name="viewport"]');
    $('input, select, textarea,#complex_text').bind('focus blur', function(event) {
        $viewportMeta.attr('content', 'width=device-width,initial-scale=1,maximum-scale=' +        (event.type == 'blur' ? 10 : 1));
    });
    $("#registration_mobile_form form input").keypress(function(e){
        if(e.which == 13) {
            $("#registration_mobile_form .button").trigger('click');
        }
    });
    $("#registration_mobile_form form .back").click(function(){
        window.history.back();
    });
    $("#registration_mobile_form form .button").click(function(){
         $("#registration_mobile_form form .error").text('');
        if(Registration_validation()==1){
            var name=$("#registration_mobile_form form div.input .username").val();
            var email=$("#registration_mobile_form form div.input .email").val();
            var password=$("#registration_mobile_form form div.input .password").val();
            $.post('ajax_submit.php?user_exists=1',{email:email,password:password,name:name},function(data){
                if(data=='Ok'){
                    $("#registration_mobile_form form").submit();
                }else{
                    $("#registration_mobile_form form .ajax-error").text(data);
                    $("#registration_mobile_form form .ajax-error").show();
                }
            });
            
        }else{
            $("#registration_mobile_form form .error").show();
        }
    });
}); 
function Validate_user(){
    var name=$("#login_mobile_form form div.input .text").val();
    var password=$("#login_mobile_form form div.input .password").val();
    var captcha='';
    if($("#login_mobile_form form .captcha").length){
        captcha=$("#login_mobile_form form .captcha input").val();
    }
    $.post('/ajax_submit.php?check_user=1',{email:name,password:password,captcha:captcha},function(data){
        if(data=='Ok'){
            location.reload();
        }
        else{
            $("#login_mobile_form form .captcha").remove();
            if(data=='captcha'){
                $.post('/upload.php?captcha=1',{},function(data){                        
                    $("#login_mobile_form form .error").after(data);
                });
            }else{
                $("#login_mobile_form form .error").show();
            }
        }
    });
    return false;
    //$("#login_mobile_form form").submit();
}
function Registration_validation(){
    var name=$("#registration_mobile_form form div.input .username").val();
    var email=$("#registration_mobile_form form div.input .email").val();
    var password=$("#registration_mobile_form form div.input .password").val();
    if(!/^[a-z0-9_]+$/i.test(name)){
        $("#registration_mobile_form form #username_error").text("Username may contain only letters,digits,'_'");
        return 0;
    }
    if(name.length<3||name.length>15){
        $("#registration_mobile_form form #username_error").text("Username may contain form 3 to 15 characters");
        return 0;
    }
    var reg = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i;
    if(!reg.test(email)){
        $("#registration_mobile_form form #email_error").text("Your email is incorrect");
        return 0;
    }
    if(!/^[a-z0-9_]+$/i.test(password)){
        $("#registration_mobile_form form #password_error").text("Pasword may contain only letters,digits,'_' ");
        return 0;
    }
    return 1;
}
var clickTimer = null;
function touchStart() {
    if (clickTimer == null) {
        clickTimer = setTimeout(function () {
            clickTimer = null;
            alert("single");
        }, 500)
    } else {
        clearTimeout(clickTimer);
        clickTimer = null;
        alert("double");
    }
}