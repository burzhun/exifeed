$(document).ready(function() {
    $('#content,.post').on('click', '.upvote', function () {
            var p = $(this).parent();
            var post_id = $(this).attr('post_id');

            if (!$(p).hasClass('upvoted')) {
                if ($(p).hasClass('downvoted')) {
                    $(p).removeClass('downvoted');
                }
                $(p).addClass('upvoted');
                Add_post_rating(post_id, 1);
            }
            $(".post .post_rating .upvote span.text").text('Upvoted');
            $(".post .post_rating .downvote ").text('Downvote');
        }
    );
    $('#content,.post').on('click', '.downvote', function () {
            var p = $(this).parent();
            var post_id = $(this).attr('post_id');
            if (!$(p).hasClass('downvoted')) {
                if ($(p).hasClass('upvoted')) {
                    $(p).removeClass('upvoted');
                }
                $(p).addClass('downvoted');
                Add_post_rating(post_id, -1);
            }
            $(".post .post_rating .upvote span.text").text('Upvote');
            $(".post .post_rating .downvote ").text('Downvoted');
        }
    );
    $('body').on('click','.copy_link',function(){
        var link=$(this).attr('data-link');
        var width=$(this).parent().width()-55;
        $(this).parent().find('input').css('width',width+'px').val(link).show().select();
        $(this).find('span').hide();
    });
    $('#content').on('click', '.save_button', function () {
        var post_id = $(this).parent().attr('post_id');
        $.post('ajax_submit.php?save_post=1', {'post_id': post_id});
        $(this).css('opacity', '0.9');
    });
    $(".parent_link").click(function(){
        var com_id=$(this).attr("parent_comment_id");
        var com=$('#comment'+com_id);
        var top=com.offset().top-100;
        $('html,body').animate({ scrollTop: top }, 'slow');
        $(".comment_author").css('background-color','white');
        com.find(".comment_author").first().css('background-color','yellow');
    });
    //alert(post_id);
    $(".post").each(function(){
        PostCreateEvent($(this));
    });
});
$(document).on('DOMNodeInserted', '.post', function () {
    PostCreateEvent($(this));
});
function PostCreateEvent(post){
    post.find('.text_overlay').each(function(){
        var font_size=$(this).attr('font-size-param')*post.width();
        $(this).css('font-size',font_size+'px');
        //console.log($(this).css('font-size'));
    });
}
function Add_post_rating(post_id,value){
    $.post('/ajax_submit.php?rating=1',{'post_id':post_id,'value':value},function(data){
    });
}
function LoadCommentsAjax(time,post_id){

}