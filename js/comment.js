$(document).ready(function(){
    $('#comments,#post_view_div,#post_pc_view').on('click','#add_subcomment_image_button',function(){
        $(this).parent().find("#subcomment_image_file").trigger('click');
    });
//     $('#add_comment_div,#post_view_div,#post_pc_view').on('click','#load_image_button',function(){  
//         $(this).parent().find("#comment_image_file").trigger('click'); 
//     });
    var comment_button_object=null;
    $("body").on('click',"#post_pc_view #load_image_button,#post_view_div #load_image_button, #add_comment_div #load_image_button",function(){
        $("#add_image_popup").show();
        if(!isMobile)  $("#layer2").show(); 
         comment_button_object=$(this);
        if(isMobile){
            window.location.hash='#upload_image';
            current_hash='#upload_image';
        }
    }); 
    $("#add_image_popup #upload_image_popup").click(function(){
        $("#add_image_popup").hide();
        if(!isMobile) $("#layer2").hide();       
        comment_button_object.parent().find("input[type='file']").trigger('click');
    });
     $("#load_from_giphy").click(function(){
         $("#giphy_images").show();
     });
    var giphy_offset=0;
    var giphy_loading=false;
     $("#giphy_images button").click(function(){
         giphy_offset=0;
         giphy_loading=false;
         $("#giphy_images_blok").scrollTop(0);
         var text=$("#giphy_images input").val();
         if(text!=''){
            $.post('/upload.php?giphy=1',{query:text},function(data){
                $("#giphy_images_blok").html(data);
            }); 
         }
     });
    var video_opening=false;
    $("body").on('mouseover','.giphy_video_image',function(){
        if(video_opening){
            return;
        }
        video_opening=true;
        $(".giphy_video2").each(function(){
            $(this).get(0).pause();
        });
        var video=$(this).attr('video_src');
        var img_src=$(this).attr('src');
        var video="<video class='giphy_video2' style='height:200px;z-index:1000;position:absolute;top:0;left:0;'  preload='none' loop='none'  webkit-playsinline  class='giphy_video' autoplay>"+
            "<source img_src='"+img_src+"' src='"+video+"' type='video/mp4;' /></video>";
        //$(this).get(0).play();
        $(this).parent().append(video);
        $(this).attr('class','giphy_video_image_playing');        
        video_opening=false;
        $("#long_post_data").sortable();
    });
    $("body").on('mouseover','.giphy_video2',function(){
        $(this).get(0).play();
    });
    $("body").on('mouseleave','.giphy_video2',function(){
        $(this).get(0).pause();
    });
    $('body').on('click','.imgur_mobile',function(){
        $(this).parent().find('button').show();
    });
    $("body").on('click','.giphy_video_mobile',function(){        
        if($(this).hasClass('playing')){
            $(this).parent().find('img').show();
            $(this).remove();
            $(this).removeClass('playing');
        }else{
            $("#giphy_images_blok").find("video").each(function(){
                $(this).parent().find('img').show();
                $(this).remove();
                $(this).parent().find('button').hide();
            });
            var video_src=$(this).parent().find('button').attr('video_src');
            var video="<video preload autoplay loop='true'  webkit-playsinline  class=''><source src='"+video_src+"' type='video/mp4;' /></video>";
            $(this).parent().prepend(video);
            $(this).hide();            
            $(this).parent().find('video').get(0).play();
            $(this).parent().find('button').show();
            $(this).addClass('playing');
        }        
    });
    $("body").on('click','#giphy_images_blok button',function(){
        var img_src=$(this).attr('img_src');
        var video_src=$(this).attr('video_src');
        $("#add_image_popup").hide();
        comment_button_object.parent().find('.comment_image_preivew').show().find('img').attr('src',img_src).show();
        comment_button_object.parent().find('#image_source_type').val('giphy');
        comment_button_object.parent().find('#image_url').val(img_src);
        comment_button_object.parent().find('#video_url').val(video_src);
    });
    
    $("body").on('click','.giphy_video2',function(){
        $("#add_image_popup").hide();
        $("#layer2").hide(); 
        var img_src=$(this).find('source').attr('img_src');
        var video_src=$(this).find('source').attr('src');
        comment_button_object.parent().find('.comment_image_preivew').show().find('img').attr('src',img_src).show();
        comment_button_object.parent().find('#image_source_type').val('giphy');
        comment_button_object.parent().find('#image_url').val(img_src);
        comment_button_object.parent().find('#video_url').val(video_src);
    });
    $('#giphy_images_blok').on('scroll', function() {
        if($(this).scrollTop() + $(this).innerHeight() +50>= $(this)[0].scrollHeight) {
            if(!giphy_loading){
                giphy_offset+=15;
                giphy_loading=true;
                var text=$("#giphy_images input").val();
                if(text!=''){
                    $.post('/upload.php?giphy=1',{query:text,offset:giphy_offset},function(data){
                        $("#giphy_images_blok").append(data);
                        giphy_loading=false;
                    }); 
                }
            }
        
        }
    });
     $('#giphy_images_blok').on('scrollstop', function() {
        if($(this).scrollTop() + $(this).innerHeight() +50>= $(this)[0].scrollHeight) {
            if(!giphy_loading){
                giphy_offset+=15;
                giphy_loading=true;
                var text=$("#giphy_images input").val();
                if(text!=''){
                    $.post('/upload.php',{query:text,offset:giphy_offset},function(data){
                        $("#giphy_images_blok").append(data);
                        giphy_loading=false;
                    }); 
                }
            }        
        }
    });
    $('#comments,#post_view_div,#post_pc_view').on('click','span.comment_respond',function(){
        var comment_id=$(this).attr('comment_id');
        var post_id=$(this).attr('post_id');
        var is_opened=$(this).attr('is_opened');
        var level=$(this).attr('level');
        if(is_opened=='0')
        {
            var add_form="<div id='add_comment_respond'>" +
                "<span class='add_comment_respond_title'>Add comment</span>"+
                "<form id='add_comment_respond_form' comment_id='"+comment_id+"' action='/ajax_submit.php' method='post' enctype='multipart/form-data'>"+
                "<div contenteditable='true' id='comment_text_editable'></div>"+
                "<textarea  name='subcomment_text'></textarea><br>"+
                "<input type='hidden' value='"+comment_id+"' name='comment_id' >"+
                "<input type='hidden' value='"+post_id+"' name='post_id' >"+
                "<input type='hidden' id='image_source_type' value='' name='image_source_type'>"+
                "<input type='hidden' id='image_url' value='' name='image_url'>"+
                "<input type='hidden' id='video_url' value='' name='video_url'>"+
                "<div class='comment_image_preivew' style='display:none;position:relative'>"+
                    "<canvas id='canvas'   style='background-color: rgb(255, 255, 255);  margin-left: 2px; margin-top: 5px;display:none;'></canvas>"+
                    "<img src='' width='200px' style='display:none;'>"+
                    "<span class='comment_image_preview_delete'></span>"+
                "</div>  "+
                "<input type='button' class='button' value='Load Image' id='add_subcomment_image_button' >"+
                "<input type='hidden' value='"+level+"' name='level' >"+
                "<input type='file' id='subcomment_image_file' value='Load Image' accept='image/*' title='12' name='subcomment_image' style='display:none;'>"+
                "<input type='submit' class='button' value='Add comment'>"+
                "   </form>"+
                "</div>";
            $(this).after(add_form);
            is_opened='1';
            $(this).text('Cancel');
        }
        else{
            $(this).parent().find('#add_comment_respond').remove();
            is_opened='0';
            $(this).text('Add comment');
        }
        $(this).attr('is_opened',is_opened);
    });
    $("#comments,#post_view_div,#post_pc_view").on('click','.comment_upvote',function(){
        var p=$(this).parent();
        var comment_id=p.attr('comment_id');
        if(!$(p).hasClass('upvoted')){
            if($(p).hasClass('downvoted')){
                $(p).removeClass('downvoted');
            }
            $(p).addClass('upvoted');
            Add_comment_rating(comment_id,1);
        }
        var v=$(this).find('v').text()*1+1;
        $(this).find('v').text(v);
        $(this).find('t').text('Upvoted');
        $(p).find('.downvote').text('Downvote');
    });    
    $("#comments,#post_view_div,#post_pc_view").on('click','.comment_downvote',function(){
        var p=$(this).parent();
        var comment_id=p.attr('comment_id');
        if(!$(p).hasClass('downvoted')){
            if($(p).hasClass('upvoted')){
                $(p).removeClass('upvoted');
            }
            $(p).addClass('downvoted');
            Add_post_rating(comment_id,-1);
        }
        var v=p.find('v').text()*1-1;
        p.find('v').text(v);
        $(this).find('t').text('Upvote');
        $(p).find('.downvote').text('Downvoted');
    });
    $("body").on('change',"#comment_image_file,#subcomment_image_file",function(){
        var canvas=$(this).parent().find("#canvas").show();
        $(this).parent().find('#image_source_type').val('upload');
        var context = canvas.get(0).getContext("2d");
        if ( this.files && this.files[0] ) {
            var FR= new FileReader();
            FR.onload = function(e) {
               var img = new Image();
               img.onload = function() {
                 context.drawImage(img, 0, 0,200,200*img.height/img.width);
                 canvas.parent().css('display','block');
               };
                img.src = e.target.result;
            };       
            FR.readAsDataURL( this.files[0] );
        }
    });    
    $("#post_pc_view,#add_comment_div").on('click','.comment_image_preview_delete',function(){
        $(this).parent().find('img').hide();
        var canvas = $(this).parent().find('#canvas')[0]; // or document.getElementById('canvas');
        canvas.width = canvas.width;
        $(this).parent().hide();
        var text=$("#add_comment_div #comment_text").val();
        document.getElementById("add_comment_form").reset();
        $("#add_comment_div #comment_text").val(text);
        $(this).parent().parent().find('#image_source_type').val('');
    });
    $("body").on('submit',"#add_comment_form",function(e){
        $(this).find(".warning").hide();
        e.preventDefault();
        var text=$(this).find("#comment_text").val();
        var file=$(this).find("#comment_image_file").val();
        var img=$(this).find('#image_url').val();
        if(text.length==0&&file.length==0&&img.length==0){
            $("#add_comment_form .warning").show();
            return false;
        }
        $(this).ajaxSubmit(function(data){
            var p=$(".root_comment[rating=0]");
            $("#comments").append(data);
            document.getElementById("add_comment_form").reset();
        });
        return false;
    });
    $("#comments,#post_view_div,#post_pc_view").on('submit','#add_comment_respond_form',function(e){
        e.preventDefault();
        var comment_id=$(this).attr('comment_id');
       // var text=$(this).find('#comment_text_editable').html();
       // $(this).find('textarea').text(text);
        $(this).ajaxSubmit(function(data){
            if(!isMobile){
                $("#comment"+comment_id).find('.subcomment').first().prepend(data);
            }else{
                $("#comment"+comment_id).after(data);
            }
            $("#add_comment_respond_form").parent().parent().find('.comment_respond').trigger('click'); 
        });
        return false;
    });
    
    
    if(post_id!=null){
        if(!isMobile){
            setInterval(function(){LoadPostCommentsAjaxPc(time,post_id,'')},10000);
        }
        else{
            setInterval(function(){LoadPostCommentsAjaxMobile(time,post_id,'')},10000);
        }
    }
    
    function isTextSelected() {
        var selecttxt = '';
        if (window.getSelection) {
            selecttxt = window.getSelection();
        } else if (document.getSelection) {
            selecttxt = document.getSelection();
        } else if (document.selection) {
            selecttxt = document.selection.createRange().text;
        }
        if (selecttxt == '') {
            return false;
        }
        return true;
    }
    //var mobileAjaxComments=setInterval(function(){LoadPostCommentsAjaxPc(time,post_id)},5000);
    function getSelectionCoords() {
        var sel = document.selection, range, rect;
        var x = 0, y = 0;
        if (sel) {
            if (sel.type != "Control") {
                range = sel.createRange();
                range.collapse(true);
                x = range.boundingLeft;
                y = range.boundingTop;
            }
        } else if (window.getSelection) {
            sel = window.getSelection();
            if (sel.rangeCount) {
                range = sel.getRangeAt(0).cloneRange();
                if (range.getClientRects) {
                    range.collapse(true);
                    if (range.getClientRects().length>0){
                        rect = range.getClientRects()[0];
                        x = rect.left;
                        y = rect.top;
                    }
                }
                // Fall back to inserting a temporary element
                if (x == 0 && y == 0) {
                    var span = document.createElement("span");
                    if (span.getClientRects) {
                        // Ensure span has dimensions and position by
                        // adding a zero-width space character
                        span.appendChild( document.createTextNode("\u200b") );
                        range.insertNode(span);
                        rect = span.getClientRects()[0];
                        x = rect.left;
                        y = rect.top;
                        var spanParent = span.parentNode;
                        spanParent.removeChild(span);
                        // Glue any broken text nodes back together
                        spanParent.normalize();
                    }
                }
            }
        }
        return { x: x, y: y };
    }
    function getSelectionText() {
        var text = "";
        if (window.getSelection) {
            text = window.getSelection().toString();
        } else if (document.selection && document.selection.type != "Control") {
            text = document.selection.createRange().text;
        }
        return text;
    }
    function placeCaretAtEnd(el) {
        el.focus();
        if (typeof window.getSelection != "undefined"
            && typeof document.createRange != "undefined") {
            var range = document.createRange();
            range.setStartAfter( el );
            range.setEndAfter( el );
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        } else if (typeof document.body.createTextRange != "undefined") {
            var textRange = document.body.createTextRange();
            textRange.moveToElementText(el);
            textRange.collapse(false);
            textRange.select();
        }
    }
    function moveCaretToEnd(el) {
    if (typeof el.selectionStart == "number") {
        el.selectionStart = el.selectionEnd = el.value.length;
    } else if (typeof el.createTextRange != "undefined") {
        el.focus();
        var range = el.createTextRange();
        range.collapse(false);
        range.select();
    }
}
    $(".comment_text").mouseup(function  () {
        if(isTextSelected()){
            if(quotes){
                var id=$("#quotes").attr('comment_id');
                $("#comment"+id).find(".comment_respond").first().trigger('click');
            }
            $("#add_comment_respond_form").remove();
            var t=getSelectionCoords();
            var y=t.y-20+$(window).scrollTop();
            var comment_id=$(this).parent().find(".comment_rating").attr('comment_id');
            var text=getSelectionText();
            $("#quotes").css('top',y+'px').css('left',t.x+'px').attr('comment_id',comment_id).attr('comment_text',text).show();
        }
    });
    function pasteHtmlAtCaret(html) {
        var sel, range;
        if (window.getSelection) {
            // IE9 and non-IE
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                range = sel.getRangeAt(0);
                range.deleteContents();
                // Range.createContextualFragment() would be useful here but is
                // non-standard and not supported in all browsers (IE9, for one)
                var el = document.createElement("div");
                el.innerHTML = html;
                var frag = document.createDocumentFragment(), node, lastNode;
                while ( (node = el.firstChild) ) {
                    lastNode = frag.appendChild(node);
                }
                range.insertNode(frag);
                // Preserve the selection
                if (lastNode) {
                    range = range.cloneRange();
                    range.setStartAfter(lastNode);
                    range.collapse(true);
                    sel.removeAllRanges();
                    sel.addRange(range);
                }
            }
        } else if (document.selection && document.selection.type != "Control") {
            // IE < 9
            document.selection.createRange().pasteHTML(html);
        }
    }
    var quotes=false;
    $("#quotes").click(function(){
        var id=$(this).attr('comment_id');
        var text=$(this).attr('comment_text');
        $("#comment"+id).find(".comment_respond").first().trigger('click');
        $("#add_comment_respond_form #comment_text_editable").trigger('click');
        
        var textarea = document.getElementById("comment_text_editable");
        //placeCaretAtEnd(textarea);
        //moveCaretToEnd(textarea);
        $("#add_comment_respond_form #comment_text_editable").focus();
        moveCaretToEnd( document.getElementById("comment_text_editable"));
        pasteHtmlAtCaret("<span class='quote'>"+text+"</span></br><div class='text'>1</div>");
        $("#comment_text_editable").trigger('click');
        $("#comment_text_editable .text ").text('');
        $("#quotes").hide();
        quotes=true;
    });
    
    
    $('body').click(function(e){
        if(quotes&&e.target.id!='quotes'){
            $("#quotes").hide();
            quotes=false;
        }
    });
    $("body").on('click','.comment_delete',function(){
        if (confirm("Are you sure")) {   
            var id=$(this).attr('comment_id');
            $.post('/ajax_submit.php?delete_comment=1',{'delete_comment_id':id});
        }
    });
});
function Add_comment_rating(post_id,value){
    $.post('/ajax_submit.php?rating=1',{'comment_id':post_id,'value':value},function(data){
    });
}
