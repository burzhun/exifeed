$(document).ready(function(){
    $(".user_view").on("click",".subscribe_button",function(){
        var source_id=$(this).attr('source_id');
        var t=$(this);
        $.post('/ajax_submit.php?user_subscribe=1',{source_id:source_id,value:1},function(data){
            if(data=='Ok'){
                t.text('Unsubscribe');
                t.removeClass('subscribe_button');
                t.addClass('unsubscribe_button');
            }
        });
    });
    $(".user_view").on("click",".unsubscribe_button",function(){
        var source_id=$(this).attr('source_id');
        var t=$(this);
        $.post('/ajax_submit.php?user_subscribe=1',{source_id:source_id,value:-1},function(data){
            if(data=='Ok'){
                t.text('Subscribe');
                t.removeClass('unsubscribe_button');
                t.addClass('subscribe_button');
            }
        });
    });
    if (window.location.hash && window.location.hash == '#_=_') {
        window.location.hash = '';
        }
    $('away_link').click(function(){
        var url=encodeURIComponent($(this).attr('href'));
        window.location.href=url;
        return false;
    });
    $('body').on('click','.post_delete',function(){
        var id=$(this).attr('post_id');
        var t=$(this);
        $.post('/ajax_submit.php?delete_post=1',{'post_id':id},function(data){
            if(data=='done'){
                t.parent().parent().parent().remove();
            }
        });
    });
    console.log(Math.round(Math.random()*100).toString());
    $('body').on('click','.share_facebook_button',function(e){
        e.preventDefault();
        var name=$(this).attr('title');
        var description=$(this).attr('desc');
        var image='www.exifeed.com/images/post/'+$(this).attr('image_url')+'?v=1';
        var link='www.exifeed.com/post/id'+$(this).attr('post_id')+'?v=1'+Math.round(Math.random()*100).toString();
        if($(this).attr('image_url')==''){
            FB.ui(
                {
                    method: 'share',
                    name: name,
                    link: link,
                    href: link,
                    caption: 'exifeed.com',
                    description: description,
                    message: ''
                });
        }else{
            FB.ui(
                {
                    method: 'feed',
                    name: name,
                    link: link,
                    picture: image,
                    caption: 'exifeed.com',
                    description: description,
                    message: ''
                });
        }
    });
    $("body").on('click',' .gif_play_button',function(){
        if(!$(this).is('[video_src]')||$(this).attr('video_src')=='nope'){
            var src=$(this).attr('gif_src');
            $(this).parent().find('.gif_image').attr('src',src).addClass('gif_playing');
            $(this).hide();            
        }else{
            var src=$(this).attr('video_src');
            if($(this).parent().find('.gif_video').length>0){
                $(this).parent().find('.gif_video').show();
                $(this).parent().find('.gif_video').get(0).play();  
               // $(this).parent().find(".gif_image").hide();
                $(this).hide();
            }else{
                var video="<video preload loop='true' autoplay webkit-playsinline  class='gif_video'>"+
                        "<source src='"+src+"' type='video/mp4;' /></video>";
                $(this).after(video);
              var v=$(this).parent().find('.gif_video');
              var t=$(this);
              t.hide();                 
              v.on('loadeddata', function(e) {
                //t.parent().find(".gif_image").hide();                
                v.get(0).play();  
              });
            }                            
        }        
    });
    $('body').on('click','.long_post_part.text a',function(e){
        e.preventDefault();
      var url='';
        if(!$(this).attr('href').startsWith('http')&&!$(this).attr('href').startsWith('//')){
            url='http://';

        }
        window.open(url+$(this).attr('href'),'_blank');
        return false;
    });
    $("body").on('click','.gif_playing',function(){
        var src=$(this).attr('src');
        $(this).attr('src',src+'.jpg');
        $(this).parent().find('.gif_play_button').show();
    });
    $("body").on('click','.gif_video',function(e){
        e.preventDefault();
        $(this).hide().get(0).pause();
        $(this).parent().find('.gif_play_button').show();
        $(this).parent().find('.gif_image').show();
    });
    $('body').on('click','.youtube_play',function(){
        var id=$(this).attr('v_id');
        var t=$(this).parent();
        t.find('img').hide();
        t.find('span').hide();
        var iframe='<iframe width="100%" height="100%" src="//www.youtube.com/embed/' + id + '?autoplay=1" frameborder="0" allowfullscreen ></iframe>';
        t.append(iframe);
    });
    $('body').on('click','.coub_play',function(){
        var id=$(this).attr('v_id');
        var t=$(this).parent();
        var w=t.find('.coub_image').width();
        var h=t.find('.coub_image').height();
        t.find('img').hide();
        t.find('span').hide();
        if(isMobile){
           // t.css('padding-bottom','56.25% !important');
            //t.css('height','200px !important')
            t.attr('style','height:0px !importnant;padding-bottom:'+h*100/w+'% !important');
        }
        var iframe='<iframe id="coubVideo" src="http://coub.com/embed/'+id+'?muted=false&autostart=true&originalSize=true&hideTopBar=false&startWithHD=false" allowfullscreen="true" frameborder="0" width="100%" height="100%"></iframe>';
        t.append(iframe);
    });
    $('.tags_view .subscribe_tag').click(function(){
        var tag=$(this).attr("tag");
        $.post('/ajax_submit.php',{subscribe_tag:tag});
    });
    $('.tags_view .block_tag').click(function(){
        var tag=$(this).attr("tag");
        $.post('/ajax_submit.php',{unsubscribe_tag:tag});
    });
    $('#mainbody').on('click','.share_button',function(){
        var clicked=$(this).attr('clicked');
        if(clicked=='0'){
            $('.share_button').attr('clicked','0');
            var x=$(this).offset().left;
            var y=$(this).offset().top;
            var screen_y=y-$(window).scrollTop();
            if($(window).height()-screen_y<180){
                y-=130;
            }
            else{
                y+=30;
            }
            var t=$(this).parent().find('.share_buttons_list');
            var html= t.html();
            var t=$('#social_buttons_container');
            t.html(html);
            t.css('top',y+'px');
            t.css('left',x+'px');
            t.css('display','inline-block');
            $(this).attr('clicked','1');
        }
        else{
            $('#social_buttons_container').css('display','none');
            $(this).attr('clicked',0);
        }
    });
    $('body').on('click','.twitter_link',function(){
        $(this).parent().find('iframe').contents().find('#b').remove();
        $('#b').trigger('click');
        document.getElementById('twitter-widget-0').click();
    });
    $('body').on('click','a.share_twitter_button',function(e){
        //We tell our browser not to follow that link
        e.preventDefault();
        //We get the URL of the link
        var loc = $(this).attr('href');
        //We get the title of the link
        var title  = $(this).attr('title');
        var tags=$(this).attr('tags');
        //We trigger a new window with the Twitter dialog, in the middle of the page
        window.open('http://twitter.com/share?hashtags='+tags+'&url=' + loc + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
    });
    $('#social_buttons_container').on('click','.repost_button',function(){
        var post_id=$(this).attr('post_id');
        $.post('ajax_submit.php?repost=1',{post_id:post_id}); 
    });
    $("body").on('click','.show_full_button',function() {
        $(this).parent().parent().find(".post_container").css('max-height','100000px');
        $(this).remove();
    });
});
var comments_loaded=true;
function LoadPostCommentsAjaxPc(time1,post_id1,prefix){
    if(comments_loaded){
        comments_loaded=false;
        $.post('/ajax_submit.php?ajax_post_comments=1',{time:time1,post_id:post_id1},function(data){
            //alert(data);
            if(data!='No comments'&&data!=''){
                try{
                    data=JSON.parse(data);
                }catch(exception){
                    data=null;
                }
                if(data){
                    var array=data.array;
                    time=data.time;                
                    //alert(array[0].html);
                    for(var i=0;i<array.length;i++){
                        var parent_comment_id=array[i].parent_comment_id;
                        if(parent_comment_id!=0){
                            $(prefix+' #comment'+parent_comment_id).append(array[i].html);
                        }
                        else{
                            $(prefix+' #comments').append(array[i].html);
                        }
                    }
                }
                
            }
            comments_loaded=true;
        });
    }
}
function LoadPostCommentsAjaxMobile(time1,post_id1,prefix){
    if(comments_loaded){
        comments_loaded=false;
        $.post('/ajax_submit.php?ajax_post_comments=1',{time:time1,post_id:post_id1},function(data){
            //alert(data);
            if(data!='No comments'){
                try{
                    data=JSON.parse(data);                    
                }catch(exception){
                    data=null;
                }
                if(data){
                    var array=data.array;
                    time=data.time;                
                    //alert(array[0].html);
                    for(var i=0;i<array.length;i++){
                        var parent_comment_id=array[i].parent_comment_id;                    
                        $(prefix+' #comments').append(array[i].html);                        
                    }
                }
                
            }
            comments_loaded=true;
        });
    }
}