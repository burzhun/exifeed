<?php
ini_set('apc.cache_by_default', 0);
session_start();
require_once( "hybridauth/hybridauth/Hybrid/Auth.php" );
require_once( "hybridauth/hybridauth/Hybrid/Endpoint.php" );
require_once( "hybridauth/hybridauth/Hybrid/User_Profile.php" );
require_once("models/user.php");
header('Content-Type: text/html; charset=utf-8');
$prev_page="";
if(isset($_GET['prev_page'])){
    $prev_page=$_GET['prev_page'];
}
$config = array("base_url" => "http://www.exifeed.com/social_login.php",
    "providers" => array (
        "Facebook" => array (
            "enabled" => true,
            "keys"    => array ( "id" => "119062525124829", "secret" => "023aa52ec3be3c199cc58f7a20b55407" ),
            "scope" => "email, user_about_me, user_birthday, user_hometown"  //optional.
        ),
        "Twitter"=>array(
            "enabled"=>true,
            "keys"    => array ( "key" => "05Sn2C8cpsknIZCFQwXu7JNUS", "secret" => "WHbztAkPtIMxRGwmC6o5AoPJJGcZ7cuK5sAWgAufZ5QsqgrlH7" )
        ),
    ),
    "debug_mode" => true,
    "debug_file" => "log.txt",
);


if (isset($_REQUEST['hauth_start']) || isset($_REQUEST['hauth_done']))
{
    Hybrid_Endpoint::process();
}

//var_dump($_SESSION['user_profile']);
//session_destroy();
if( isset( $_GET["login"] )&&($_GET['login']=="Facebook"||$_GET['login']=="Twitter")) {
    try  {
        $hybridauth = new Hybrid_Auth( $config );
        $adapter = $hybridauth->authenticate( $_GET["login"] );
        $user_profile = $adapter->getUserProfile();
        if(!isset($_SESSION['user_profile'])&&isset($user_profile)){
            $array=(array) $user_profile;
            $identifier=$array['identifier'];
            $provider=$_GET['login'];

            $id=User::check_user_provider($identifier,$provider);
            if($id){
                User::autorize($id);
            }
            else{
                $name=$array['displayName'];
                $email=$array['email'];
                $image=$array['photoURL'];
                $id=User::add_provider_user($provider,$identifier,$email,$name,$image);
                $_SESSION['user']=(array)new User($id);
                User::autorize($id);
            }
            //var_dump($array);
            header("Location:".$prev_page);
        }
    } catch( Exception $e )  {
        die( "<b>got an error!</b> " . $e->getMessage()." error_code". $e->getCode() );
    }
}
if( ! isset( $user_profile ) ) {
    ?>
    <p>Нажмите Войти для авторизации.</p>
    <h2><a href ="social_login.php?login=Facebook">Войти</a></h2>
    <h2><a href ="social_login.php?login=Twitter">Войти</a></h2>
<?php
} else {
    ?>
    <fieldset>
        <legend>twitter данные</legend>
        <b>Привет <?php echo $user_profile->displayName; ?></b>
        <hr />
        <b>Hybridauth access tokens for twitter:</b>
        <pre></pre>
    </fieldset>
<?php
}
?>