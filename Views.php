<?php
require_once('Views/registration_forms.php');
require_once('Views/comments.php');
require_once('Views/insert_form_not_logged.php');
require_once('Views/Mobile/login_mobile.php');
require_once('Views/Mobile/registration_mobile.php');
require_once('models/user.php');
require_once('functions/isMobile.php');
class Views{

    static function header_form(){
        ob_start();
        if(isMobile()){
            include 'Views/Mobile/header_form.php';
        }
        else{
            include 'Views/header_form.php';
        }
        return ob_get_clean();
    }
    static function insert_form($post_html=''){
        ob_start();
        include "Views/insert_form.php";
        return ob_get_clean();
    }
    static  function log_in($error){
        return log_in($error);
    }
    static function log_out(){
        return log_out();
    }
    static function registration_form(){
        return registration_form();
    }

    static function Post($post,$separately,$user_id){
        ob_start();
        if(isset($_GET['1t'])){
            include "Views/post_mini.php";
        }else{
            include "Views/post.php";
        }
        
        return ob_get_clean();

    }
    static function Android_Post($post,$user_id){
        $post_html=self::Post($post,true,$user_id);
        $comments_html=self::Comments($post->id,$post->user_id);
        ob_start();
        include "Views/android_post.php";
        return ob_get_clean();
    }
    static function PostView($post,$separately,$user_id){
        ob_start();
        include "Views/post_view.php";
        return ob_get_clean();

    }
    static function update_post($id,$post){
        ob_start();
        include "Views/upload_post.php";
        return ob_get_clean();
    }
    static function Repost($repost_user,$post,$separately,$user_id){
        $post_html=self::Post($post,$separately,$user_id);
        
        ob_start();
        include "Views/post_views/repost.php";
        return ob_get_clean();
    }
    static function Comments($post_id,$post_author){
        if(isMobile()){
            $user_id='';
            if(isset($_SESSION['user']['id'])){
                $user_id=$_SESSION['user']['id'];
            }
            $list=Comment::comments_list_mobile($post_id);
            ob_start();
            include "Views/Mobile/comments.php";  
            return ob_get_clean();
        }
        else{ 
            $comments=Comment::comments_array($post_id);
            require_once('Views/comments.php');
            $user_id='';
            if(isset($_SESSION['user']['id'])){
                $user_id=$_SESSION['user']['id'];
            }
            return comments($comments,$post_id,$post_author,$user_id);
        }

    }
    static function comment_ajax($comment,$post_author,$user_id,$p_com_id){
        if(isMobile()){
            ob_start();
            if($p_com_id){
                $p_com=new Comment($p_com_id);
            }
            include "Views/Mobile/comment_respond_ajax.php";
            return ob_get_clean();
        }
        else{
            return comment_ajax_pc($comment,$post_author,$user_id);
        }

    }
    static function unresponded_comments($comment){
        return unresponded_comments($comment);
    }
    static function single_comment($id,$user_id,$parent_comment_id,$text,$image,$video,$post_id){
        return single_comment($id,$user_id,$parent_comment_id,$text,$image,$video,$post_id);
    }
    static function Add_comment_div($user_id,$post_id){
        if($user_id!=null){
            ob_start();
            include 'Views/add_comment_div.php';
            return ob_get_clean();
        }else{
            ob_start();
            include 'Views/not_logged_comments_div.php';
            return ob_get_clean();
        }
        
    }
    static function insert_form_not_logged(){
        return insert_not_logged();
    }
    static function search_page_form($text){
        ob_start();
        include 'Views/search.php';
        return ob_get_clean();
    }
    static function user_search($row){
        ob_start();
        include 'Views/user_search.php';
        return ob_get_clean();
    }
    static function login_mobile_form(){
        return login_mobile_form();
    }
    static  function registration_mobile_form(){
        return registration_mobile_form();
    }
    static function settings($id){

        ob_start();
        $user=new User($id);
        $email=$user->email;
        include 'Views/settings.php';
        return ob_get_clean();
    }
   
    static function user_view($id,$t){
        ob_start();  
        $user=new User($id);
        $isSubscribed=$t;
        include 'Views/user_view.php';
        return ob_get_clean();
    }
    static function sign_form(){
        ob_start();
        include 'Views/sign_in_window.php';
        return ob_get_clean();
    }
    static function tags_view($user,$tag,$status){
        ob_start();
        include 'Views/tags_view.php';
        return ob_get_clean();
    }
    static function recent_posts_view($html){
        ob_start();
        include 'Views/recent_post_view.php';
        return ob_get_clean();
    }
    static function log($res){
        ob_start();
        include 'Views/log.php';
        return ob_get_clean();
    }
    static function UserIdeas($res){
        ob_start();
        include 'Views/UserIdeas.php';
        return ob_get_clean();
    }
}