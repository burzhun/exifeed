<?php 
require_once('classes/db.php');
require_once('models/user.php');
require_once('models/post.php');
require_once('functions/image_upload.php');
require_once('functions/sanitize.php');
require_once('includes/cloudinary/Cloudinary.php');
require_once('includes/cloudinary/Uploader.php');
require_once('includes/cloudinary/Api.php');
session_start();
$type="";
if(isset($_GET['type'])){
    $type=$_GET['type'];
}


header('Content-Type: text/html; charset=utf-8');
if(isset($_POST['title'])&&isset($_POST['Text'])&&isset($_POST['tags'])&&isset($_SESSION['user']['id'])&&$type=='text'){
    $db=MyDatabase::getInstance();
    $text=$db->real_escape_string(removeScript($_POST['Text']));
    $title=$db->real_escape_string(removeScript($_POST['title']));
    $tags=$db->real_escape_string($_POST['tags']);
    $description="";
    if(isset($_POST['description'])){
        $description=$db->real_escape_string(url_link(htmlentities($_POST['description'])));
    }
    $user_id=$db->real_escape_string($_SESSION['user']['id']);
    $type='text';
    $image='';
    $url='';
    Post::AddPost($title,$description,$type,$text,$image,$tags,$user_id);
}
if(isset($_POST['title'])&&isset($_FILES['image'])&&isset($_POST['tags'])&&isset($_SESSION['user']['id'])&&$type=='image'){
    $db=MyDatabase::getInstance();
    $description='';
    if(isset($_POST['description'])){
        $description=$db->real_escape_string(url_link(htmlentities($_POST['description'])));
    }
    $tags=$_POST['tags'];
    $title=removeScript($_POST['title']);
    $user_id=$_SESSION['user']['id'];
    $image=$_FILES['image'];
    $ext="";
    switch(exif_imagetype($image['tmp_name']))
    {
        case IMAGETYPE_JPEG: $ext = 'jpg'; break;
        case IMAGETYPE_GIF: $ext = 'gif'; break;
        case IMAGETYPE_PNG: $ext = 'jpg'; break;
    }
    if($ext=='jpg'||$ext=='gif'){
        $id=Post::AddPost($title,$description,'image','','',$tags,$user_id);
        if($id){
            $name=$id.'.'.$ext;
            // print_r($_FILES);
            //echo "<img src='{$image['tmp_name']}'>'";
            compress($image['tmp_name'],'images/post/'.$name,100);
            MyDatabase::UpdateQuery("update post set image='{$name}' where id={$id}");
        }
    }
}
if(isset($_POST['title'])&&isset($_POST['url'])&&$type=='link'){
    $title=removeScript($_POST['title']);
    $link=$_POST['url'];
    $description='';
    if(isset($_POST['description'])){
        $description=$db->real_escape_string(url_link(htmlentities($_POST['description'])));
    }
    $tags=$_POST['tags'];
    $array=parse_url($link);
    $user_id=$_SESSION['user']['id'];
    if($array['host']=='www.youtube.com'||$array['host']=='youtube.com'){
        if(strpos($link,'/watch?v')){
            $step1=explode('v=', $link);
            $step2 =explode('&',$step1[1]);
            $video_id = $step2[0];
            $link='<iframe width="320" height="240" src="https://www.youtube.com/embed/'. $video_id.'" frameborder="0"></iframe>';
        }
        else{
            $step1=explode('embed/', $link);
            $step2 =explode('&',$step1[1]);
            $video_id = $step2[0];
            $link='<iframe width="320" height="240" src="https://www.youtube.com/embed/'. $video_id.'" frameborder="0"></iframe>';
        }
        Post::AddPost($_POST['parent_id'],$title,$description,'video',$link,'',$tags,$user_id);
    }
    if($array['host']=='www.vimeo.com'||$array['host']=='vimeo.com'){
        $link=str_replace('www.','',$link);
        $link = str_replace('http://vimeo.com/','',$link);
        $link = str_replace('https://vimeo.com/','',$link);
        $embedurl = "http://player.vimeo.com/video/".$link;
        $link='<iframe width="320" height="240" src="'.$embedurl.'" webkitallowfullscreen="webkitallowfullscreen"
         mozallowfullscreen="mozallowfullscreen" allowfullscreen="allowfullscreen" frameborder="0"></iframe>';
        Post::AddPost($title,$description,'video',$link,'',$tags,$user_id);
    }
}
if($type=='longpost'&&isset($_POST['long_post_data'])&&isset($_POST['title'])&&User::isLogin()){
    $title=MyDatabase::real_escape_string(removeScript($_POST['title']));
    $description=MyDatabase::real_escape_string(url_link(removeScript($_POST['description'])));
//	echo strlen($_POST['long_post_data']);
	//echo $_POST['long_post_data'];
    $data=html_entity_decode($_POST['long_post_data']);
//	echo $data;
    $data=urldecode($data);
    $data2=removeScript($data);
    $has_script=false;
    $s2=implode(" ",array_diff(explode(" ", $data), explode(" ",$data2)));
    if(preg_match("/script/", $s2)){
        $has_script=true;
    }
	//echo $data2;
	//exit;
    $post_data=url_link(removeScript($data2));
    if($title==''||$post_data==''){
        header("Location:/new");
    }
    //echo $post_data;
  //  exit;
    $tags=MyDatabase::real_escape_string($_POST['tags']);
    $user_id=User::id();
    $parent_id=(int)$_POST['parent_id'];
	if(isset($_GET['post_id'])){
		$post=new Post($_GET['post_id']);      
		if($user_id!=$post->user_id){
			header("Location:/new");
		}else{
			$id=Post::UpdatePost($title,$description,$tags,$post->id);
			$id=$post->id;
		}
	}else{
    	$id=Post::AddPost($parent_id,$title,$description,'complex','','',$tags,$user_id);
	}
    if($id) {
        $doc = new DOMDocument();
        
        $doc->loadHTML('<?xml encoding="UTF-8">' . $post_data);
        $folder = false;
        $folder_video=false;
        foreach ($doc->getElementsByTagName('img') as $img) {
            $index = $img->GetAttribute('index');
            $class = $img->GetAttribute('class');
			$src= $img->GetAttribute('src');
			if($class == 'pass gif_image'){
				if(!preg_match('/http\:\/\/media[0-9]+\.giphy\.com\/media\/[0-9a-zA-Z]+\/giphy_s\.gif/',$src)){
					$img->parentNode->removeChild($img);
				}				
				continue;
			}
			if($class == 'gif_play_button'){
				$video = $img->GetAttribute('video_src');
				if(!preg_match('/http\:\/\/media[0-9]+\.giphy\.com\/media\/[0-9a-zA-Z]+\/giphy\.mp4/',$video)){
					//$img->parentNode->removeChild($img);
				}
				continue;
			}
            if ($src!=''||$img->hasAttribute('old')) {
                continue;
            }
            if (isset($_FILES['image' . $index])) {
                $image = $_FILES['image' . $index];
                $ext = null;
                switch (exif_imagetype($image['tmp_name'])) {
                    case IMAGETYPE_JPEG:
                        $ext = 'jpg';
                        break;
                    case IMAGETYPE_GIF:
                        $ext = 'gif';
                        break;
                    case IMAGETYPE_PNG:
                        $ext = 'jpg';
                        break;
                }
                if ($ext) {
                    if (!$folder) {
                        mkdir("images/post/" . $id);
                        $folder = true;
                    }
                    compress($image['tmp_name'], "images/post/" . $id . '/' . $index . '.' . $ext, 100);                    
                    $class = $img->getAttribute('class');
                    if ($ext == 'gif') {
                        $play_image = $doc->createElement('img');
                        $play_image->setAttribute('gif_src', "/images/post/" . $id . '/' . $index . '.' . $ext);
                        $play_image->setAttribute('src', "/images/icons/play_icon.png");
                        $play_image->SetAttribute('class', 'gif_play_button');
                        $img->parentNode->appendChild($play_image);
                        $img->SetAttribute('src', "/images/post/" . $id . '/' . $index . '.' . $ext . '.jpg');
                        $img->SetAttribute('class', 'gif_image');
                        /*$size=filesize("images/post/" . $id . '/' . $index . '.gif');
                        if($size<1024*1024*10){
                        	$url="http://exifeed.com/images/post/" . $id . '/' . $index . '.gif';
	                        $f=\Cloudinary\Uploader::upload($url,array("format"=>"mp4"));
	                        $video_url=$f['url'];
	                        $play_image->setAttribute('video_src', $video_url);
                        }*/
                        if (!$folder_video) {
                          mkdir("videos/post/" . $id);
                          $folder_video = true;
                        }
											  
                        $cmd='ffmpeg -i images/post/'.$id.'/'.$index.'.gif -c:v libx264 -preset ultrafast -crf 35 -movflags +faststart -pix_fmt yuv420p -strict -2 -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" videos/post/'.$id.'/'.$index.'.mp4';    
                        exec(escapeshellcmd($cmd));
                        $video_url='http://www.exifeed.com/videos/post/'.$id.'/'.$index.'.mp4';
                        $play_image->setAttribute('video_src', $video_url);                    
                        $newimage=imagecreatefromjpeg("images/post/".$id.'/'.$index.'.'.$ext.'.jpg');
                    } else {
                        $newimage=imagecreatefromjpeg("images/post/".$id.'/'.$index.'.'.$ext);
                        $img->SetAttribute('src', "/images/post/" . $id . '/' . $index . '.' . $ext);
                    }
                    $width=imagesx($newimage);
                    $img->parentNode->setAttribute('style','max-width:'.$width.'px');
                } else {
                    $img->parentNode->removeChild($img);
                }
            } else {
				if($img){
					@$img->parentNode->removeChild($img);
				}
                
            }
        }
		$folder = false;
		foreach ($doc->getElementsByTagName('video') as $video) {
			$index = $video->GetAttribute('index');
			if($video->hasAttribute('old')){
				continue;
			}
			if (isset($_FILES['video' . $index])) {
				$video_file = $_FILES['video' . $index];
				$size=$_FILES['video' . $index]['size'];
				$ext=$_FILES['video' . $index]['type'];
				if (($ext=='video/mp4'||$ext=='video/webm')&&$size<30*1024*1024) {
						if (!$folder) {
								mkdir("videos/post/" . $id);
								$folder = true;
						}  
					if(!is_dir("images/post/". $id)){
						mkdir("images/post/" . $id);
					}
					$ext=str_replace('video/','.',$ext);
					move_uploaded_file($video_file['tmp_name'],"videos/post/".$id."/video_".$index.$ext);

					$image_url='images/post/'.$id.'/video_'.$index.'.jpg';
					$cmd=' ffmpeg -ss 0.5 -i videos/post/'.$id.'/video_'.$index.$ext.' -t 1   -f image2 '.$image_url;
					exec(escapeshellcmd($cmd));

					$play_image = $doc->createElement('img');
					$play_image->setAttribute('video_src', "http://www.exifeed.com/videos/post/".$id.'/video_'.$index.$ext);    
					$play_image->setAttribute('src', "/images/icons/play_icon.png");
					$play_image->SetAttribute('class', 'gif_play_button');
					$img = $doc->createElement('img');						
					$img->SetAttribute('src', "/".$image_url);
					$img->SetAttribute('class', 'gif_image');
					list($width, $height) = getimagesize($image_url);
					$video->setAttribute('style','max-width:'.$width.'px');
					$video->parentNode->appendChild($img);		
					$video->parentNode->appendChild($play_image);						
					$video->parentNode->removeChild($video);
				} else {
						$video->parentNode->removeChild($video);
				}
			} else {
				if($video->hasAttribute('src')){
					if (!$folder) {
							mkdir("videos/post/" . $id);
							$folder = true;
					}  
					if(!is_dir("images/post/". $id)){
						mkdir("images/post/" . $id);
					}
					$src=$video->GetAttribute('src');
					$ext=substr($src,strpos($src,'.'));
					$video_url='videos/post/'.$id.'/video_down_'.$index.'.mp4';
					$prev_video_url='videos/post/'.$id.'/prev_video_down_'.$index.'.mp4';
					$file=file_get_contents($src);
					file_put_contents($prev_video_url, $file);
					$cmd='ffmpeg -i '.$prev_video_url.'  -c:v libx264 -preset ultrafast -movflags +faststart -pix_fmt yuv420p -crf 35  -strict -2 -y '.$video_url;    
					exec(escapeshellcmd($cmd));						
					//@unlink($prev_video_url);			
					$image_url='images/post/'.$id.'/video_'.$index.'.jpg';
					$cmd=' ffmpeg -ss 0.5 -i '.$video_url.' -t 1  -f image2 '.$image_url;
					exec(escapeshellcmd($cmd));
					$play_image = $doc->createElement('img');
					$play_image->setAttribute('video_src', "http://www.exifeed.com/".$video_url);    
					$play_image->setAttribute('src', "/images/icons/play_icon.png");
					$play_image->SetAttribute('class', 'gif_play_button');
					$img = $doc->createElement('img');						
					$img->SetAttribute('src', "/".$image_url);
					$img->SetAttribute('class', 'gif_image');
					list($width, $height) = getimagesize($image_url);
					$video->setAttribute('style','max-width:'.$width.'px');
					$video->parentNode->appendChild($img);		
					$video->parentNode->appendChild($play_image);						
					$video->parentNode->removeChild($video);
				}
				elseif($video){
					$doc->removeChild($video);
				}
                
			}
		}
		$doc->removeChild($doc->doctype);
		$doc->removeChild($doc->firstChild);
		$doc->replaceChild($doc->firstChild->firstChild, $doc->firstChild);
		$post_data=$doc->SaveHTML();
		//echo $post_data;
		$post_data=html_entity_decode($post_data);
		$post_data=str_replace('Â','',$post_data);
		$post_data=MyDatabase::real_escape_string($post_data);
		//MyDatabase::SetParameter('post',$id,'complex_text',$post_data,false);
		MyDatabase::SetParameter('post',$id,'text',$post_data,false);		
		$post = new Post($id);
		$post->android_text=MyDatabase::real_escape_string(json_encode($post->getJsonArray()));
		MyDatabase::SetParameter('post',$id,'android_text',$post->android_text,false);
		echo 'post_uploaded';
    }
    
	exit;
}
if(isset($_POST['title'])&&isset($_POST['complex_text'])&&isset($_POST['tags'])&&$type=='complex'&&User::isLogin()){
    $user_id=$_SESSION['user']['id'];
    $title=MyDatabase::real_escape_string(removeScript($_POST['title']));
    $description=MyDatabase::real_escape_string(removeScript(url_link($_POST['description'])));
    $text=$_POST['complex_text'];
    if($title==''){
        header("Location:/new");
    }
    $tags=MyDatabase::real_escape_string($_POST['tags']);
    $parent_id=(int)$_POST['parent_id'];
    $id=Post::AddPost($parent_id.$title,$description,'complex','','',$tags,$user_id);
	//$id=1;
    if($id){
        $doc = new DOMDocument();
        $doc->loadHTML('<?xml encoding="UTF-8">' .$text);
        $folder=false;
        foreach($doc->getElementsByTagName('img') as $img){
            $index=$img->GetAttribute('index');
            $class=$img->GetAttribute('class');
            if($class=='gif_play_button'||$img->hasAttribute('old')){
                continue;				
            }
            if(isset($_FILES['image'.$index])){
                $image=$_FILES['image'.$index];
                $ext=null;
                switch(exif_imagetype($image['tmp_name']))
                {
                    case IMAGETYPE_JPEG: $ext = 'jpg'; break;
                    case IMAGETYPE_GIF: $ext = 'gif'; break;
                    case IMAGETYPE_PNG: $ext = 'jpg'; break;
                }
                if($ext){
                    if(!$folder){
                        mkdir("images/post/".$id);
                        $folder=true;
                    }
                    compress($image['tmp_name'],"images/post/".$id.'/'.$index.'.'.$ext,75);
                    $class=$img->getAttribute('class');
                    if($ext=='gif'){
                        $play_image=$doc->createElement('img');
                        $play_image->setAttribute('gif_src',"images/post/".$id.'/'.$index.'.'.$ext);
                        $play_image->setAttribute('src',"/images/icons/play_icon.png");
                        $play_image->SetAttribute('class','gif_play_button');
                        $img->parentNode->appendChild($play_image);
                        $img->SetAttribute('src',"/images/post/".$id.'/'.$index.'.'.$ext.'.jpg');
                        $img->SetAttribute('class','gif_image');
                        $newimage=imagecreatefromgif("/images/post/".$id.'/'.$index.'.'.$ext);
                    }
                    else{
                        $newimage=imagecreatefromjpeg("/images/post/".$id.'/'.$index.'.'.$ext);                        
                        $img->SetAttribute('src',"/images/post/".$id.'/'.$index.'.'.$ext);
                    }
                    $width=imagesx($newimage);
                    $img->parentNode->setAttribute('style','max-width:'.$width.'px');

                }
                else{
                    $img->parentNode->removeChild($img);
                }
            }
            else{
                $doc->removeChild($img);
            }
        }
        # remove <!DOCTYPE
        $doc->removeChild($doc->doctype);
        $doc->removeChild($doc->firstChild);
        //print_r($_POST);
        # remove <html><body></body></html>
        $doc->replaceChild($doc->firstChild->firstChild, $doc->firstChild);
        $text=$doc->SaveHTML();
        //echo $text;
        $text=html_entity_decode($text);
        $text=str_replace('Â','',$text);
        MyDatabase::SetParameter('post',$id,'complex_text',$text,false);
        $clean_text=clean_complex_text($text);
        MyDatabase::SetParameter('post',$id,'text',$clean_text,false);
		$post->android_text=MyDatabase::real_escape_string(json_encode($post->getJsonArray()));
		MyDatabase::SetParameter('post',$post->id,'android_text',$post->android_text,false);
    }
}
header("Location:/new");