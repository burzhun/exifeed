<?php
function Default1(){       
    //Check token
    if(isset($_GET['check_token535345'])){
        $token=preg_replace('/[^0-9a-z]/','',$_GET['check_token535345']);
        $res=MyDatabase::ReadQuery("select user_id from user_token where token='".$token."'");
        if($res->num_rows>0){
            $row=$res->fetch_assoc();
            $user=new User($row['user_id']);
            if(!$user){
                echo 'nope';
                exit;
            }else{
				$unread_comments_count = $user->UnreadCommentsCount();
                $array=['id'=>$user->id,'username'=>$user->username,'image'=>$user->GetImageLink(),'shorten_post'=>$user->shorten_longpost,'unread_comments'=>$unread_comments_count];
                echo json_encode($array);
                exit;
            }
        }else echo 'nope';
        exit;
    } 
}
function Login(){
	$captcha = false;
	if(isset($_POST['hash'])){
		$code=substr(sha1("somecodehere142".$_POST['captcha']),5,10);
		if(isset($_POST['captcha']) && $code==$_POST['hash']){
			$captcha=true;      
		}else{
			echo '{text:"wrong_captcha",value:""}';
			exit;
		}
	}
	$id=User::checkUserApp($_POST['email'],$_POST['password'],$captcha);
	if($id){
		if($id=='captcha'){
			$_GET['captcha']=1;
			ob_start();
			include("includes/simple-php-captcha/simple-php-captcha.php");
			$rawImageBytes = ob_get_clean();
			$code=substr(sha1("somecodehere142".$_SESSION['captcha']['code']),5,10);
			echo '{text:"captcha",value:"'.base64_encode($rawImageBytes).'",hash:"'.$code.'"}';
		}else{
			$token = User::createHash();
			$query = "insert into user_token(user_id,token,attempts) values({$id},'{$token}',0)";
			MyDatabase::UpdateQuery($query);
			echo '{text:"token",value:"'.$token.'"}';
		}
	}else{
		echo '{text:"wrong",value:""}';
	}

	exit;
    
}
function GetPost($id){
	if(isset($_POST['token'])){
        $user=User::FindByToken($_POST['token']);
        if($user){ $_SESSION['user'] =(array)$user;}
    }
    $post = new Post($id);
    if($post){
        echo json_encode($post->GetApiJson());
    }
    exit;
}
function GetPostsList(){
    $offset=isset($_POST['offset']) ? (int)$_POST['offset'] : 0;
    $limit = isset($_POST['limit']) ? (int)$_POST['limit'] : 10;
    $type=isset($_POST['type']) ? strtolower($_POST['type']) : 'top';
    if(isset($_POST['token'])){
        $user=User::FindByToken($_POST['token']);
        if($user){ $_SESSION['user'] =(array)$user;}
    }
	$posts=false;
    switch($type){
        case 'top':
            $posts=Post::LoadPosts('hour(timediff(now(),add_date))<'.($offset+10)*35 .' and post.id<>488','recent_rating desc',$limit,$offset);
            break;
        case 'new':
            $posts=Post::LoadPosts('','add_date desc',$limit,$offset);
            break;
        case 'best':
            $posts=Post::LoadPosts('hour(timediff(now(),add_date))<24','rating desc',10,$offset);
            break;
		case 'saved posts':
			$posts=User::Get_saved_posts($user->id,$offset);
			break;
		case 'tag':
			if(!isset($_POST['tag'])||$_POST['tag']=='') {echo 'error'; exit;}
			$text=MyDatabase::real_escape_string($_POST['tag']);
			          //  $posts=Post::LoadPosts('','add_date desc',$limit,$offset);

			$posts = Post::LoadPosts("(tags like '%,{$text},%') or (tags like '{$text},%') or (tags like '%,{$text}')",'add_date desc',10,$offset);
			break;
		case 'search':
			if(isset($_POST['search'])){
				$text = MyDatabase::real_escape_string($_POST['search']);
				$posts=Post::Search($text,$offset,$limit);
			}
			break;
		case 'feed':
			if($user){
				$posts=User::Get_subscriptions($user->id,$offset);
			}
			break;
    }
    if(!$posts){
        echo 'error'; exit;
    }
    $list=[];
    while($row=$posts->fetch_assoc()){
        $post=new Post($row);
        if($post->title==null){ continue; }
        if($post->android_text==''){
            $post->android_text=MyDatabase::real_escape_string(json_encode($post->getJsonArray()));
            MyDatabase::SetParameter('post',$post->id,'android_text',$post->android_text,false);        
        }
        $list[]=$post->GetApiJson();
    }
    echo json_encode($list);
}

function GetAllTypesPostsList(){
    $offset=isset($_POST['offset']) ? (int)$_POST['offset'] : 0;
    $limit = isset($_POST['limit']) ? (int)$_POST['limit'] : 10;
    if(isset($_POST['token'])){
        $user=User::FindByToken($_POST['token']);
        if($user){ $_SESSION['user'] =(array)$user;}
    }
	  $posts=[];
    $posts[0]=Post::LoadPosts('hour(timediff(now(),add_date))<'.($offset+10)*35 .' and post.id<>488','recent_rating desc',$limit,$offset);
    $posts[1]=Post::LoadPosts('','add_date desc',$limit,$offset);
    $posts[2]=Post::LoadPosts('hour(timediff(now(),add_date))<24','rating desc',10,$offset);
    $posts[3]=$user ? User::Get_subscriptions($user->id,$offset) : null;
    $array=[];
	
    foreach($posts as $posts1){
      $list=[];
      if($posts1){      
        while($row=$posts1->fetch_assoc()){
            $post=new Post($row);
            if($post->title==null){ continue; }
            if($post->android_text==''){
                $post->android_text=MyDatabase::real_escape_string(json_encode($post->getJsonArray()));
                MyDatabase::SetParameter('post',$post->id,'android_text',$post->android_text,false);        
            }
            $list[]=$post->GetApiJson();
        }
      }
      $array[]=json_encode($list);
    }
    
    echo json_encode($array);
}
function GetCommentsList($post_id,$offset){
    require_once('models/comment.php');
    $query="select * from comments where post_id={$post_id} order by add_date limit 10 offset {$offset}";
    $res=MyDatabase::ReadQuery($query);
    $array=[];	
    while($row=$res->fetch_assoc()){
        $comment=new Comment(1,$row);
        $ar=$comment->getJson();
		$ar['post_title'] = '';
		$array[]=$ar;
    }
	$user=User::FindByToken($_GET['token']);
	if($user){
		MyDatabase::UpdateQuery("update comments set seen=1 where post_id={$post_id} and parent_comment_user_id={$user->id}");
	}
    echo json_encode($array);
	exit;
}

function GetUserInfo($user_id){
	$user1=User::FindByToken($_GET['token']);
    $user = new User($user_id);
    $username = $user->username;
    $subscribers = $user->Get_Subscribers_Count();
    $subscriptions = $user->Get_Subscriptions_Count();
    $post_count = $user->PostCount();
    $image = $user->GetImageLink();
	if(!$user1 || $user1->id==$user->id){
		$subs=-1;
	}else{
		$isSubscribed=$user1->isSubscribed($user->id);
		$subs=$isSubscribed ? 1:0; 
	}
    $array = ["username"=>$username,"image"=>$image,"userinfo"=>$subscribers." subscribers, ".$subscriptions." subscriptions, ","posts_info"=>$post_count. " posts","subscribed"=>$subs];
    echo json_encode($array);
	exit;
}
function GetComment($comment_id){
    require_once('models/comment.php');
    $comment = Comment::SingleComment($comment_id);
    echo json_encode($comment->getJson());
	exit;
}


function GetMyComments(){
	require_once('models/comment.php');
	$user=User::FindByToken($_POST['token']);
	if(!$user) exit;
	//$user =new User(23);
	$offset=isset($_POST['offset']) ? (int)$_POST['offset'] : 0;
	$query="select * from comments  where user_id<>{$user->id} and
			  parent_comment_user_id={$user->id}
			  order by seen asc, add_date desc limit 10 offset {$offset}";
	$res=MyDatabase::ReadQuery($query);
	$array=[];
	$responds=[];
    while($row=$res->fetch_assoc()){
        $comment=new Comment(1,$row);
		$ar=$comment->getJson();
		if($comment->user_id!=$user->id){
			$responds[]=$comment->id;
		}
		$ar['post_title']=MyDatabase::GetParameter('post',$comment->post_id,'title');
        $array[]=$ar;
    }
	$s=implode(',',$responds);
	MyDatabase::UpdateQuery("update comments set seen=1 where id in ({$s})");	
    echo json_encode($array);
	exit;
}
function GetUserPosts($user_id){
    $id=(int)$user_id;
    $offset=isset($_GET['offset']) ? (int)$_GET['offset'] : 0;
    $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
    if(isset($_GET['user_token124'])){
        $user=User::FindByToken($_GET['user_token124']);
        if($user){ $_SESSION['user'] =(array)$user;}
    }
    $posts=Post::LoadPosts('user_id='.$id,'add_date desc',$limit,$offset);
    if(!$posts){
        echo 'error'; exit;
    }
    $list=[];
    while($row=$posts->fetch_assoc()){
        $post=new Post($row);
        if($post->title==null){ continue; }
        if($post->android_text==''){
            $post->android_text=MyDatabase::real_escape_string(json_encode($post->getJsonArray()));
            MyDatabase::SetParameter('post',$post->id,'android_text',$post->android_text,false);        
        }
        $list[]=$post->GetApiJson();
    }
    echo json_encode($list);
}
function SavePost(){
	$token=preg_replace('/[^0-9a-z]/','',$_POST['token']);
	$user=User::FindByToken($token);
	if(!$user) return;
	if(isset($_POST['unsave'])){
		$db=MyDatabase::getInstance();
		$post_id=$db->real_escape_string($_POST['post_id']);
		$query="delete from saved_posts where post_id={$post_id} and user_id={$user->id}";
		$db->query($query);
	}else{
		$db=MyDatabase::getInstance();
		$post_id=$db->real_escape_string($_POST['post_id']);
		$query="insert into saved_posts(post_id,user_id) values({$post_id},{$user->id})";
		$db->query($query);
	}
}
function Add_comment($token,$post_id,$parent_comment_id,$text,$image){
    require_once('models/comment.php');
    $user=User::FindByToken($token);
    if(!$user) return;
    if($image != null){
			$id=Comment::add_comment($text,'','',$user->id,$user->username,$user->image_url,$post_id,$parent_comment_id);
        $im = imagecreatefromstring($image);
        if($im){
            $s="images/comments_images/".$id.".jpg";
            imagejpeg($im,$s);
            MyDatabase::UpdateQuery("update comments set image_url='/{$s}' where id={$id}");
        }
		}elseif(isset($_POST['video'])){
			$id=Comment::add_comment($text,'','',$user->id,$user->username,$user->image_url,$post_id,$parent_comment_id);		
			if($id){
				file_put_contents('videos/comments/prev_'.$id.'.mp4',base64_decode($_POST["video"]));
				$video_url='videos/comments/'.$id.'.mp4';
				$cmd='ffmpeg -i videos/comments/prev_'.$id.'.mp4  -c:v libx264 -preset ultrafast -movflags +faststart -pix_fmt yuv420p  -strict -2 -y '.$video_url;    
			  exec(escapeshellcmd($cmd));
				$image_url='images/comments_images/'.$id.'.jpg';
				$cmd=" ffmpeg -ss 0.5 -i {$video_url} -t 1  -f image2 ".$image_url;
				exec(escapeshellcmd($cmd));
				MyDatabase::UpdateQuery("update comments set image_url='/{$image_url}',video_url='/{$video_url}'  where id={$id}");
			}
    }else{
			$image1='';$video1='';
			if(isset($_POST['giphy_image'])){
				$image=MyDatabase::real_escape_string($_POST['giphy_image']);
				$image = str_replace('http://www.exifeed.com','',$image);
				if(preg_match('/http\:\/\/media[0-9]+\.giphy\.com\/media\/[0-9a-zA-Z]+\/giphy_s\.gif/',$image)||
					 preg_match('/\/images\/comments_images\/[0-9]+\.gif\.jpg/',$image)){		
					$image1=$image;
					//MyDatabase::UpdateQuery("update comments set image_url='{$image}' where id={$id}");
				}
			}
			if(isset($_POST['giphy_video'])){
				$video=MyDatabase::real_escape_string($_POST['giphy_video']);
				if(preg_match('/http\:\/\/media[0-9]+\.giphy\.com\/media\/[0-9a-zA-Z]+\/giphy\.mp4/',$image)||
					 strpos($video,'cloudinary.com')>2){		
					$image1=$image;
					//MyDatabase::UpdateQuery("update comments set video_url='{$video}' where id={$id}");
				}
			}			
			$id=Comment::add_comment($text,$image1,$video1,$user->id,$user->username,$user->image_url,$post_id,$parent_comment_id);			
		}
}

function GetUserSettings(){
	$token=preg_replace('/[^0-9a-z]/','',$_POST['token']);
	$user = User::FindByToken($token);
	if(!$user) exit;
	$array=['username'=>$user->username,'save_posts'=>$user->shorten_longpost ? '1' : '0',"image"=>$user->GetImageLink()];
	echo json_encode($array);
}

function SaveSettings(){
	$token=preg_replace('/[^0-9a-z]/','',$_POST['token']);
	$user = User::FindByToken($token); 
	if(!$user) exit; 
	if(isset($_POST['user_image'])){
		$image = base64_decode($_POST['user_image']);
		$im = imagecreatefromstring($image);
        if($im){
            $s="images/user_images/".$user->id.".jpg";
            imagejpeg($im,$s);
            MyDatabase::UpdateQuery("update user set image='{$user->id}.jpg' where id={$user->id}");
        }   
	}
	if(isset($_POST["delete_image"])){
		MyDatabase::UpdateQuery("update user set image='' where id={$user->id}");
		echo "http://exifeed.com/images/icons/no_image_user.png";
		exit;
	}
	if(isset($_POST['username'])){
		if(strlen($_POST['username'])<3||strlen($_POST['username'])>25){
			echo 'wrong length';
			exit;
		}
		if(!preg_match("/[^0-9a-zA-Z_]+/",$_POST['username'])){
			$user->username=MyDatabase::real_escape_string($_POST['username']);
			$user->shorten_longpost = (int)$_POST["shorten_post"];
			$user->save();
			echo 'saved';
		}else{
			echo 'wrong username';
			exit;
		}
	}
	if(isset($_POST['old_password'])&&isset($_POST['new_password'])){
		if(User::checkuser($user->email,$_POST['old_password'])){
            $id=$user->id;
            if(User::change_password($id,$_POST['new_password'])){
				echo 'ok';
			}else{
				echo '{error:"New password is incorrect"}';
			}
        }
	}
}

function Subscribe($user_id,$unsubscribe=false){
	$token=preg_replace('/[^0-9a-z]/','',$_POST['token']);
	$user = User::FindByToken($token);
	if(!$user) exit;
	if(!$unsubscribe){
		MyDatabase::UpdateQuery("insert into subscribes(subscriber_id,source_id) values({$user->id},{$user_id})");
	}else{
		MyDatabase::UpdateQuery("delete from subscribes where subscriber_id={$user->id} and source_id={$user_id}");
	}
}
function GetUserImages(){
	$offset=isset($_POST['offset']) ? (int)$_POST['offset'] : 0;
	$limit = isset($_POST['limit']) ? (int)$_POST['limit'] : 10;
	$query = "select * from user_images order by number desc limit {$limit} offset {$offset}";
	$res=MyDatabase::ReadQuery($query);
	$ar=[];
	while($row=$res->fetch_assoc()){
		$image = $row['image_url'];
		$video = $row['video_url'];
		if($image[0] == '/') $image = "http://www.exifeed.com".$image;
		if($video[0] == '/') $video = "http://www.exifeed.com".$video;
		$ar[]=['image'=>$image,'video'=>$video];
	}
	echo json_encode($ar);
	exit;
}

function AddPost(){
	require_once('functions/sanitize.php');
	$token=preg_replace('/[^0-9a-z]/','',$_POST['token']);
	$user = User::FindByToken($token);
	if(!$user) exit;
	$post_parts = json_decode($_POST['post']);
	$title=MyDatabase::real_escape_string(removeScript($_POST['title']));
	$description=isset($_POST['description']) ? MyDatabase::real_escape_string(url_link(removeScript($_POST['description']))) : '';
	$tags=isset($_POST['tags']) ?  MyDatabase::real_escape_string($scheme->tags) : '';
	$id=Post::AddPost(0,$title,$description,'complex','','',$tags,$user->id);
	if(!$id) exit;
	$html='';
	$json = array();
	$index=1;
	$folder=false;
	foreach($post_parts as $part){
		switch($part->type){
			case 'text':
				$text =$part->content;
				$html.= "<div class='long_post_part text'><span class='long_post_delete_button'></span>".$text."</div>";
				$ar=['type'=>'text','title'=>$text,'image'=>'','audio'=>'','video'=>'','id'=>''];
				$json[]=$ar;
				break;
			case 'image':
				if (!$folder) {
					mkdir("images/post/".$id);
					$folder = true;
				}
				$image = base64_decode($part->content);
				$im = imagecreatefromstring($image);
				if($im){
					$s="images/post/".$id."/".$index.".jpg";
					$html.="<div class='long_post_part image image_uploaded'><span class='long_post_delete_button'></span><img src='".$s."'/></div>";
					$ar=['type'=>'image','title'=>'','image'=>"http://exifeed.com/".$s,'video'=>'','audio'=>'','id'=>''];
					$json[]=$ar;
					imagejpeg($im,$s);					
				}  
				break;
			case 'video':
				if (!$folder) {
					mkdir("images/post/".$id);
					$folder = true;
				}
				if(!is_dir("videos/post/". $id)){
					mkdir("videos/post/" . $id);
				}
				file_put_contents('videos/post/'.$id.'/prev_'.$index.'.mp4',base64_decode($part->content));
				$video_url='videos/post/'.$id.'/'.$index.'.mp4';
				$cmd='ffmpeg -i videos/post/'.$id.'/prev_'.$index.'.mp4  -c:v libx264 -preset ultrafast -movflags +faststart -pix_fmt yuv420p  -strict -2 -y '.$video_url;    
			  exec(escapeshellcmd($cmd));
				$image_url='images/post/'.$id.'/'.$index.'jpg';
				$cmd=" ffmpeg -ss 0.5 -i {$video_url} -t 1  -f image2 ".$image_url;
				exec(escapeshellcmd($cmd));
				list($width, $height) = getimagesize($image_url);
				$html.="	<div class='long_post_part video_uploaded' style='max-width:{$width}px'>  <span class='long_post_delete_button'>  </span>  <img src='/{$image_url}' class='gif_image'/>
						<img video_src='http://www.exifeed.com/{$video_url}' src='/images/icons/play_icon.png' class='gif_play_button'/></div>";
				$ar=['type'=>'gif','title'=>'','image'=>'http://www.exifeed.com/'.$image_url,'video'=>'http://www.exifeed.com/'.$video_url,'audio'=>'','id'=>''];
				$json[]=$ar;
				break;
			case 'giphy':
				$image=$part->content;
				$video = $part->video;
				$html.="<div class='long_post_part image image_giphy'><span class='long_post_delete_button'></span><img class='pass gif_image'   src='".$image."' />
				<img class='gif_play_button' video_src='".$video."'  src='/images/icons/play_icon.png' /></div>";
				$ar=['type'=>'gif','title'=>'','image'=>$image,'video'=>$video,'audio'=>'','id'=>''];
				$json[]=$ar;				
				break;
			case 'coub':
				$id1=$part->content;
				$data=file_get_contents("http://coub.com/api/v2/coubs/".$id1);
				$data=json_decode($data);
				$title=$data->title;
				$mp4=$data->file_versions->integrations->ifunny_video;
				$mp3=$data->file_versions->mobile->looped_audio;
				$image=$data->picture;
				$html.='<div class="long_post_part video coub_video"><span class="long_post_delete_button"></span>';
				$html.='<img class="coub_play" v_id='+s+' src="http://www.exifeed.com/images/icons/play-button-icon-png-6.png"><span class="coub_name">Youtube name</span><img class="coub_image"  src="'.$image.'"></div>';
				$ar=['type'=>'coub','title'=>$title,'video'=>$mp4,'audio'=>$mp3,'image'=>$image,'id'=>$id1];
				$json[]=$ar;
				break;
			case 'youtube':
				$id1=$part->content;
				$title = getYoutubeName($id1);
				$html.='<div class="long_post_part video youtube_video"><span class="long_post_delete_button"></span><img class="youtube_play" v_id='.$id.' src="http://exifeed.com/images/icons/play-button-icon-png-6.png">
				<span class="youtube_name">'.$title.'</span><img class="youtube_image"  src="http://img.youtube.com/vi/'.$id.'/maxresdefault.jpg"></div>';
				$image="http://img.youtube.com/vi/{$Id}/maxresdefault.jpg";
				$ar2=['type'=>'youtube','title'=>$title,'image'=>$image,'audio'=>'','video'=>'','id'=>$id1];
				$json[]=$ar2;
				break;
		}
		$index++;
	} 
	$text=MyDatabase::real_escape_string(removeScript($html));
	MyDatabase::SetParameter('post',$id,'text',$text,false);
	$android_text=MyDatabase::real_escape_string(json_encode($json));
	MyDatabase::SetParameter('post',$id,'android_text',$android_text,false);
	echo 'post_uploaded';
	exit;
}
function PostRating(){
	$token=preg_replace('/[^0-9a-z]/','',$_POST['token']);
	$user = User::FindByToken($token);
	if(!$user) exit;
	$db=MyDatabase::getInstance();
	$value=$_POST['rating'];
	$post_id=$_POST['post_id'];
    $query="select value from users_ratings where user_id={$user->id} and post_id={$post_id}";
    $res=$db->query($query);
    $num=$res->num_rows;
    $res=$res->fetch_row();
//	echo $num.' |'.$res[0].' !'.$value;
    if($num==1&&$res[0]==-1&&$value==1){
        $query="update users_ratings set value=1 where user_id={$user->id} and post_id={$post_id}";
        $db->query($query);
        $query="update post set rating=rating+2,current_rating=current_rating+2 where post_id={$post_id}";
        $db->query($query);
    }
    if($num==1&&$res[0]==1&&$value==0){
        $query="delete from users_ratings where user_id={$user->id} and post_id={$post_id}";
        $db->query($query);
	$query="update post set rating=rating+1,current_rating=current_rating+1 where post_id={$post_id}";
        $db->query($query);
    }
    if($num==0){
        $query="insert into users_ratings(user_id,post_id,value) values({$user->id},{$post_id},{$value})";
        $db->query($query);
        $query="update users_ratings set value={$value} where user_id={$user->id} and post_id={$post_id}";
        $db->query($query);
        $query="update post set rating=rating+{$value},current_rating=current_rating+{$value} where post_id={$post_id}";
        $db->query($query);
    }
}

function SearchGiphy($text){
	$offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
	$t1=urlencode($text);
	$url = "http://api.giphy.com/v1/gifs/search?q={$t1}&api_key=dc6zaTOxFJmzC&limit=15&offset=".$offset;
	$array=[];
    $data=json_decode(file_get_contents($url))->data;
	foreach($data as $t){
        $image=$t->images->original_still->url;
        $video = $t->images->original->mp4;
        $array[]=['image'=>$image,'video'=>$video];        
    }
	$page=floor($offset/15);
	$client_id='c6d0b68ae9a67b0';
    $client_secret='6ef7fd61da5af922a422863429143b9f2af3e700';
    $c_url = curl_init();
    curl_setopt($c_url, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($c_url, CURLOPT_RETURNTRANSFER, true);
   curl_setopt($c_url, CURLOPT_URL,"https://api.imgur.com/3/gallery/search/score/all/page/{$page}?scrolled&q=".$text);
//     curl_setopt($c_url, CURLOPT_URL,"https://api.imgur.com/3/image/h6UcroP");
    curl_setopt($c_url, CURLOPT_HTTPHEADER, array('Authorization: Client-ID ' . $client_id));
    $result=curl_exec($c_url);
    
    $json_array = json_decode($result, true);
    //var_dump($json_array);
      $master = curl_multi_init();
  
    $curl_arr = array();
    $i=0;
    
    foreach($json_array['data'] as $id){
        //print_r($id);
        $curl_arr[$i]=curl_init();
        curl_setopt($curl_arr[$i], CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_arr[$i], CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_arr[$i], CURLOPT_HTTPHEADER, array('Authorization: Client-ID ' . $client_id));
        curl_setopt($curl_arr[$i], CURLOPT_URL,"https://api.imgur.com/3/image/".$id['id']);
        curl_multi_add_handle($master, $curl_arr[$i]);
        $i++;
    }
    if($i>0){
        do {
            curl_multi_exec($master,$running);
        } while($running > 0);

        $t=$i;
        for($i = 0; $i < $t; $i++)
        {
            $results = json_decode(curl_multi_getcontent  ( $curl_arr[$i]  ));
            if($results->success){
                if($results->data->animated){ 
                    $video=$results->data->mp4;
                    $image='http://i.imgur.com/'.$results->data->id.'l.jpeg';
                    $width=$results->data->width;
                    $array[]=['image'=>$image,'video'=>$video];
                }else{
                    $image='http://i.imgur.com/'.$results->data->id.'l.jpeg';
                    $width=$results->data->width;
					$array[]=['image'=>$image,'video'=>''];
                }
            }
        }
    }
	echo json_encode($array);
}
function RegisterUser($username,$email,$password){
	$id=User::AddUser($_POST['email'],$_POST['username'],$_POST['password']);
	if($id){
		$query = "insert into user_token(user_id,token) values({$id},'{$token}')";
		MyDatabase::UpdateQuery($query);
		echo '{text:"token",value:"'.$token.'"}';
		exit;
	}else{
		echo '{text:"There has been some error",value:""}';
		exit;
	}
}
function AddFacebookUser($facebook_id,$username,$userimage,$email){
	$id=User::check_user_provider($facebook_id,'Facebook');
	if(!$id){
		$id=User::add_provider_user('Facebook',$identifier,$email,$name,$image);
	}
	$token = User::createHash();
	$query = "insert into user_token(user_id,token,attempts) values({$id},'{$token}',0)";
	MyDatabase::UpdateQuery($query);
	echo '{text:"token",value:"'.$token.'"}';
}
function hex2str($hex) {
	$str='';
	$len = strlen($hex)/2;
	for($i=0;$i<$len;$i++){ 
		$s=substr($hex,$i*2,2);
		$str.=unichr(hexdec($s));
	}
    return $str;
  }
function unichr($dec) { 
  if ($dec < 128) { 
    $utf = chr($dec); 
  } else if ($dec < 2048) { 
    $utf = chr(192 + (($dec - ($dec % 64)) / 64)); 
    $utf .= chr(128 + ($dec % 64)); 
  } else { 
    $utf = chr(224 + (($dec - ($dec % 4096)) / 4096)); 
    $utf .= chr(128 + ((($dec % 4096) - ($dec % 64)) / 64)); 
    $utf .= chr(128 + ($dec % 64)); 
  } 
  return $utf;
}
