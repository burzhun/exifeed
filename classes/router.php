<?php
class Router{
    //return array with url parts
    static function getUrlArray(){
        $requestURI =$_SERVER['REQUEST_URI'];
        $scriptName = explode('/',$_SERVER['SCRIPT_NAME']);
        if(strpos($requestURI,'?')!=false){
            $requestURI=substr($requestURI,0,strpos($requestURI,'?'));
        }
        $requestURI=str_replace('#','',$requestURI);
        $requestURI = explode('/', $requestURI);
        for($i= 0;$i < sizeof($scriptName);$i++)
        {
            if ($requestURI[$i]== $scriptName[$i]||$requestURI[$i] == "mysite")
            {
                unset($requestURI[$i]);
            }
        }

        $array = array_values($requestURI);
        return $array;
    }
    //Return path to www folder
    static function pathtofolder(){
        $path=$_SERVER['SCRIPT_NAME'];
        $pos=strripos($path,'/');
        $path=substr($path,0,$pos+1);
        return $path;
    }

    //Returns the type of url
    static function urltype($routing=null){
        if($_SERVER['REQUEST_URI']=='/cron/post_rating_update'){
            return 'cron_post_rating_update';
        }
        if($routing==null){$routing=self::getUrlArray();}
        $type='34 ';
        if((count($routing)==1&&$routing[0]=='')||count($routing)==0){
            $type="index";
            return $type;
        }
        if(count($routing)==1){
            $type=$routing[0];
            return $type;
        }

        if(count($routing)==2&&($routing[0]=='post'||$routing[0]=='p')&&preg_match("/^id[0-9]+/",$routing[1])){
            $type="post";
            return $type;
        }
        if(count($routing)==2&&$routing[0]=='view'&&preg_match("/^id[0-9]+/",$routing[1])){
            $type="view";
            return $type;
        }

        if(count($routing)==3&&$routing[0]=='post'&&$routing[1]=='update'&&preg_match("/^id[0-9]+/",$routing[2])){
            $type="update_post";
            return $type;
        }

        if((count($routing)==1)&&($routing[0]=='settings')){
            $type='settings';
            return $type;
        }
        if((count($routing)==1)&&$routing[0]=='best'){
            $type='best_day';
            return $type;
        }
        if((count($routing)==2)&&$routing[0]=='best'&&$routing[1]=='week'){
            $type='best_week';
            return $type;
        }
        if((count($routing)==2)&&$routing[0]=='best'&&$routing[1]=='month'){
            $type='best_month';
            return $type;
        }
        if(count($routing)==2&&$routing[0]=='tags'){
            $type="tags";
            return $type;
        }
        if(count($routing)==2&&$routing[0]=='user'&&preg_match("/^id[0-9]+/",$routing[1])){
            $type="user";
            return $type;
        }
        if(count($routing)==1&&$routing[0]=='away'&&isset($_GET['link'])){
            $type='away';
            return $type;
        }
        //Log::AddLogData($type);
        return $type;
    }
}