<?php
class Resource{
    public $javascript,$css;
    public function __construct($router){
        switch($router)
        { 
            case 'index':
               $this->css=array('post.css','styles.css','comments.css');
               $this->javascript=array('post.js','comment.js');
                break; 

            case 'best_day':
                $this->css=array('post.css','styles.css','comments.css');
                $this->javascript=array('post.js','comment.js');
                break;

            case 'best_week':
                $this->css=array('post.css','styles.css','comments.css');
                $this->javascript=array('post.js','comment.js');
                break;

            case 'find':
                $this->css=array('post.css','styles.css','search.css','comments.css');
                $this->javascript=array('post.js','comment.js');
                break;

            case 'best_month':
                $this->css=array('insert.css','styles.css','comments.css');
                $this->javascript=array('post.js','comment.js');
                break;

            case 'new':
                $this->css=array('post.css','styles.css','comments.css');
                $this->javascript=array('post.js','comment.js');
                break;

            case 'post':
                $this->css=array('post.css','styles.css','comments.css');
                $this->javascript=array('comment.js','post.js');
                break;
            case 'update_post':
                $this->css=array('post.css','styles.css','comments.css','insert.css');
                $this->javascript=array('post.js','insert_post.js');
                break;
            case 'add':
                $this->css=array('insert.css','styles.css');
                $this->javascript=array('../includes/ckeditor/ckeditor.js','insert_post.js','Autolinker.js');
                break;
            case 'user':
                $this->css=array('post.css','styles.css','user_view.css','comments.css');
                $this->javascript=array('post.js','comment.js');
                break;
            case 'my':
                $this->css=array('post.css','styles.css','user_view.css','comments.css');
                $this->javascript=array('post.js','comment.js');
                break;
            case 'settings':
                $this->css=array('styles.css','settings.css');
                $this->javascript=array('settings.js');
                break;
            case 'saved':
                $this->css=array('post.css','styles.css','comments.css');
                $this->javascript=array('post.js','comment.js');
                break;
            case 'feed':
                $this->css=array('post.css','styles.css','comments.css');
                $this->javascript=array('post.js','comment.js');
                break;
            case 'comments':
                $this->css=array('styles.css','comments.css');
                $this->javascript=array();
                break;
            case 'tags':
                $this->css=array('post.css','styles.css','tags_view.css','comments.css');
                $this->javascript=array('post.js','comment.js');
                break;
            case 'useridea':
                $this->css=array('styles.css','userideas.css');
                $this->javascript=array('user_ideas.js');
                break;
            default:
                $this->css=array('styles.css');
                $this->javascript=array();
                break;
        }
    }
}