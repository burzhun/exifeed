<?php
class Controller{
    public $type,$routing,$user_id;
    static function Content($routing,$user_id=''){
        $content='';
        $user=null;
        if($user_id!=''){
            $user=new User($user_id);
        }
        switch($routing)
        {
            case 'index':
                $res=Post::LoadPosts('hour(timediff(now(),add_date))<350 and post.id<>488','recent_rating*100+rand() desc, post_views desc, id desc',10,0);
                $html="<div id='content'>";
                if($user){
                    $date=$user->last_visit_time;
                    $recent_posts=User::Get_last_subscriptions($user_id,0,$date);
                    if($recent_posts){
                        $recent_posts_html="";
                        while($row=$recent_posts->fetch_assoc()){
                            $post=new Post($row);
                            if($post->user_repost_id==0){
                                $recent_posts_html.=Views::Post($post,false,$user_id);
                            }
                            else{
                                $repost_user=new User($post->user_repost_id);
                                $recent_posts_html.=Views::Repost($repost_user,$post,false,$user_id);
                            }
                        }
                        $html.=Views::recent_posts_view($recent_posts_html);
                    }
                }
                $html.="<div class='top_title'>Most popular</div>";
                if(User::isFirstTime()){
                    $first_post=new Post(488);
                    $html.=Views::Post($first_post,false,$user_id);
                }                
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    $html.=Views::Post($post,false,$user_id);
                }
                $html.="</div>";
                return $html;
                break;
            case 'settings':
                if(!User::isLogin()){
                    header('Location:/');
                }
                $user= new User(User::id());
                if(isset($_GET['no_image'])&&$_GET['no_image']==1){
                    $user->image='';
                    $user->save();
                    unlink('images/user_images/'.$user->id.'.jpg');
                    exit;
                }
                if(isset($_POST['username'])){
                    $username=MyDatabase::real_escape_string($_POST['username']);
                    if(!preg_match("/[^0-9a-zA-Z_]+/",$_POST['username'])){
                        $user->username=$username;
                    }
                    $shorten_longpost=(int) $_POST['shorten_longpost'];
                    if($shorten_longpost==1 || $shorten_longpost==0){
                        $user->shorten_longpost=$shorten_longpost;
                    }
                    $user->save();
                }
                $user->reload_user_data();
                if(User::isLogin()){
                    $id=$_SESSION['user']['id'];
                    return Views::settings($id);
                }
                else{
                    return "";
                }
                break;
            case 'find':
                if(isset($_GET['text'])){
                    $text=MyDatabase::real_escape_string($_GET['text']);
                    $html=Views::search_page_form($text);
                    if(strlen($text)>2){
                        $query="select id,username,provider,image from user where match(username) against('{$text}*' in boolean mode)";
                        $res=MyDatabase::ReadQuery($query);
                        $html.="<div id='content'>";
                        if($res->num_rows){
                            $html.="<div id='user_search'>";
                            while($row=$res->fetch_assoc()){
                                $html.=Views::user_search($row);
                            }
                            $html.="</div>";
                        }
                        $res=Post::Search($text,0,10);
                        while($row=$res->fetch_assoc()){
                            $post=new Post($row);
                            $html.=Views::Post($post,false,$user_id);
                        }
                        $html.="</div>";
                    }
                }
                else{
                    $html=Views::search_page_form('');
                }
                return $html;
                break;
            case 'best_day':
                $res=Post::LoadPosts('hour(timediff(now(),add_date))<24','rating desc',10,0);
                $html="<div id='content'>";
                if(User::isFirstTime()){
                    $first_post=new Post(488);
                    $html.=Views::Post($first_post,false,$user_id);
                }  
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    $html.=Views::Post($post,false,$user_id);
                }
                $html.="</div>";
                return $html;
                break;
            case 'best_week':
                $res=Post::LoadPosts('floor(hour(timediff(now(),add_date))/24)<7','rating desc',10,0);
                $html="<div id='content'>";
                if(User::isFirstTime()){
                    $first_post=new Post(488);
                    $html.=Views::Post($first_post,false,$user_id);
                }  
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    $html.=Views::Post($post,false,$user_id);
                }
                $html.="</div>";
                return $html;
                break;
            case 'best_month':
                $res=Post::LoadPosts('floor(hour(timediff(now(),add_date))/24)<31','rating desc',10,0);
                $html="<div id='content'>";
                if(User::isFirstTime()){
                    $first_post=new Post(488);
                    $html.=Views::Post($first_post,false,$user_id);
                }  
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    $html.=Views::Post($post,false,$user_id);
                }
                $html.="</div>";
                return $html;
                break;
            case 'new':
                $res=Post::LoadPosts('','add_date desc',10,0);
                $html="<div id='content'>";
                if(User::isFirstTime()){
                    $first_post=new Post(488);
                    $html.=Views::Post($first_post,false,$user_id);
                }  
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    $html.=Views::Post($post,false,$user_id);
                }
                $html.="</div>";
                return $html;
                break;
            case 'post':
                $id2=Router::getUrlArray();
                $id=substr($id2[1],2);
                $post=new Post($id);
                $post_author=$post->user_id;
                $html=Views::Post($post,true,$user_id);
                if(isset($_GET['embed'])){
                    echo $html;
                    exit;
                }
                $html.=Views::Comments($post->id,$post_author);
                if($user_id!=''){
                    $html.=Views::Add_comment_div($user_id,$post->id);
                    Post_views::Add_view($id,$user_id);
                }
                $html.="<div id='additional_posts'></div>";
                return $html;
                break;
             case 'view':
                $id2=Router::getUrlArray();
                $id=substr($id2[1],2);
                $post=new Post($id);
                $post_author=$post->user_id;
                $html=Views::PostView($post,true,$user_id);
                echo $html;
                exit;
                $html.=Views::Comments($post->id,$post_author);
                if($user_id!=''){
                    $html.=Views::Add_comment_div($user_id,$post->id);
                    Post_views::Add_view($id,$user_id);
                }
                $html.="<div id='additional_posts'></div>";
                return $html;
                break;
            case 'add':
                if (User::isLogin()){
                    $html=Views::insert_form();
                }
                else{
                    $html=Views::insert_form_not_logged();
                }
                return $html;
                break;
            case 'update_post':
                $url1=Router::getUrlArray();
                $id=substr($url1[2],2);
                $post=new Post($id);                
                if($user_id!=$post->user_id){
                    return "";
                }
                $text="";
                if(isset($_POST['text'])){
                    $text=url_link(htmlentities($_POST['text']));
                }
                $image_html="";
                $width='';
                
                return Views::update_post($id,$post);
                break;
            case 'my':
                if(!User::isLogin()){
                    return 'Please log in first';
                }
                $id=User::id();
                $res=Post::LoadPosts('post.user_id='.$id,'add_date desc',10,0);
                if (User::isLogin()){
                    $isSubscribed=$user->isSubscribed($id);
                }
                $html=Views::user_view($id,$isSubscribed);
                $html.="<div id='content'>";
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    $html.=Views::Post($post,false,$user_id);
                }
                $html.="</div>";
                return $html;
                break;
            case 'user':
                $id2=Router::getUrlArray();
                $id=substr($id2[1],2);
                $res=Post::LoadPosts('post.user_id='.$id,'add_date desc',10,0);
                if (User::isLogin()){
                    $isSubscribed=$user->isSubscribed($id);
                }
                $html=Views::user_view($id,$isSubscribed);
                $html.="<div id='content'>";
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    $html.=Views::Post($post,false,$user_id);
                }
                $html.="</div>";
                return $html;
                break;
            case "tags":
                $t=Router::getUrlArray();
                $text=MyDatabase::real_escape_string($t[1]);
                $query="select *,hour(timediff(now(),add_date)) as hours,minute(timediff(now(),add_date)) as minutes,
                        datediff(now(),add_date) as days from post where (tags like '%,{$text},%') or (tags like '{$text},%') or (tags like '%,{$text}')
                          ORDER by add_date desc limit 10";
                $res=MyDatabase::ReadQuery($query);
                $res=Post::LoadPosts("(tags like '%,{$text},%') or (tags like '{$text},%') or (tags like '%,{$text}')",'add_date desc',10,0);
                $html="";
                if(User::isLogin()){
                    $status=$user->User_tag_status($text);
                    $html=Views::tags_view($user,$text,$status);
                }else{
                    $html.='<div class="tags_view">
    <span class="title">Posts with tag <span class="tag">#'.$text.'</span></span><br></div>';
                }
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    $html.=Views::Post($post,false,$user_id);
                }
                $html.="</div>";
                return $html;
                break;
            case 'feed':
                if(User::isLogin()){
                    $res=User::Get_subscriptions($user->id,0);
                    $html="<div id='content'>";
                    if($res){
                        while($row=$res->fetch_assoc()){
                            $post=new Post($row);
                            if($post->user_repost_id==0){
                                $html.=Views::Post($post,false,$user_id);
                            }
                            else{
                                $repost_user=new User($post->user_repost_id);
                                $html.=Views::Repost($repost_user,$post,false,$user_id);
                            }
                        }
                    }
                    else{
                        $html.="<div style='text-align:center;font-size:19px;'>No posts yet</div>";
                    }
                    $html.="</div>";
                }
                else{
                    $html="<div>You have to be logged in, to be able to subscribe</div>";
                }
                //User::reload_visit_time_id($user_id);
                return $html;
                break;
            case 'saved':
                if(User::isLogin()){
                    $res=User::Get_saved_posts($user->id,0);
                    $html="<div id='content'>";
                    while($row=$res->fetch_assoc()){
                        $post=new Post($row);
                        $html.=Views::Post($post,false,$user_id);
                    }
                    $html.="</div>";
                }
                else{
                    $html="<div>You have to be logged in, to be able to subscribe</div>";
                }
                return $html;
                break;
            case 'comments':
                if(isset($_SESSION['user']['id'])){
                    $id=$_SESSION['user']['id'];
                    $query="select * from comments  where user_id={$id}
                              or exists(select id from comments com2 where comments.parent_comment_id=com2.id and com2.user_id={$id})
                              order by add_date desc limit 10";
                    $res=MyDatabase::ReadQuery($query);
                    $html="<div id='comments_list'>";
                    while($row=$res->fetch_assoc()){
                        $html.=Views::single_comment($row['id'],$row['user_id'],$row['parent_comment_id'],$row['text'],$row['image_url'],$row['video_url'],$row['post_id']);
                    }
                    $html.="</div>";
                    return $html;
                }
                break;
            case 'log':
               // Post::post_to_buffer(297);
                exit;
                if(User::isLogin()&&User::id()==1){
                    require_once('models/Log.php');
                    $res=Log::GetLogDdata(15,0);
                    $html="<div id='log_list'>".Views::log($res)."</div>";
                    return $html;
                }
                break;
            case 'useridea':
                require_once('models/UserIdeas.php');
                $res=UserIdeas::GetIdeas(15);
                $html=Views::UserIdeas($res);
            case 'away':
                $url=urldecode($_GET['link']);
                if(strpos($url,'http://')||strpos($url,'https://')){
                    $html='<script type="text/javascript">
           window.location.href = "'.$url.'"
      </script>';
                }
                else{
                    $html='<script type="text/javascript">
           window.location.href = "http://'.$url.'"
      </script>';
                }
                return $html;
                break;
            case 'cron_post_rating_update':
                $query="update post set recent_rating=current_rating,current_rating=0 where hour(timediff(now(),add_date))<48";
                MyDatabase::UpdateQuery($query);
                $time=time()-2*86400;
                $query="delete from tags where date<{$time}";
                exit;
                break;
            case 'about':
                ob_start();
                include 'Views/about.php';
                return ob_get_clean();
                exit;
                break;
            case 'api':
                echo 'android';
                exit;
                break;

            case 'map':
                ob_start();
                include 'Views/map.php';
                return ob_get_clean();
                exit;
            break;
            
            case 'coub_iframe':
                $id=$_GET['id'];
                ob_start();
                include 'Views/coub_iframe.php';
                return ob_get_clean();
                break;
        }
    }
}