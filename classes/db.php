<?php

class MyDatabase
{
    static $db=null;
	static function getInstance($isAdmin=false){
        if(self::$db!=null){
            return self::$db;
        }
        else{
            if($isAdmin){
                self::$db=new mysqli('localhost','root','11270507','exifeed');
            }else{
                self::$db=new mysqli('localhost','newuser','11270507','exifeed');
            }

            self::$db->query("SET CHARSET 'utf8';");
            return self::$db;
        }
	}
	 static function UpdateQuery($query){
         //echo $query;
         $db=self::getInstance();
         //var_dump($db);
         //$db->query("SET CHARSET 'utf8';");
         //$db->query("SET Names 'utf8';");
         //echo $query;
         $db->query($query);
         //$db->query("SET Names 'utf8';");
         $id=$db->insert_id;
         //MyDatabase::UpdateQueries($query);
         return $id;
     }

     static function ReadQuery($query){
         //require_once('models/Log.php');
         $db=self::getInstance();
         $result=$db->query($query);
         if(!$result){
             $error=$db->error;
             Log::AddLogData($error);
         }
         //MyDatabase::UpdateQueries($query);
         return $result;
     }
    static function real_escape_string($text){
        $db=self::getInstance();
        $t=$db->real_escape_string($text);
        return $t;
    }
    static function Exists($query){
        $db=self::getInstance();
        $result=$db->query($query);
        if($result->num_rows==0){
            return false;
        }
        else{
            return true;
        }
    }
    static function SetParameter($table,$id,$key,$value,$isnumber){
        if($isnumber) $query="update {$table} set {$key}={$value} where id={$id}";
        else $query="update {$table} set {$key}='{$value}' where id={$id}";
        MyDatabase::UpdateQuery($query);
        //MyDatabase::UpdateQueries();
    }
    static function GetParameter($table,$id,$column){
        $query="select {$column} from {$table} where id={$id} limit 1";
        $res=MyDatabase::ReadQuery($query);
        $row=$res->fetch_row();
       // MyDatabase::UpdateQueries();
        return $row[0];
    }
    static function UpdateQueries($query){
        MyDatabase::UpdateQuery("insert into queries(number,query) values (1,'{$query}')");
    }
}

?>