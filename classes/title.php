<?php
require_once('router.php');
function title($routing){
    switch($routing){

        case 'index':
            return 'Exifeed.com It is a place, where anyone can share some interesting article, image, video with other people and comment it.Gifs,videos,funny pictures.';
            break;
        case 'settings':
            return 'Your settings. Exifeed.com';
            break;
        case 'find':
            $text=MyDatabase::real_escape_string($_GET['text']);
            return $text."- Exifeed search";
            break;
        case 'best_day':
            return "Best posts for last day on Exifeed.com";
        break;
        case 'best_week':
            return "Best posts for last week on Exifeed.com";
        break;
        case 'best_month':
            return "Best posts for last month on Exifeed.com";
        break;
        case 'new':
            return "New posts on Exifeed.com";
        break;
        case 'post':
            $id2=Router::getUrlArray();
            $id=substr($id2[1],2);
            $title=MyDatabase::GetParameter('post',$id,'title');
            return stripslashes($title).". -Exifeed.com";
            break;
        case 'insert':
            if (User::isLogin()){
                return "Add new post to Exifeed.com";
            }
            else{
                return "You should be logged in to add new post. Exifeed.com";
            }
            break;
        case 'user':
            $id2=Router::getUrlArray();
            $id=substr($id2[1],2);
            $username=MyDatabase::GetParameter('user',$id,'username');
            return $username.'-Exifeed.com';
        break;
        case 'tags':
            $t=Router::getUrlArray();
            $text=MyDatabase::real_escape_string($t[1]);
            return "Posts with tag ".$text." on Exifeed.com";
        break;
        case 'feed':
            return 'Your feed - Exifeed.com';
        break;
        case 'saved':
            return "Your saved posts on Exifeed.com";
        break;
        case 'comments':
            return "Your comments on Exifeed.com";
        break;
        case 'log':
            return "log";
        break;
        case "useridea":
            return "Ideas proposed by users. -Exifeed.com";
        break;

    }
    return "Exifeed.com";
}