<?php
require_once('router.php');
require_once('models/user.php');
require_once('models/comment.php');
class Page{
    private  $javascript,$javascript_bottom;
    private $css;
    private $title,$header,$menu,$mainbody,$rightside,$footer,$error_message,$user;
    public function __construct(){
        $path=Router::pathtofolder();
        $this->javascript="<script src='{$path}js/jquery-2.0.3.min.js'></script>";
        $this->javascript.="<script src='{$path}js/main.js'></script>";
        if(isMobile()){
            $this->javascript.="<script async src='{$path}js/mobile.js'></script>";
            $this->javascript.="<script async src='//code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js'></script>";
            $this->css="<link rel='stylesheet' type='text/css' href='{$path}css/mobile/header.css'/>";
            $this->javascript.="<script async src='{$path}js/mobile/registration.js'></script>";
        }
        else{
            $this->javascript.="<script src='{$path}js/pc.js'></script>";
            $this->javascript_bottom="";
            $this->css="<link rel='stylesheet' type='text/css' href='{$path}css/header.css'/>";
            $this->css.="<link rel='stylesheet' type='text/css' href='{$path}css/menu.css'/>";

        }


        $this->header=Views::header_form();
        $this->rightside="";
        $this->mainbody="";
        $this->footer="";
        $this->error_message="";
        $user=null;
    }
    public function add_javascript($javascript){
        if(!empty($javascript)){
            $path=Router::pathtofolder();
            $links="";
            foreach($javascript as $item)
            {
                $item=$path.'js/'.$item;
                $links.="<script async src='{$item}'></script>";
            }
            $this->javascript.=$links;
        }
        return $this;
    }
    public function add_css($css){
        if(!empty($css)){
            $path=Router::pathtofolder();
            $links="";
            $im='';
            if(isMobile()){
                $im='mobile/';
            }
            foreach ($css as $item) {
                $item=$path.'css/'.$im.$item;
                $links.="<link rel='stylesheet' type='text/css' href='".$item."'/>";
            }
            $this->css.=$links;
        }
        return $this;
    }
    public function set_title($title){
        $this->title=$title;
        return $this;
    }
    public function mainbody($mainbody){
        $this->mainbody=$mainbody;
        return $this;
    }
    public function rightside($rightside){
        $this->rightside=$rightside;
        return $this;
    }
    public function footer($footer){
        $this->footer=$footer;
        return $this;
    }
    public function user($user){
        $this->user=$user;
        return $this;
    }
    static function isAndroid(){
        if(isset($_SESSION['android'])&&$_SESSION['android']=='1'){
            return true;
        }
        return false;
    }
    public function CreatePage(){
        $type=Router::urltype();       
        require_once("includes/analyticstracking.php");
        if(self::isAndroid()){
            ob_start();
            include 'Views/layouts/android_layout.php';
            return ob_get_clean();
        }
        if(isMobile()){
            ob_start();
            include 'Views/layouts/mobile_layout.php';
            return ob_get_clean();
        }
        else{
            ob_start();
            include 'Views/layouts/pc_layout.php';
            return ob_get_clean();
        }

    }

}
