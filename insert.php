<?php
require_once('classes/Page.php');
require_once('Views.php');
require_once('classes/router.php');
require_once('classes/title.php');
require_once('classes/Resource.php');
require_once('classes/Controller.php');
require_once('functions/isMobile.php');
require_once('Controllers/Validate_post_data.php');
session_start();
header('Content-Type: text/html; charset=utf-8');
User::autorize(1);
if(!User::isLogin()){
    User::check_Cookies();
}
if(isset($_POST)){
    Validate_post_data($routing);
}
$routing=Router::getUrlArray();
$page=new Page();
$content=Controller::Content('insert');
$resource=new Resource('insert');
$title=title($routing);
$page->add_css($resource->css)->add_javascript($resource->javascript)->set_title($title)
    ->mainbody($content);
echo $page->CreatePage();
?>
