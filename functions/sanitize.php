<?php
function url_link($text){
    $text=str_replace("<", " <", $text);
    $text=str_replace(">", "> ", $text);
    //$text=str_replace('iframe', 'div', $text);
    $text=preg_replace(array('/=(\"|\')http/i'),array('=$1h1ttp') , $text);
    $text=preg_replace(array('/=(\"|\')www/i'),array('=$1w1ww') , $text);
    $find=array('`((?:https?|ftp)://\S+[[:alnum:]]/?)`si','`((?<!//)(www\.\S+[[:alnum:]]/?))`si');
    $replace=array(' <a href="$1$2" target="_blank">$1$2</a> ',' <a href="http://$1$2" target="_blank">$1</a> ');
    $text=preg_replace($find,$replace,$text);
  //echo $text;
    $text=preg_replace(array("/=(\"|\')w1ww/i"),array('=$1www') , $text);
    return preg_replace(array("/=(\"|\')h1ttp/i"),array('=$1http') , $text);
}
function url_link_comment($text){
    $find=array('`((?:https?|ftp)://\S+[(^(coub))[:alnum:]]/?)(\.png|\.jpg)`si','`((?<!//)(www\.\S+[[:alnum:]]/?))(\.png)`si');
    $replace=array(' <img src="$1$2" >',' <img src="$1$2" > ');
    $text=preg_replace($find,$replace,$text);
    return url_link($text);
}

function printtext($text){
    echo stripslashes(htmlspecialchars($text)); 
}
function printtext_linebreak($text){
    $text=str_replace(array('\r\n','\r','\n'), "<br />",$text);
    $t=htmlspecialchars("<br />");
    $text=htmlspecialchars($text);
    $text=str_replace($t,"<br />",$text);
    echo $text;
}
function print_complex_text($text){
    require_once("includes/htmlpurifier-4.6.0/library/HTMLPurifier.auto.php");
    $text=str_replace('&Acirc;','',$text);
    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);
    $clean_html = $purifier->purify($text);
    echo $clean_html;
}
function clean_complex_text($text){
    require_once("includes/htmlpurifier-4.6.0/library/HTMLPurifier.auto.php");
    $text=str_replace('&Acirc;','',$text);
    $config = HTMLPurifier_Config::createDefault();
    $purifier = new HTMLPurifier($config);
    $clean_html = $purifier->purify($text);
    return $clean_html;
}
function int_param($text){
    $text=preg_replace("/[^0-9]/","",$text);
    return $text;
}
function removeScript($text){
    require_once("includes/htmlpurifier-4.6.0/library/HTMLPurifier.auto.php");
    $text=str_replace('&Acirc;','',$text);
    $config = HTMLPurifier_Config::createDefault();
    $config->set('HTML.SafeIframe', true);
    //$config->set('CSS.AllowedProperties', 'left');
    //$config->set('HTML.Allowed','div[style]');
    $config->set('CSS.AllowedProperties', 'left');
    $config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/|coub.com)%');
    $config->getHTMLDefinition(true)->addAttribute('img','index','CDATA');
    $config->getHTMLDefinition(true)->addAttribute('img','gif_src','CDATA');
    $config->getHTMLDefinition(true)->addAttribute('img','video_src','CDATA');
    $config->getHTMLDefinition(true)->addAttribute('img','v_id','CDATA');
    $config->getHTMLDefinition(true)->addAttribute('img','src','CDATA');
    $config->getHTMLDefinition(true)->addAttribute('video','src','CDATA');
    $config->getHTMLDefinition(true)->addAttribute('img','old','CDATA');
    $config->getHTMLDefinition(true)->addAttribute('video','old','CDATA');
    $config->getHTMLDefinition(true)->addAttribute('iframe','id','CDATA');
    $config->getHTMLDefinition(true)->addAttribute('div','style','CDATA');
    $config->getHTMLDefinition(true)->addAttribute('div','font-size-param','CDATA');
    $def=$config->getHTMLDefinition(true);
    $video = $def->addElement(
      'video',   // name
      'Block',  // content set
      'Flow', // allowed children
      'Common', // attribute collection
      array( // attributes
        'src'=>'URI',
        'id'=>'CDATA',
        'index'=>'CDATA',
        'controls'=>'CDATA',
        'autoplay'=>'CDATA',
      )
    );
          
    $purifier = new HTMLPurifier($config);
    $clean_text = $purifier->purify($text);
    return $clean_text;
}