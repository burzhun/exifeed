<?php

function isMobile() {
	$t=false;
	//return true;
	if(isset($_GET['type'])){
		if($_GET['type']=='pc'){
			$t= false;
		}
		if($_GET['type']=='mobile'||$_GET['type']=='android'){
			$t= true;
			if($_GET['type']=='android'){
				$_SESSION['android']=1;
			}
		}
		$_SESSION['type']=$t;
	}
	if(isset($_SESSION['type'])){
		return $_SESSION['type'];
	}
	else{
    	$t=preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    	$_SESSION['type']=$t;
    	return $t;
    }
}
function isIOS(){
	if(!isset($_SESSION['isIOS'])){
		if(strpos($_SERVER['HTTP_USER_AGENT'],"iPhone")||strpos($_SERVER['HTTP_USER_AGENT'],"iPad")){
			$_SESSION['isIOS']=true;
		}else{
			$_SESSION['isIOS']=false;
		}
	}
	return $_SESSION['isIOS'];
}

function isAndroid(){
	if(!isset($_SESSION['isAndroid'])){
		$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
		if(stripos($ua,'android') !== false) { // && stripos($ua,'mobile') !== false) {
			$_SESSION['isAndroid']=true;
		}else{
			$_SESSION['isAndroid']=false;
		}
	}
	return $_SESSION['isAndroid'];	
}