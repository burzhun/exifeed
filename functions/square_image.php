<?php
function square_image($source,$destination){
    $info=getimagesize($source);
    $width=$info[0];
    $height=$info[1];
    $image=null;
    if ($info['mime'] == 'image/jpeg') {
        $image = imagecreatefromjpeg($source);
    }
    if ($info['mime'] == 'image/png') {
        $image = imagecreatefrompng($source);
    }
    if(!$image){
        return;
    }
    $width2=$width;
    $height2=$height;
    $offsetx=0;
    $offsety=0;
    if($width>$height){
        $width2=$height;
        $height2=$height;
        $offsetx=($width-$height)/2;
        $offsety=0;
    }
    else{
        $width2=$width;
        $height2=$width;
        $offsetx=0;
        $offsety=($height-$width)/2;
    }
    $canvas = imagecreatetruecolor($width2,$height2);
    imagecopy($canvas,$image,0,0,$offsetx,$offsety,$width2,$height2);
    imagejpeg($canvas,$destination,75);
}