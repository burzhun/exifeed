<?php
function compress($source, $destination, $quality) {

    $info = getimagesize($source);

    if ($info['mime'] == 'image/jpeg') {
        $image = imagecreatefromjpeg($source);
        if($image){
            imagejpeg($image, $destination, $quality);
            return $destination;
        }
    }
    if ($info['mime'] == 'image/png') {
        $image = imagecreatefrompng($source);
        if($image){
            $bg = imagecreatetruecolor(imagesx($image), imagesy($image));
            imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
            imagealphablending($bg, TRUE);
            imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
            imagedestroy($image);
            imagejpeg($bg, $destination, $quality);
            imagedestroy($bg);
            return $destination;
        }
    }

    if ($info['mime'] == 'image/gif') {
        $image = imagecreatefromgif($source);
        if($image){
            imagejpeg($image, $destination.'.jpg');
            move_uploaded_file($source,$destination);

            return $destination;
        }

    }

}