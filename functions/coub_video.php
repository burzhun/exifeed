<?php

function getDuration($path){
    $result = shell_exec("ffmpeg -i ".$path.' 2>&1 | grep -o \'Duration: [0-9:.]*\'');
    $duration = str_replace('Duration: ', '', $result); // 00:05:03.25
    if(strpos($duration,'.')){
        $duration = substr($duration,0,strpos($duration,'.'));
    }
    //get the duration in seconds
    $timeArr = preg_split('/:/', $duration);

    $t  = (($timeArr[3])? $timeArr[3]*1 + $timeArr[2] * 60 + $timeArr[1] * 60 * 60 : $timeArr[2] + $timeArr[1] * 60);
    return $t;
}
function DownloadFile($src,$path){
    file_put_contents($path,file_get_contents($src));				
}
function DownloadVideo($src,$path){
    $prev_path=str_replace('.','_prev.',$path);
    DownloadFile($src,$prev_path);
    $cmd='ffmpeg -i '.$prev_path.'  -c:v libx264 -preset ultrafast -crf 35 -movflags +faststart -pix_fmt yuv420p  -strict -2 -y '.$path; 
    exec(escapeshellcmd($cmd));					
}

function repeatvideo($folder,$video,$n){
    $s="";
    for($i=0;$i<$n;$i++){
        $s.="file '{$video}' \n";
    }
    file_put_contents($folder.'list.txt',$s);
    $cmd = "ffmpeg -f concat -i {$folder}list.txt -c copy {$folder}repeated_{$video}";
    exec(escapeshellcmd($cmd));			
    @unlink($folder.'list.txt');
}

function addAudio($video,$audio,$result){
    $cmd = "ffmpeg -i {$video} -i {$audio} -codec copy -shortest {$result}";
    exec(escapeshellcmd($cmd));		
}
function MakeCoub($folder,$index,$video,$audio){
    DownloadFile($audio,$folder.$index.'.mp3');
    DownloadVideo($video,$folder.$index.'.mp4');
    $audio=$index.'.mp3';
    $video=$index.'.mp4';
    $audio_l=getDuration($folder.$audio);
    $video_l=getDuration($folder.$video);
    if($audio_l>$video_l){
        $n=floor($audio_l/$video_l);
        if($n>3) $n=3;
        repeatvideo($folder,$video,$n);
        @unlink($folder.$video);
        $video='repeated_'.$video;
    }
    addAudio($folder.$video,$folder.$audio,$folder.'result_'.$video);
    @unlink($folder.$video);
    @unlink($folder.$audio);
    return $folder.'result_'.$video;
}