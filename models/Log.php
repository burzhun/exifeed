<?php
require_once('classes/db.php');
class Log{
    static function GetLogDdata($limit,$offset){
        $query="select * from log order by date desc limit {$limit} offset {$offset}";
        $res=MyDatabase::ReadQuery($query);
        return $res;
    }
    static function AddLogData($data,$optional=''){
        $date=date('Y-m-d H:i:s');
        $db=MyDatabase::getInstance();
        $data=$db->real_escape_string($data);
        if($optional!='') $optional=$db->real_escape_string($optional);
        $query="insert into log(data,optional,date) values('{$data}','{$optional}','{$date}')";
        MyDatabase::UpdateQuery($query);
    }
}