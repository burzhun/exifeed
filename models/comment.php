<?php
require_once('user.php');
class Comment
{
    public $id,$text,$image_url,$parent_comment_id,$post_id,$user_id,$username,$userimage,$rating,$children_comments,$level,$video_url,$seen;
    function __construct($id,$row=''){
        if($row==''){
            $query="select * from comments where id={$id}";
            $result=MyDatabase::ReadQuery($query);
            $row=$result->fetch_array();
            $this->id=$id;
            $this->level=$row['level'];
            $this->text=$row['text'];
            $this->post_id=$row['post_id'];
            $this->user_id=$row['user_id'];
            $this->userimage=$row['userimage'];
            $this->username=$row['username'];
            $this->image_url=$row['image_url'];
            $this->rating=$row['rating'];
            $this->video_url=$row['video_url'];
            $this->parent_comment_id=$row['parent_comment_id'];
            $this->seen=$row['seen'];
            $query2="select * from comments where parent_comment_id={$id}";
            $result=MyDatabase::ReadQuery($query2);
            if($result->num_rows>0)
            {
                $array=array();
                while($row=$result->fetch_assoc())
                {
                    $c_comment=Comment::Create($row['id'],$id);
                    $array[]=$c_comment;
                }
            }
            else{
                $array=null;
            }
            $this->children_comments=$array;
        }
        else{
            $this->id=$row['id'];
            $this->level=$row['level'];
            $this->text=$row['text'];
            $this->post_id=$row['post_id'];
            $this->user_id=$row['user_id'];
            $this->userimage=$row['userimage'];
            $this->username=$row['username'];
            $this->image_url=$row['image_url'];
            $this->rating=$row['rating'];
            $this->video_url=$row['video_url'];
            $this->parent_comment_id=$row['parent_comment_id'];
            $this->seen = $row['seen'];
            $this->children_comments=null;
        }
    }
    
    public function getJson($post_title=false){
        $parent_comment_user_id='';
        $parent_comment_username='';
        $parent_comment_userimage='';
        $parent_comment_text='';
        $parent_comment_image='';
        $parent_comment_video='';
        if($this->parent_comment_id){
            $p_com=self::SingleComment($this->parent_comment_id);
            $parent_comment_username=$p_com->username;
            $parent_comment_userimage=User::getCommentImage($p_com->user_id);
            $parent_comment_text=$p_com->text;
            $parent_comment_image=self::fix_url($p_com->image_url);
            $parent_comment_video=self::fix_url($p_com->video_url);
            $parent_comment_user_id=$p_com->user_id;
        }
        $array=[
            'id'=>$this->id,
            'level'=>$this->level,
            'text'=>$this->text,
            'post_id'=>$this->post_id,
            'user_id'=>$this->user_id,
            'username'=>$this->username,
            'userimage'=>self::fix_url(User::getCommentImage($this->user_id)),
            'image_url'=>self::fix_url($this->image_url),
            'video_url'=>self::fix_url($this->video_url),
            'rating'=>$this->rating,
            'seen'=>$this->seen,
            'parent_comment_user_id'=>$parent_comment_user_id,
            'parent_comment_username'=>$parent_comment_username,
            'parent_comment_userimage'=>$parent_comment_userimage,
            'parent_comment_text'=>$parent_comment_text,
            'parent_comment_image'=>$parent_comment_image,
            'parent_comment_video'=>$parent_comment_video
        ];
        return $array;
    }
    public static function SingleComment($id){
        $query="select * from comments where id={$id}";
        $result=MyDatabase::ReadQuery($query);
        $row=$result->fetch_array();
        return new Comment(1,$row);
    }
    static function Create($id,$parent_id){
        $com=new Comment($id);
        $com->parent_comment_id=$parent_id;
        return $com;
    }
    public function username(){
        $id=$this->user_id;
        $query="select username from user where id={$id}";
        $result=MyDatabase::ReadQuery($query);
        $row=$result->fetch_assoc();
        return $row['username'];
    }
    static function add_comment($text,$image,$video_url,$user_id,$username,$user_image,$post_id,$parent_comment_id)
    {
        $date=date('Y-m-d H:i:s');
        $db=MyDatabase::getInstance();
        $level=1;
        $seen=0;
        if($parent_comment_id!=0){
            $level=MyDatabase::GetParameter('comments',$parent_comment_id,'level');
            $level++;            
            $parent_comment_user_id=MyDatabase::GetParameter('comments',$parent_comment_id,'user_id');            
        }else{
            $parent_comment_user_id=MyDatabase::GetParameter('post',$post_id,'user_id');            
        }
        if($parent_comment_user_id==$user_id) $seen = 1;
        $query="insert into comments(level,text,post_id,user_id,username,userimage,image_url,parent_comment_id,add_date,seen,parent_comment_user_id)
          values({$level},'{$text}',{$post_id},{$user_id},'{$username}','{$user_image}','{$image}',{$parent_comment_id},'{$date}',{$seen},{$parent_comment_user_id})";
        $db->query($query);
        $id=$db->insert_id;
        $query = "update user_images set number=number+1 where image_url='{$image}' or (video_url='{$video_url}' and video_url<>'')";
        $db->query($query);
        if(!$db->affected_rows){
          $query = "insert into user_images(user_id,image_url,video_url,number) values({$user_id},'{$image}','{$video_url}',1)";          
        }
        return $id;
    }
    static function comments_array($post_id){
        $query="select id from comments where post_id={$post_id} and parent_comment_id=0";
        $result=MyDatabase::ReadQuery($query);
        $comments_array=array();
        if($result) {
            while ($row = $result->fetch_assoc()) {
                $comment = new Comment($row['id']);
                $comments_array[] = $comment;
            }
        }
        return $comments_array;
    }

    static function comments_list_mobile($post_id){
        if(!$post_id){
            return null;
        }
        $query="select * from comments
              where post_id={$post_id} order by add_date";
        $result=MyDatabase::ReadQuery($query);
        $list=array();
        while($row=$result->fetch_assoc()){
            $list[$row['id']]=new Comment($row['id'],$row);
        }
        return $list;
    }
    static function getMaxId(){
        $result=MyDatabase::ReadQuery('select id from comments order by id desc limit 1');
        if($result){
            $res=$result->fetch_assoc();
            if($res){
                return $res['id'];
            }
        }
        return 0;
    }
    public function getAuthorImage(){
        $user=new User($this->user_id);
        if($user->provider==''){
            $image="<img class='comment_user_image' src='/images/user_images/{$this->user_id}.jpg'>";
        }
        else{
            $image="<img class='comment_user_image' src='{$user->image}'>";
        }
        return $image;
    }
    public function ElapsedTime()
    {
        $id = $this->id;
        $query = "select hour(timediff(now(),add_date)) as hours,minute(timediff(now(),add_date)) as minutes,datediff(now(),add_date) as days
                from comments where id={$id}";
        $result = MyDatabase::ReadQuery($query);
        $res = $result->fetch_assoc();
        $days = $res['days'];
        if ($days > 0) {
            return $days . " days ago";
        } else {
            $hours = $res['hours'];
            if ($hours > 0) {
                return $hours . " hours ago";
            } else {
                $minutes = $res['minutes'];
                if ($minutes == 0) $minutes = 1;
                return $minutes . " minutes ago";
            }

        }
    }
    static function elapsed_time($id){
        $query = "select hour(timediff(now(),add_date)) as hours,minute(timediff(now(),add_date)) as minutes,datediff(now(),add_date) as days
                from comments where id={$id}";
        $result = MyDatabase::ReadQuery($query);
        $res = $result->fetch_assoc();
        $days = $res['days'];
        if ($days > 0) {
            return $days . " days ago";
        } else {
            $hours = $res['hours'];
            if ($hours > 0) {
                return $hours . " hours ago";
            } else {
                $minutes = $res['minutes'];
                if ($minutes == 0) $minutes = 1;
                return $minutes . " minutes ago";
            }

        }
    }
    static function user_rating($user_id,$comment_id){
        $query="select value from comments_rating where user_id={$user_id} and comment_id={$comment_id}";
        $res=MyDatabase::ReadQuery($query);
        if($res->num_rows==0){
            return 0;
        }
        else{
            $row=$res->fetch_assoc();
            return $row['value'];
        }
    }

    static function Last_comments($offset=0){
        $offset = (int)$offset;
        $query="select * from comments order by id desc limit 10 offset {$offset}";
        $res=MyDatabase::ReadQuery($query);
        $html="";
        while ($row=$res->fetch_assoc()) {
            $comment=new Comment(1,$row);
            $html.=Views::unresponded_comments($comment);
        }
        return $html;
    }
    static function Last_comments_responds($user_id,$offset=0){
        $offset = (int)$offset;
        $query="select * from comments where (select user_id from comments t2 where t2.id=comments.parent_comment_id)={$user_id} order by id desc limit 10 offset {$offset}";
        $res=MyDatabase::ReadQuery($query);
        $html="";
        while ($row=$res->fetch_assoc()) {
            $comment=new Comment(1,$row);
            $html.=Views::unresponded_comments($comment);
        }
        return $html;
    }
    static function delete($id){
        $query2="select * from comments where parent_comment_id={$id}";
        if(!MyDatabase::Exists($query2)){
            MyDatabase::UpdateQuery("delete from comments where id=".$id);
            @unlink('images/comments_images/'.$id.'.jpg');
            @unlink('images/comments_images/'.$id.'.gif');
            @unlink('images/comments_images/'.$id.'.gif.jpg');
            @unlink('videos/comments/'.$id.'.mp4');
        }        
    }
    
    static function fix_url($url){
        return strpos($url,"://")>1 || $url=='' ? $url : "http://www.exifeed.com".$url;
    }
}