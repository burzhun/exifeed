<?php
require_once('classes/db.php');
require_once('models/Log.php');
class User
{
    public $id;
    public $username;
    private $user;
    public $date;
    public  $email;
    public $image;
    public $provider;
    public $identifier;
    public $last_visit_time;
    public $tags;
    public $blocked_tags;
    public $shorten_longpost;
    public function __construct($id){
        if(isset($_SESSION['user'])&&$_SESSION['user']['id']==$id&&isset($_SESSION['user']['username'])){
            $this->id=$_SESSION['user']['id'];
            $this->type=$_SESSION['user']['type'];
            $this->date=$_SESSION['user']['date'];
            $this->username=$_SESSION['user']['username'];
            $this->image=$_SESSION['user']['image'];
            $this->tags=$_SESSION['user']['tags'];
            $this->blocked_tags=$_SESSION['user']['blocked_tags'];
            $this->provider=$_SESSION['user']['provider'];
            $this->identifier=$_SESSION['user']['identifier'];
            $this->last_visit_time=$_SESSION['user']['last_visit_time'];
            $this->shorten_longpost=$_SESSION['user']['shorten_longpost'];
        }
        else{
            $this->id=$id;
            $parameters=self::getUserParameters($id);
            $this->type=$parameters['type'];
            $this->date=$parameters['date'];
            $this->username=$parameters['username'];
            $this->image=$parameters['image'];
            $this->tags=$parameters['tags'];
            $this->blocked_tags=$parameters['blocked_tags'];
            $this->provider=$parameters['provider'];
            $this->identifier=$parameters['identifier'];
            $this->last_visit_time=$parameters['last_visit_time'];
            $this->shorten_longpost=$parameters['shorten_longpost'];
        }
    }
    static function id(){
        if(User::isLogin()){
            return $_SESSION['user']['id'];
        }
        return 0;
    }
    static function  getUserParameters($id){
        $query="select * from user where id={$id}";
        $result=MyDatabase::ReadQuery($query);
        if($result){
            while($row=$result->fetch_assoc()){
                return $row;
            }
        }
        else{
            return null;
        }
    }
    public function SetParameter($key,$value,$isnumber){
        MyDatabase::SetParameter('user',$this->id,$key,$value,$isnumber);
    }
    public function save(){
        $query="update user  set  email='{$this->email}',image='{$this->image}',
                    username='{$this->username}',type='{$this->type}',shorten_longpost={$this->shorten_longpost} where id={$this->id}";
        MyDatabase::UpdateQuery($query);
    }
    public function reload_user_data(){
        $parameters=self::getUserParameters($this->id);
        $this->type=$parameters['type'];
        $this->date=$parameters['date'];
        $this->username=$parameters['username'];
        $this->image=$parameters['image'];
        $this->tags=$parameters['tags'];
        $this->blocked_tags=$parameters['blocked_tags'];
        $this->provider=$parameters['provider'];
        $this->identifier=$parameters['identifier'];
        $this->last_visit_time=$parameters['last_visit_time'];
        $this->shorten_longpost=$parameters['shorten_longpost'];

        $_SESSION['user']=$parameters;
    }
    static function autorize($id){
        $hash2=md5(mcrypt_create_iv(10,MCRYPT_DEV_URANDOM));
        $user=new User($id);
        $parameters=self::getUserParameters($id);
        MyDatabase::UpdateQuery("insert into cookies(user_id,cookie_hash) values({$id},'{$hash2}')");
        if(isset($_COOKIE['hash'])&&$_COOKIE['hash']!=''){
            setcookie('hash','',time()-1,'/');
        }
        setcookie('hash',$hash2,time()+24*3600*31,'/');
        $_SESSION['user']=(array)$user;
    }
    static function getEmail($id){
        $query="select email from user where id={$id}";
        $result=MyDatabase::ReadQuery($query);
        if($result){
            $row=$result->fetch_row();
            return $row[0];
        }
        else{
            return null;
        }
    }
    static function unautorize(){
        if(User::isLogin()){
            $_SESSION['user']['id']='';
            unset($_SESSION['user']['id']);
        }
        if(isset($_COOKIE['hash'])&&$_COOKIE['hash']!=''){
            $hash=MyDatabase::real_escape_string($_COOKIE['hash']);
            MyDatabase::UpdateQuery("delete from cookies where cookie_hash='{$hash}'");
            setcookie('hash','',time()-1,'/');
        }
    }

    static function check_Cookies(){
        if(isset($_COOKIE['hash'])&&$_COOKIE['hash']!=''){
            $db=MyDatabase::getInstance();
            $hash=$db->real_escape_string($_COOKIE['hash']);
            $query="select user_id from cookies where cookie_hash='{$hash}'";
            $res=$db->query($query);
            if($res->num_rows==1){
                $row=$res->fetch_row();
                $id=$row[0];
                $res=MyDatabase::ReadQuery("select * from user where id={$id}");
                $row=$res->fetch_assoc();
                $_SESSION['user']=$row;
            }
            else{
                $_COOKIE['hash']='';
            }
        }
    }
    
    static function checkuser($email,$password,$captcha = false){
        $db=MyDatabase::getInstance();
        $email=$db->real_escape_string($email);
        $password=$db->real_escape_string($password);
        $query="select hash,password,id from user where email='{$email}'";
        $result=$db->query($query);
        if($result->num_rows>0){            
            $password=$db->real_escape_string($password);
            $row=$result->fetch_assoc();
            $hash=$row['hash'];
            $password=sha1($hash.$password);
            if($password===$row['password']){
                MyDatabase::SetParameter('user',$row['id'],'attempts',0,true);
                return $row['id'];
            }else{
                if($captcha){
                    MyDatabase::SetParameter('user',$row['id'],'attempts',0,true);
                }else{
                    MyDatabase::SetParameter('user',$row['id'],'attempts','attempts+1',true);
                    $n=(int)MyDatabase::GetParameter('user',$row['id'],'attempts');
                    if($n>5){
                        $_SESSION['login_attempts']=6;
                    }
                }
                
            }
        }
        return false;
    }
    static function checkuser_name($name,$password,$captcha = false){
        $db=MyDatabase::getInstance();
        $name=$db->real_escape_string($name);
        $password=$db->real_escape_string($password);
        $query="select hash,password,id from user where username='{$name}'";
        $result=$db->query($query);
        if($result->num_rows>0){
            $row=$result->fetch_assoc();
            MyDatabase::SetParameter('user',$row['id'],'attempts','attempts+1',true);
            $password=$db->real_escape_string($password);
            $hash=$row['hash'];
            $password=sha1($hash.$password);
            if($password==$row['password']){
                return $row['id'];
            }else{
                if($captcha){
                    MyDatabase::SetParameter('user',$row['id'],'attempts',0,true);
                }else{
                    MyDatabase::SetParameter('user',$row['id'],'attempts','attempts+1',true);
                    $n=(int)MyDatabase::GetParameter('user',$row['id'],'attempts');
                    if($n>5){
                        $_SESSION['login_attempts']=6;
                    }
                }
            }
        }
        return false;
    }
    
    static function checkUserApp($name,$password,$captcha){
        $db=MyDatabase::getInstance();
        $name=$db->real_escape_string($name);
        $password=$db->real_escape_string($password);
        if(filter_var($name, FILTER_VALIDATE_EMAIL)){
            $query="select hash,password,id from user where email='{$name}'";
        }else{
            $query="select hash,password,id from user where username='{$name}'";
        }        
        $result=$db->query($query);
        if($result->num_rows>0){
            $row=$result->fetch_assoc();
            MyDatabase::SetParameter('user',$row['id'],'attempts','attempts+1',true);
            $password=$db->real_escape_string($password);
            $hash=$row['hash'];
            $password=sha1($hash.$password);
            if($password==$row['password']){
                return $row['id'];
            }else{
                if($captcha){
                    MyDatabase::SetParameter('user',$row['id'],'attempts',1,true);
                    return false;
                }else{
                    MyDatabase::SetParameter('user',$row['id'],'attempts','attempts+1',true);
                    $n=(int)MyDatabase::GetParameter('user',$row['id'],'attempts');
                    if($n>5){
                        return 'captcha';
                    }
                }                
            }
        }
        $_SESSION['login_attempts']  = isset($_SESSION['login_attempts']) ? $_SESSION['login_attempts']+1 : 1;
        if($_SESSION['login_attempts']>5){
            return 'captcha';
        }
        return false;
    }

    static function isFirstTime(){
      return false;
        if(isset($_COOKIE['first_time'])){
            return false;
        }else{
            setcookie('first_time','1',time()+30*86400,'/');
            return true;
        }
    }
    
    static function FindByToken($token){
        if (!$token) return false;
        $token=preg_replace('/[^0-9a-z]/','',$token);
        $query="select user_id from user_token where token='{$token}'";
        $result=MyDatabase::ReadQuery($query);
        if($result->num_rows>0){
            $row=$result->fetch_assoc();
            $user = new User($row['user_id']);
            return $user;
        }
        return false;
    }
    static function user_exists($email){
        $db=MyDatabase::getInstance();
        $email=$db->real_escape_string($email);
        $query="select id from user where email='{$email}'";
        $result=MyDatabase::ReadQuery($query);
        if($result->num_rows==0){
            return false;
        }
        return true;
    }
    static function username_used($name){
        $db=MyDatabase::getInstance();
        $name=$db->real_escape_string($name);
        $query="select id from user where username='{$name}' and provider=''";
        $result=MyDatabase::ReadQuery($query);
        if($result->num_rows==0){
            return false;
        }
        return true;
    }
    static function createHash()
    {
        $hash=md5(mcrypt_create_iv(10,MCRYPT_DEV_URANDOM));
        return $hash;
    }
    static function GetUserImage(){
        $user=new User(User::id());
        if($user->provider==''){
            if($user->image==''){
                return "/images/icons/no_image_user.png";
            }else{
                return "/images/user_images/".$user->image;
            }
        }else{
            return $user->image;
        }
        return null;
    }
    public static function GetUsername(){
        $id=User::id();
        return MyDatabase::GetParameter('user',$id,'username');
    }
    static function AddUser($email,$name,$password){
        $db=MyDatabase::getInstance();
        $email=$db->real_escape_string($email);
        $name=$db->real_escape_string($name);
        $date=date('Y-m-d H:i:s');
        $password=$db->real_escape_string($password);
        $hash=self::createHash();        
        if(preg_match("/[^0-9a-zA-Z]+/i",$password)||strlen($password)<5){
            return 0;
        }
        $password=sha1($hash.$password);
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){    
            return 0;
        }
        $last_visit_time = date('Y-m-d H:i:s');
        $query="insert into user(email,password,username,date,type,hash,shorten_longpost,last_visit_time)
            values('{$email}','{$password}','{$name}','{$date}','user','{$hash}',1,'{$last_visit_time}')";
        $db->query($query);
        $id=$db->insert_id;
        if($id){
            return $id;
        }
        else{
            return 0;
        }
    }
    
    static function Add_image($id,$image){
        $user=new User($id);
        $ext="";
        switch(exif_imagetype($image['tmp_name']))
        {
            case IMAGETYPE_JPEG: $ext = 'jpg'; break;
            case IMAGETYPE_GIF: $ext = 'gif'; break;
            case IMAGETYPE_PNG: $ext = 'jpg'; break;
        }
        if($ext=='jpg'){
            if($user->image==''){
                $url=$id.'.jpg';
                compress($image['tmp_name'],'/images/user_images/'.$url,75);
                $query="update user set image='{$url}' where id={$id}";
                MyDatabase::UpdateQuery($query);
            }
            else{
                unlink('/images/user_images/'.$id.'jpg');
                $url=$id.'.'.$ext;
                compress($image['tmp_name'],'/images/user_images/'.$url,75);
            }
        }
    }

    static function change_password($id,$new_password){
        $hash=self::createHash();
        if(!(preg_match("/[^0-9a-zA-Z]+/i",$new_password)||strlen($new_password)<3)||strlen($new_password)>30){
            return false;
        }
        $new_password=MyDatabase::real_escape_string($new_password);
        $password=sha1($hash.$new_password);
        $query="update user set hash='{$hash}',password='{$password}' where id={$id}";
        MyDatabase::UpdateQuery($query);
    }
    static function isLogin(){
        return (isset($_SESSION['user'])&&$_SESSION['user']['id']!='');
    }
    static function Add_subscription($subscriber_id,$source_id){
        MyDatabase::UpdateQuery("insert into subscribes(subscriber_id,source_id) values({$subscriber_id},{$source_id})");
    }
    static function Get_subscriptions($id,$offset){
        $tags=MyDatabase::GetParameter('user',$id,'tags');
        $tags=explode(',',$tags);
        $where="";
        foreach($tags as $tag){
            if($tag!=''){
                $where.="or (tags like '{$tag},%') or (tags like '%,{$tag},%') or (tags like '%,{$tag}')";
            }
        }
        $query="SELECT  post.*,ifnull(comments_count,0) as comments_count,users.value as value,author.image as author_image,
                        author.username as author_username,author.provider as author_provider,
                        hour(timediff(now(),post.add_date)) as hours,minute(timediff(now(),post.add_date)) as minutes,
                          datediff(now(),post.add_date) as days
                        FROM (SELECT post . * , post.add_date AS add_date2,0 as user_repost_id
FROM post
UNION ALL SELECT post . * , user_repost.date AS add_date2,user_repost.user_id as user_repost_id
FROM post, user_repost
WHERE post.id=user_repost.post_id and exists(select * from subscribes where subscriber_id={$id} and source_id=user_repost.user_id)) as post
                        left join (select id,post_id,count(id) as comments_count from comments) as com on com.post_id=post.id
                        left join (select value,post_id from users_ratings where user_id={$id}) as users on users.post_id=post.id
                        left join (select id,username,image,provider from user) as author on author.id=post.user_id
                        where (user_id=(select source_id from subscribes where subscriber_id={$id}) or (user_repost_id>0) )
                          and post.user_id<>{$id} and user_repost_id<>{$id} order by add_date2 desc limit 10 offset {$offset}
                           ";
        //echo $query;
        $res=MyDatabase::ReadQuery($query);

        if($res->num_rows==0){
            return null;
        }
        return $res;
    }
    static function Get_last_subscriptions($id,$offset,$time){
        $tags=MyDatabase::GetParameter('user',$id,'tags');
        $tags=explode(',',$tags);
        $where="";
        foreach($tags as $tag){
            if($tag!=''){
                $where.="or (tags like '{$tag},%') or (tags like '%,{$tag},%') or (tags like '%,{$tag}')";
            }
        }
        $query="SELECT  post.*,ifnull(comments_count,0) as comments_count,users.value as value,author.image as author_image,
                        author.username as author_username,author.provider as author_provider,
                        hour(timediff(now(),post.add_date)) as hours,minute(timediff(now(),post.add_date)) as minutes,
                          datediff(now(),post.add_date) as days
                        FROM (SELECT post . * , post.add_date AS add_date2,0 as user_repost_id
FROM post
UNION ALL SELECT post . * , user_repost.date AS add_date2,user_repost.user_id as user_repost_id
FROM post, user_repost
WHERE post.id=user_repost.post_id and exists(select * from subscribes where subscriber_id={$id} and source_id=user_repost.user_id)) as post
                        left join (select id,post_id,count(id) as comments_count from comments) as com on com.post_id=post.id
                        left join (select value,post_id from users_ratings where user_id={$id}) as users on users.post_id=post.id
                        left join (select id,username,image,provider from user) as author on author.id=post.user_id
                        where (user_id=(select source_id from subscribes where subscriber_id={$id}) or (user_repost_id>0) )
                         and (add_date>'{$time}' or update_date>'{$time}') and post.user_id<>{$id} and user_repost_id<>{$id} order by add_date2 desc limit 10 offset {$offset}
                           ";
        //echo $query;
        $res=MyDatabase::ReadQuery($query);
        if($res->num_rows==0){
            return null;
        }
        return $res;
    }
    public function Get_Subscribers_Count(){
        $res=MyDatabase::ReadQuery("select count(id) from subscribes where source_id={$this->id}");
        $row=$res->fetch_row();
        return $row[0];
    }
    public function Get_Subscriptions_Count(){
        $res=MyDatabase::ReadQuery("select count(id) from subscribes where subscriber_id={$this->id}");
        $row=$res->fetch_row();
        return $row[0];
    }
    public function isSubscribed($id){
        $query="select source_id from subscribes where subscriber_id={$this->id} and source_id={$id}";
        $result=MyDatabase::ReadQuery($query);
        if($result->num_rows==0){
            return false;
        }
        else{
            return true;
        }
    }
    static function save_post($id,$post_id){
        MyDatabase::UpdateQuery("insert into saved_posts(post_id,user_id) values({$post_id},{$id})");
    }
    static function Get_saved_posts($id,$offset){
        $query="select post.*,count(com.id) as comments_count,users.value as value,author.image as author_image,
                        author.username as author_username,author.provider as author_provider,
                        hour(timediff(now(),post.add_date)) as hours,minute(timediff(now(),post.add_date)) as minutes,
                          datediff(now(),post.add_date) as days from post
                          left join (select id,post_id from comments) as com on com.post_id=post.id
                        left join (select value,post_id from users_ratings where user_id={$id}) as users on users.post_id=post.id
                        left join (select id,username,image,provider from user) as author on author.id=post.user_id
                where exists(select * from saved_posts where user_id={$id} and post_id=post.id)
                group by post.id order by post.add_date desc limit 10 offset {$offset}";
        $res=Post::LoadPosts("exists(select * from saved_posts where user_id={$id} and post_id=post.id)",'post.add_date desc',10,$offset);
        //$res=MyDatabase::ReadQuery($query);
        return $res;
    }
    static function check_user_provider($identifier,$provider){
        $query="select id from user where identifier='{$identifier}' and provider='{$provider}' limit 1";
        $result=MyDatabase::ReadQuery($query);
        if($result->num_rows==0){
            return false;
        }
        else{
            $row=$result->fetch_row();
            return $row[0];
        }
    }
    static function add_provider_user($provider,$identifier,$email,$name,$image){
        $db=MyDatabase::getInstance();
        $email=$db->real_escape_string($email);
        $name=$db->real_escape_string($name);
        $date=date('Y-m-d H:i:s');
        $query="insert into user(email,provider,identifier,password,username,date,type,hash,image,shorten_longpost)
            values('{$email}','{$provider}','{$identifier}','','{$name}','{$date}','user','','{$image}',1)";
        $db->query($query);
        $id=$db->insert_id;
        return $id;
    }
    private function reload_visit_time(){
        $date=date('Y-m-d H:i:s');
        MyDatabase::SetParameter('user',$this->id,'last_visit_time',$date,false);
    }
    static  function reload_visit_time_id($user_id){
        $date=date('Y-m-d H:i:s');
        MyDatabase::SetParameter('user',$user_id,'last_visit_time',$date,false);
    }
    public function Add_subscribe_tag($tag){
        $tag=MyDatabase::real_escape_string($tag);
        $tags=$this->tags.$tag.',';
        $this->SetParameter('tags',$tags,false);
        $this->reload_user_data();
    }
    public function Delete_subscribe_tag($tag){
        $tags=$this->tags;
        $tags=str_replace(','.$tag.',',',',$tags);
        $this->SetParameter('tags',$tags,false);
        $this->reload_user_data();
    }
    public function Add_block_tag($tag){
        $tag=MyDatabase::real_escape_string($tag);
        $tags=$this->blocked_tags.$tag.',';
        $this->SetParameter('blocked_tags',$tags,false);
        $this->reload_user_data();
    }
    public function Delete_block_tag($tag){
        $tags=$this->blocked_tags;
        $tags=str_replace(','.$tag.',',',',$tags);
        $this->SetParameter('blocked_tags',$tags,false);
        $this->reload_user_data();
    }
    public function User_tag_status($tag){
        $tags=$this->tags;
        $blocked_tags=$this->blocked_tags;
        if(strpos($tags,','.$tag.',')!==false){
            return 1;
        }
        else{
            if(strpos($blocked_tags,','.$tag.',')!==false){
                return -1;
            }
            else{
                return 0;
            }
        }
    }
    static function Repost($user_id,$post_id){
        $date=date('Y-m-d H:i:s');
        $query="insert into user_repost(user_id,post_id,date) values({$user_id},{$post_id},'{$date}')";
        MyDatabase::UpdateQuery($query);
    }
    static function AddTag($tag){
        $db=MyDatabase::getInstance();
        $tag=$db->real_escape_string($tag);
        $query="insert into recent_tags(tag,number) values('{$tag}',1) on duplicate key update number=number+1";
        $db->query($query);
        $db->close();
    }
    static function FindUser($text){
        $query="select id from user  where  username='{$text}'";
        //echo $query;
        $res=MyDatabase::ReadQuery($query);
        $row=$res->fetch_assoc();
        $user=new User($row['id']);
        return $user;
    }
    static function FindUsers($text){
        $query="select * from user  where  match(username) against('{$text}')";
        $res=MyDatabase::ReadQuery($query);
        return $res;
    }

    public function PostCount(){
        $query="select count(id) as count from post where user_id={$this->id}";
        $res=MyDatabase::ReadQuery($query);
        $row=$res->fetch_assoc();
        return $row['count'];
    }
    public function registeredTime(){

    }
    public static function getCommentImage($id){
        $query="select provider,image from user where id={$id}";
        $result=MyDatabase::ReadQuery($query);
        $row=$result->fetch_assoc();
        if($row['provider']!=''){
            return $row['image'];
        }else{
            if($row['image']!=''){
                return '/images/user_images/'.$row['image'];
            }else{
                return "/images/icons/no_image_user.png";
            }
        }
    }
    
    public static function isAdmin(){        
        return self::isLogin()&&self::id()<=41;
    }
    
    public function GetImageLink(){
        $image="http://exifeed.com/images/icons/no_image_user.png";
        if($this->image!=''){
            if($this->provider==''){
                $image="http://exifeed.com/images/user_images/".$this->image;
            }
            else{
                $image=$this->image;
            }
        }
        return $image;
    }
    
    public function UnreadCommentsCount(){
        $query="select count(id) as count from comments  where seen=0 and parent_comment_user_id={$this->id}";
        $res=MyDatabase::ReadQuery($query);
        $row=$res->fetch_assoc();
        return $row['count'];
    }
}