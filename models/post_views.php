<?php
require_once('classes/db.php');
require_once('models/Log.php');
class Post_views{
    public $id;
    public $user_id;
    public $post_id;
    static function isVisited($post_id,$user_id){
        return MyDatabase::Exists("select * from post_views where post_id={$post_id} and user_id={$user_id}");
    }
    static function Add_view($post_id,$user_id){
        if(!self::isVisited($post_id,$user_id)){
            MyDatabase::UpdateQuery("insert into post_views(post_id,user_id) values({$post_id},{$user_id})");
        }
    }
}