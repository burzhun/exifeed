<?php
require_once('classes/db.php');
class UserIdeas{
    public $id;
    public $user_id;
    public $idea;
    public $date;
    public $rating;

    function __construct($user_id,$idea,$date,$rating){
        $this->user_id=$user_id;
        $this->idea=$idea;
        $this->date=$date;
        $this->rating=$rating;
    }

    static function LoadById($id){
        $query="select * from userideas where id={$id}";
        $res=MyDatabase::ReadQuery($query);
        if($res){
            $row=$res->fetch_assoc();
            $user_idea=new UserIdeas($row['user_id'],$row['idea'],$row['date'],$row['rating']);
            return $user_idea;
        }
        else{
            return null;
        }
    }
    static function UpvoteIdea($id){
        if(User::isLogin()){
            $user_id=User::id();
            if(!MyDatabase::Exists("select id from ideas_upvotes where user_id={$user_id} and idea_id={$id}")){
                MyDatabase::SetParameter('userideas',$id,'rating','rating+1',true);
            }

        }

    }
    static function AddIdea($user_id,$idea){
        $date=date('Y-m-d H:i:s');
        $query="insert into userideas(user_id,idea,date,rating) values({$user_id},'{$idea}','{$date}',1)";
        $id=MyDatabase::UpdateQuery($query);
        MyDatabase::UpdateQuery("insert into ideas_upvotes(user_id,idea_id) values({$user_id},{$id})");
        return $id;
    }
    static function GetIdeas($limit,$offset=0){
        $query="select * from userideas order by rating desc limit {$limit} offset {$offset}";
        //Log::AddLogData($query);
        $res=MyDatabase::ReadQuery($query);
        return $res;
    }
}
