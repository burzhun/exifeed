<?php
require_once('classes/db.php');


class Post{
    public $id;
    public $title;
    public $description;
    public $type;
    public $text;
    public $android_text;
    public $image;
    public $url;
    public $user_id;
    public $tags;
    public $rating;
    public $elapsed_time;
    public $comments_number;
    public $user_rating;
    public $author_username;
    public $author_image;
    public $author_provider;
    public $user_repost_id;
    public $add_date;
    public $update_date;
    public $has_script;
    public $preview_image;
    function __construct($row){
        if(isset($row['id'])){
            $this->id=$row['id'];
            $this->title=$row['title'];
            $this->image=$row['image'];
            $this->text=$row['text'];
            $this->android_text=$row['android_text'];
            $this->type=$row['type'];
            //$this->url=$row['url'];
            $this->add_date=$row['add_date'];
            $this->update_date=$row['update_date'];
            $this->description=$row['description'];
            $this->user_id=$row['user_id'];
            $this->rating=$row['rating'];
            $this->author_username=$row['author_username'];
            $this->author_image=$row['author_image'];
            $this->author_provider=$row['author_provider'];
            $this->tags=explode(',',$row['tags']);
            $this->comments_number=$row['comments_count'];
            $this->has_script=$row['has_script'];
            $this->preview_image=$row['preview_image'];
            $days=$row['days'];
            $hours=$row['hours'];
            $minutes=$row['minutes'];
            $this->elapsed_time=self::ElapsedTime($days,$hours,$minutes);
            $this->user_rating=0;
            if(isset($row['value'])){
                $this->user_rating=$row['value'];
            }
            $this->user_repost_id=0;
            if(isset($row['user_repost_id'])){
                $this->user_repost_id=$row['user_repost_id'];
            }
            //print_r($row);
        }
        else{
            $row=self::GetPostById($row);
            if($row!=null){
                $this->id=$row['id'];
                $this->title=$row['title'];
                $this->image=$row['image'];
                $this->text=$row['text'];
                $this->android_text=$row['android_text'];
                $this->type=$row['type'];
               // $this->url=$row['url'];
                $this->add_date=$row['add_date'];
                $this->update_date=$row['update_date'];
                $this->description=$row['description'];
                $this->user_id=$row['user_id'];
                $this->rating=$row['rating'];
                $this->author_username=$row['author_username'];
                $this->author_image=$row['author_image'];
                $this->author_provider=$row['author_provider'];
                $this->tags=explode(',',$row['tags']);
                $this->comments_number=$row['comments_count'];
                $this->has_script=$row['has_script'];
                $this->preview_image=$row['preview_image'];
                $days=$row['days'];
                $hours=$row['hours'];
                $minutes=$row['minutes'];
                $this->elapsed_time=self::ElapsedTime($days,$hours,$minutes);
                $this->user_rating=0;
                if(isset($row['value'])){
                    $this->user_rating=$row['value'];
                }
                $this->user_repost_id=0;
                if(isset($row['user_repost_id'])){
                    $this->user_repost_id=$row['user_repost_id'];
                }
            }else{
                return null;
            }


        }
    }

    public function get_user_name(){
        $id=$this->user_id;
        $user=new User($id);
        return $user->username;
    }
    static function GetPostById($id){
        if(User::isLogin()){
            $user_id=$_SESSION['user']['id'];
            $query="SELECT  post.*,count(com.id) as comments_count,users.value as value,author.image as author_image,
                        author.username as author_username,author.provider as author_provider,
                        hour(timediff(now(),post.add_date)) as hours,minute(timediff(now(),post.add_date)) as minutes,
                          datediff(now(),post.add_date) as days
                        FROM post
                        left join (select id,post_id from comments) as com on com.post_id=post.id
                        left join (select value,post_id from users_ratings where user_id={$user_id}) as users on users.post_id=post.id
                        left join (select id,username,image,provider from user) as author on author.id=post.user_id
                        where post.id={$id} GROUP BY post.id  ";
        }
        else{
            $query="SELECT  post.*,count(com.id) as comments_count,author.image as author_image,
                        author.username as author_username,author.provider as author_provider,
                        hour(timediff(now(),post.add_date)) as hours,minute(timediff(now(),post.add_date)) as minutes,
                          datediff(now(),post.add_date) as days
                        FROM post
                        left join (select id,post_id from comments) as com on com.post_id=post.id
                        left join (select id,username,image,provider from user) as author on author.id=post.user_id
                        where post.id={$id}  GROUP BY post.id  limit 1";
        }
        $res=MyDatabase::ReadQuery($query);
        if($res->num_rows==1){
            $row=$res->fetch_assoc();
            return $row;
        }
        else{
            return null;
        }
    }
    //Get rating of post for this user
    static  function user_rating($user_id,$post_id){
        $query="select value from users_ratings where user_id={$user_id} and post_id={$post_id}";
        $res=MyDatabase::ReadQuery($query);
        if($res->num_rows==0){
            return 0;
        }
        else{
            $row=$res->fetch_assoc();
            return $row['value'];
        }
    }
        
    static function LoadPosts($where,$order,$limit,$offset){
        if(User::isLogin()){
            $user_id=$_SESSION['user']['id'];           
            if(isset($_GET['other_posts'])&&isset($_POST['post_id'])){
              $post_id=(int)$_POST['post_id'];
              if($where != '') $where.=" and post.id<>".$post_id;else $where.= " post.id<>".$post_id;
            }
            if($where!='') $where='where '.$where;
            $query="SELECT  post.*,count(distinct(com.id)) as comments_count,count(post_views.id) as post_views, users.value as value,author.image as author_image,
                        author.username as author_username,author.provider as author_provider,
                        hour(timediff(now(),post.add_date)) as hours,minute(timediff(now(),post.add_date)) as minutes,
                          datediff(now(),post.add_date) as days
                        FROM post
                        left join (select id,post_id from comments) as com on com.post_id=post.id
                        left join (select value,post_id from users_ratings where user_id={$user_id}) as users on users.post_id=post.id
                        left join (select id,username,image,provider from user) as author on author.id=post.user_id
                        left join (select id,post_id from post_views) as post_views on post_views.post_id=post.id
                        {$where} GROUP BY post.id order by {$order} limit {$limit} offset {$offset} ";
            //MyDatabase::UpdateQueries($query);
            //echo $query;

            $result=MyDatabase::ReadQuery($query);
            return $result;
        }
        else{
            if(isset($_GET['other_posts'])&&isset($_POST['post_id'])){
              $post_id=(int)$_POST['post_id'];
              if($where != '') $where.=" and post.id<>".$post_id;else $where.= " post.id<>".$post_id;
            }
            if($where!='') $where='where '.$where;
            $query="SELECT  post.*,count(com.id) as comments_count,count(post_views.id) as post_views,author.image as author_image,
                        author.username as author_username,author.provider as author_provider,
                        hour(timediff(now(),post.add_date)) as hours,minute(timediff(now(),post.add_date)) as minutes,
                          datediff(now(),post.add_date) as days
                        FROM post
                        left join (select id,post_id from comments) as com on com.post_id=post.id
                        left join (select id,username,image,provider from user) as author on author.id=post.user_id
                        left join (select id,post_id from post_views) as post_views on post_views.post_id=post.id
                         {$where} GROUP BY post.id order by {$order} limit {$limit} offset {$offset} ";
            $result=MyDatabase::ReadQuery($query);
            //MyDatabase::UpdateQueries($query);
            return $result;
        }
    }
    static function AddPost($parent_id,$title,$description,$type,$text='',$image='',$tags,$user_id){
        $date=date('Y-m-d H:i:s');
        $title=MyDatabase::real_escape_string($title);
        if($title==''){
            return false;
        }
        if($text!='') $text=MyDatabase::real_escape_string($text);
        if($image!='')$image=MyDatabase::real_escape_string($image);
        if($tags!='') $tags=MyDatabase::real_escape_string($tags);
        if($description!='') $description=MyDatabase::real_escape_string($description);
        $tags_array=explode(',',$tags);
        $tags=','.$tags.','; 
        $coords=Post::GetCoordinates();
		if(!isset($coords->lat)) {$lat='0';$lon = '0';}
		else{$lat=$coords->lat;$lon=$coords->lon;}
        $query="insert into post(parent_id,title,type,description,text,complex_text,image,add_date,update_date,tags,user_id,lat,lng) values
              ({$parent_id},'{$title}','{$type}','{$description}','{$text}','','{$image}','{$date}','{$date}','{$tags}',{$user_id},'{$lat}','{$lon}')";
        $id=MyDatabase::UpdateQuery($query);
        $query="";
        $time=time();
        foreach ($tags_array as $tag2) {
            if($tag2!=''){
                $query.="insert into tags(tag,date) values ('{$tag2}',{$time}) ;";
            }
        }
		
        if($query!=''){
            MyDatabase::UpdateQuery($query);
        }
        
        return $id;
    }
	static function Search($text,$offset,$limit){
		$query="select post.*,hour(timediff(now(),post.add_date)) as hours,minute(timediff(now(),post.add_date)) as minutes,
			datediff(now(),post.add_date) as days,
			count(com.id) as comments_count,author.image as author_image,
			author.username as author_username,author.provider as author_provider,match(title) against ('*{$text}*' IN BOOLEAN MODE) as rel1,match (description) against ('*{$text}*' IN BOOLEAN MODE) as rel2,
			match(text) against('*{$text}*' IN BOOLEAN MODE) as rel3, match(tags) against ('*{$text}*' IN BOOLEAN MODE) as rel4
			from post
			left join (select id,post_id from comments) as com on com.post_id=post.id
			left join (select id,username,image,provider from user) as author on author.id=post.user_id
			where (match(title,description,text,tags) against ('*{$text}*' IN BOOLEAN MODE))
			GROUP BY post.id order by rel1 desc,rel2 desc, rel3 desc, rel4 desc, rating desc limit {$limit} offset {$offset}";
		$res=MyDatabase::ReadQuery($query);
		return $res;
	}
    static function UpdatePost($title,$description,$tags,$post_id){
        $date=date('Y-m-d H:i:s');
        $title=MyDatabase::real_escape_string($title);
        if($title==''){
            return false;
        }
        if($tags!='') $tags=MyDatabase::real_escape_string($tags);
        if($description!='') $description=MyDatabase::real_escape_string($description);
        $tags_array=explode(',',$tags);
        $tags=','.$tags.',';
        $query="update post set title='{$title}',description='{$description}',tags='{$tags}', update_date='{$date}' where id=".$post_id;
        MyDatabase::UpdateQuery($query);

        $query="";
        $time=time();
        foreach ($tags_array as $tag2) {
            if($tag2!=''){
                $query.="insert into tags(tag,date) values ('{$tag2}',{$time}) ;";
            }
        }
        if($query!=''){
            MyDatabase::UpdateQuery($query);
        }


    }
    
    static function AddData($post_id,$text,$image,$maxwidth=''){
        $date=date('Y-m-d H:i:s');
        $text=MyDatabase::real_escape_string(url_link(htmlentities($text)));
        $html="";
        if($text!=''){    
            $html.="<div class='post_addition_text'>{$text}</div>";
        }
        if($image!=''){
            $html.="<div class='post_addition_image' style='max-width:".$maxwidth."px'>{$image}</div>";
        }
        $html.="</div>";
        $html=MyDatabase::real_escape_string($html);
        $query="update post set update_date='{$date}',text=concat(text,'{$html}') where id=".$post_id;
        //echo $query;
        MyDatabase::UpdateQuery($query);
    }

    static function Popular_tags(){
        $time=-time()-86400;
        $query="select * from tags where date>{$time} group by tag order by count(tag) desc limit 5";
        $res=MyDatabase::ReadQuery($query);
        $html="";
        while($row=$res->fetch_assoc()){
            $html.="<a href='/tags/{$row['tag']}' class='tag'>#{$row['tag']}</a></br>";
        }
        return $html;
    }
    static function get_Post_by_title($title){
        $title=MyDatabase::real_escape_string($title);
        $result=MyDatabase::ReadQuery("select * from post where title='{$title}'");
        $row=$result->fetch_assoc();
        return $row;
    }

    static function getMaxId(){
        $result=MyDatabase::ReadQuery('select max(id) from post ');
        if($result){
            $res=$result->fetch_assoc();
            if($res){
                return $res['max(id)'];
            }
        }
        return 0;
    }
    static function ElapsedTime($days,$hours,$minutes){
        if($days>0){
            return $days." days ago";
        }
        else{
            if($hours>0){
                return $hours." hours ago";
            }
            else{
                if($minutes==0) $minutes=1;
                return $minutes." minutes ago";
            }

        }

    }
    static function updateRating($post_id,$value){
        $query="update post set rating=rating+{$value}, current_rating=current_rating+{$value} where id={$post_id}";
        MyDatabase::UpdateQuery($query);
    }
    static function isSaved($user_id,$post_id){
        $query="select id from saved_posts where user_id={$user_id} and post_id={$post_id}";
        return MyDatabase::Exists($query);
    }

    public function GetViews(){
        $post_id=$this->id;
        $query="select count(id) from post_views where post_id={$post_id}";
        $res=MyDatabase::ReadQuery($query);
        $row=$res->fetch_array();
        return $row[0];
    }
    public function getTagsList(){
        $tags="Exifeed,".implode(',',$this->tags);
        return $tags;
    }

    static function delete($id){
        MyDatabase::UpdateQuery("delete from post where id=".$id);
        $files=scandir('images/post/'.$id);
        foreach ($files as $key => $value) {
            if($key>1){
                @unlink('images/post/'.$id.'/'.$value);
            }
        }
        $files=scandir('videos/post/'.$id);
        foreach ($files as $key => $value) {
            if($key>1){
                @unlink('videos/post/'.$id.'/'.$value);
            }
        }
        @rmdir('images/post/'.$id);
        @rmdir('videos/post/'.$id);
    }

    static function post_to_buffer($post_id){
        require_once('includes/buffer/buffer.php');
        $post=new Post($post_id);
        $code=$post->text;
        $dom = new DOMDocument();
        $dom->loadHTML($code);
        //print_r($dom);
        $post_parts=$dom->getElementsByTagName("div");
        $img_link="";
        $video_link="";
        $coub="";

        foreach ($post_parts as $part) {
            $class=$part->GetAttribute('class');
            if($class=='long_post_part image'){
                $img=$part->getElementsByTagName('img');
                $img_link=$img[0]->GetAttribute('src');  
                break;                           
            }
            if($class=='long_post_part coub'){

            }
        }
        exit;
        $client_id = 'yourid';
        $client_secret = 'yoursecret';

        $buffer = new BufferApp($client_id, $client_secret, null);

        $profiles = array(
            '561aa0563v24244c321316b1', 
            '561aa056563bb32c321316b1',
        );

        foreach($profiles as $profile){
            $buffer->go('/updates/create', array(
                'text' => 'My article title',
                'media[title]'=>  'My article title',
                'media[link]'=>  'http://domain.com',
                'media[description]'=>  'you will love this story!',
                'media[picture]'=>  'http://domain.com/images/story.jpg',
                'media[thumbnail]'=>  'http://domain.com/images/story.jpg',
                'profile_ids[]' => $profile
            ));
        }
        //print_r($post_parts);
    }

    static function GetCoordinates(){
        $data=json_decode(file_get_contents("http://ip-api.com/json/".$_SERVER['REMOTE_ADDR']));
        return $data;
    }
    
    static function getPostPreviewImage($id){
        require_once('includes/phpQuery-onefile.php');
        $post=new Post($id);
        if(!$post){
            return null;
        }
        if($post->preview_image!=''){
            //return $post->preview_image;
        }
        $doc = phpQuery::newDocument($post->text);
        foreach($doc->find('.long_post_part') as $div){
            if(pq($div)->hasClass('coub_video')){
                $s=pq(pq($div)->find('.coub_image'))->attr('src');
                MyDatabase::SetParameter('post',$post->id,'preview_image',$s,false);
                return $s;
            }
            if(pq($div)->hasClass('image')){
                $s=pq(pq($div)->find('img'))->attr('src');
								if(strpos($s,'http://') === false && strpos($s,'https://')===false) $s="http://www.exifeed.com".$s;	
                MyDatabase::SetParameter('post',$post->id,'preview_image',"http://www.exifeed.com".$s,false);
                return $s;
            }
            if(pq($div)->hasClass('video')){
                if(pq($div)->find('.youtube_image')){
                    $s=pq(pq($div)->find('.youtube_image'))->attr('src');
                    if($s){
                        MyDatabase::SetParameter('post',$post->id,'preview_image',$s,false);
                        return $s;
                    }                    
                }
                $s=pq(pq($div)->find('iframe'))->attr('src');
                if(strstr($s,'youtube.com')){
                    $s=explode('embed/',$s)[1];
                    $url="http://img.youtube.com/vi/".$s."/sddefault.jpg";
                    MyDatabase::SetParameter('post',$post->id,'preview_image',$url,false);
                    return $url;
                }
            }
        }
        return '';
       // echo $doc->htmlOuter();
    }

    public function getJsonArray(){
        require_once('includes/phpQuery-onefile.php');
		require_once("functions/coub_video.php");
        $doc = phpQuery::newDocument($this->text);
        $json=array();
		$index=0;		
        foreach($doc->find('.long_post_part') as $div){
			$index++;
            if(pq($div)->hasClass('text')){
                pq(pq($div)->find('.long_post_delete_button'))->remove();
                $text=pq($div)->html();
                $ar2=['type'=>'text','title'=>$text,'image'=>'','audio'=>'','video'=>'','id'=>''];
                $json[]=$ar2;
                continue;
            }
            if(pq($div)->hasClass('coub_video')){
                $id='';
                if(pq($div)->find('.coub_play')->attr('v_id')){
                    $id=pq($div)->find('.coub_play')->attr('v_id');
                }elseif(pq($div)->find('#coubVideo')){
                    $s=pq($div)->find('#coubVideo')->attr('src');
                    $s=explode('?',$s)[0];
                    $id=explode('embed/',$s)[1];
                }
                if($id){
                    $data=file_get_contents("http://coub.com/api/v2/coubs/".$id);
                    $data=json_decode($data);
                    $title=$data->title;
                    $mp4=$data->file_versions->html5->video->med->url;
                    $mp3=$data->file_versions->mobile->looped_audio;
					if(!is_dir("videos/post/". $this->id)){
						mkdir("videos/post/" . $this->id);
					}
					$mp4='http://exifeed.com/'.MakeCoub("videos/post/" . $this->id.'/',$index,$mp4,$mp3);
                    $image=$data->picture;
                    $ar=['type'=>'coub','title'=>$title,'video'=>$mp4,'audio'=>$mp3,'image'=>$image,'id'=>$id];
                    $json[]=$ar;
                }
                continue;
            }
            if(pq($div)->hasClass('youtube_video')){
                if(pq($div)->find('.youtube_play')){
                    $image=pq($div)->find('.youtube_image')->attr('src');
                    $title=pq($div)->find('.youtube_name')->html();
                    $id=pq($div)->find('.youtube_play')->attr(v_id);
                    $ar2=['type'=>'youtube','title'=>$title,'image'=>$image,'audio'=>'','video'=>'','id'=>$id];
                    $json[]=$ar2;
                }
            }
            //https://forums.xamarin.com/discussion/59329/playing-an-youtube-video-from-ios-app
            if(pq($div)->hasClass('image')){
                if(pq($div)->find('.gif_play_button')->attr('src')){
                    $image=pq($div)->find('.gif_image')->attr('src');
                    if(strpos($image,'http://')===false){
                        $image='http://exifeed.com'.$image;
                    }
                    $video=pq($div)->find('.gif_play_button')->attr('video_src');
                    if($video){
                        $ar2=['type'=>'gif','title'=>'','image'=>$image,'video'=>$video,'audio'=>'','id'=>''];
                    }else{
                        $video=pq($div)->find('.gif_play_button')->attr('gif_src');
                        $ar2=['type'=>'gif2','title'=>'','image'=>$image,'video'=>$video,'audio'=>'','id'=>''];
                    }
                    
                    $json[]=$ar2;
                }else{
                    $image=pq($div)->find('img')->attr('src');
                    if(strpos(str_replace('https://','http://',$image),'http://')===false){
                        $image='http://exifeed.com'.$image;
                    }
                    $ar=['image'=>$image];
                    $ar2=['type'=>'image','image'=>$image,'video'=>'','audio'=>'','id'=>''];
                    $json[]=$ar2;
                }
                continue;
            }
            if(pq($div)->hasClass('video_uploaded')){
                $video=pq($div)->find('video')->attr('src');
								if($video){
									if(!strpos($video,'http://')){
                    $video='http://exifeed.com'.$video;
									}
									$ar=['video'=>$video];
									$ar2=['type'=>'video','image'=>'','video'=>'','audio'=>'','id'=>'','title'=>''];
								}else{
									$image=pq($div)->find('.gif_image')->attr('src');
                    if(strpos($image,'http://')===false){
                        $image='http://exifeed.com'.$image;
                    }
                    $video=pq($div)->find('.gif_play_button')->attr('video_src');
                    if($video){
                        $ar2=['type'=>'gif','title'=>'','image'=>$image,'video'=>$video,'audio'=>'','id'=>''];
                    }else{
                        $video=pq($div)->find('.gif_play_button')->attr('gif_src');
                        $ar2=['type'=>'gif2','title'=>'','image'=>$image,'video'=>$video,'audio'=>'','id'=>''];
                    }
								}                
                $json[]=$ar2;
                continue;
            }
        }
        return $json;
    }
    
    public function GetUserImageLink(){
        $image="/images/icons/no_image_user.png";
        if($this->author_image!=''){
            if($this->author_provider==''){
                $image="/images/user_images/".$this->author_image;
            }
            else{
                $image=$this->author_image;
            }
        }
        return $image;
    }
    
    public function GetApiJson(){
        $array=['id'=>$this->id,
                'user_id'=>$this->user_id,
                'title'=>stripslashes(htmlspecialchars($this->title)),
                'username'=>$this->author_username,
                'userimage'=>$this->GetUserImageLink(),
                'text'=>$this->android_text,
                'elapsed_time'=>$this->elapsed_time,
                'comments_count'=>$this->comments_number,
				'saved' => 0,
				'tags'=>implode(',',$this->tags),
				'rating'=>$this->rating,
				'user_rating'=>'-1',
                'description'=>$this->description];
			if(User::isLogin()){
				$array['user_rating'] = $this->user_rating;
				if(Post::isSaved(User::id(),$this->id)){
					$array['saved'] = 1;
				}
			}
        return $array;
    }
	//http://www.cc.com/video-clips/6dlnrd/the-colbert-report-ktvu-tv-on-asiana-airlines-crash
		static function TweetPost($post_id){
			require_once('includes/twit/twitteroauth/twitteroauth.php');
			require_once('includes/twit/config.php');
			$post = new Post($post_id);
			$url = "http://www.exifeed.com/post/id".$post_id;
			$title=$post->title;
			if(strlen($title>=140)){
				$title = substr($title,0,139);
			}
			$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET);
			$content = $connection->get('account/verify_credentials');
			$content = $connection->post('statuses/update', array('status' => $title . ' ' . $url));
			print_r($content);
		}
}
