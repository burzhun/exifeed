<?php
session_start();
require_once('models/user.php');
require_once('Views.php');
require_once('classes/Page.php');
require_once('classes/Resource.php');
$message="";
//print_r($_POST);
if(isset($_POST['email'])&&isset($_POST['name'])&&isset($_POST['password']))
{
	$password=$_POST['password'];
    if(!User::user_exists($_POST['email'])&&!User::username_used($_POST['name'])&& !(preg_match("/[^0-9a-zA-Z]+/i",$password)||strlen($password)<3))
    {
        User::AddUser($_POST['email'],$_POST['name'],$_POST['password']);
        $_SESSION["registration_confirmed"]="1";
        header('Location:registration_confirmed.php');
    }
    else{
        $_SESSION["registration_confirmed"]="0";
    }
} 
$header=Views::header_form();
$form=Views::registration_form();
$page=new Page();
$resource=new Resource('registration');
$page->add_css($resource->css)->add_javascript($resource->javascript)->set_title('Main Page')->mainbody($form);
$html=$page->CreatePage();
echo $html;