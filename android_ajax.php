<?
require_once('classes/db.php');
require_once('models/user.php');
require_once('classes/router.php');
require_once('models/post.php');
require_once('models/comment.php');
require_once('functions/sanitize.php');
require_once('Views.php');


    if(isset($_POST['android_menu'])){
        $link=str_replace('http://exifeed.com/','',$link);
        $type=preg_replace("/[^a-z]+/",'',$link);
        $offset=isset($_POST['offset']) ? $_POST['offset'] :0;
        switch($type)
    {
        case 'index':
            $query="select * from post ORDER by recent_rating desc limit 10 offset {$offset} ";
            $res=Post::LoadPosts('','recent_rating desc',10,$offset);
            $html="";
            while($row=$res->fetch_assoc()){
                $post=new Post($row);
                $html.=Views::Post($post,false,$user_id);
            }

            echo $html;
            break;
        case 'find':
            $db=MyDatabase::getInstance();
            if(isset($_GET['text'])){
                $text=$db->real_escape_string($_GET['text']);
                if(strlen($text)>3){
                    $type="";
                    if(isset($_GET['text_search'])||isset($_GET['image_search'])){
                        $type="and";
                        if(isset($_GET['text_search'])){
                            if(isset($_GET['image_search'])){
                                $type.="(type='text' or type='image')";
                            }
                            else{
                                $type.="(type='text')";
                            }
                        }
                        else{
                            $type.="(type='image')";
                        }
                    }
                    $query="select post.*,hour(timediff(now(),post.add_date)) as hours,minute(timediff(now(),post.add_date)) as minutes,
                                datediff(now(),post.add_date) as days,
                                count(com.id) as comments_count,author.image as author_image,
                                author.username as author_username,author.provider as author_provider,match(title) against ('*{$text}*' IN BOOLEAN MODE) as rel1,match (description) against ('*{$text}*' IN BOOLEAN MODE) as rel2,
                                match(text) against('*{$text}*' IN BOOLEAN MODE) as rel3, match(tags) against ('*{$text}*' IN BOOLEAN MODE) as rel4
                                from post
                                left join (select id,post_id from comments) as com on com.post_id=post.id
                                left join (select id,username,image,provider from user) as author on author.id=post.user_id
                                where (match(title,description,text,tags) against ('*{$text}*' IN BOOLEAN MODE))
                                {$type} GROUP BY post.id order by rel1 desc,rel2 desc, rel3 desc, rel4 desc, rating desc limit 10 offset {$offset}";
                    $res=$db->query($query);
                    while($row=$res->fetch_assoc()){
                        $post=new Post($row);
                        $html="";
                        $html.=Views::Post($post,false,$user_id);
                    }
                }
            }
            echo $html;
            break;
        case 'best_day':
            $query="select * from post where hour(timediff(now(),add_date))<24 order by rating desc limit 10 offset {$offset}";
            $res=Post::LoadPosts('hour(timediff(now(),add_date))<24','rating desc',10,$offset);
            $html="";
            while($row=$res->fetch_assoc()){
                $post=new Post($row);
                $html.=Views::Post($post,false,$user_id);
            }

            echo $html;
            break;

        case 'best_week':
            $query="select * from post where floor(hour(timediff(now(),add_date))/24)<7 order by rating desc limit 10 offset {$offset}";
            $res=Post::LoadPosts('floor(hour(timediff(now(),add_date))/24)<7','rating desc',10,$offset);
            $html="";
            while($row=$res->fetch_assoc()){
                $post=new Post($row);
                $html.=Views::Post($post,false,$user_id);
            }

            echo $html;
            break;


        case 'best_month':
            $query="select * from post where hour(timediff(now(),add_date))*24<31 order by rating desc limit 10 offset {$offset}";
            $res=Post::LoadPosts('floor(hour(timediff(now(),add_date))/24)<31','rating desc',10,$offset);
            $html="";
            while($row=$res->fetch_assoc()){
                $post=new Post($row);
                $html.=Views::Post($post,false,$user_id);
            }

            echo $html;
            break;
        case 'new':
            $query="select * from post  order by add_date desc limit 10 offset {$offset}";
            $res=Post::LoadPosts('','add_date desc',10,$offset);
            $html="";
            while($row=$res->fetch_assoc()){
                $post=new Post($row);
                $html.=Views::Post($post,false,$user_id);
            }

            echo $html;
            break;

        case 'user':
            if(isset($_POST['user_id'])){
                $id=MyDatabase::real_escape_string($_POST['user_id']);
                $query="select * from post  where user_id={$id} order by add_date desc limit 10 offset {$offset}";
                $res=Post::LoadPosts('post.user_id='.$id,'add_date desc',10,$offset);
                $html="";
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    $html.=Views::Post($post,false,$user_id);
                }

                echo $html;

            }
            break;

        case 'subscriptions':
            echo $type;
            if(isset($_SESSION['user']['id'])){
                $res=User::Get_subscriptions($_SESSION['user']['id'],$offset);
                $html="<div id='posts'>";
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    $html.=Views::Post($post,false,$user_id);
                }
                $html.="</div>";
                return $html;
            }
            else{
                return '';
            }
            break;
        case 'saved':
            if(isset($_SESSION['user']['id'])){
                $res=User::Get_saved_posts($_SESSION['user']['id'],$offset);
                $html="";
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    $html.=Views::Post($post,false,$user_id);
                }
                return $html;
            }
            else{
                return '';
            }
            break;
        case "tags":
            $t=Router::getUrlArray();
            $text=MyDatabase::real_escape_string($t[1]);
            $query="select *,hour(timediff(now(),add_date)) as hours,minute(timediff(now(),add_date)) as minutes,
                        datediff(now(),add_date) as days from post where (tags like '%,{$text},%') or (tags like '{$text},%') or (tags like '%,{$text}')
                          ORDER by add_date desc limit 10";
            $res=Post::LoadPosts("(tags like '%,{$text},%') or (tags like '{$text},%') or (tags like '%,{$text}')",'add_date desc',10,$offset);
            $html="";
            while($row=$res->fetch_assoc()){
                $post=new Post($row);
                $html.=Views::Post($post,false,$user_id);
            }
            return $html;
            break;
        case 'feed':
            if(User::isLogin()){
                $res=User::Get_subscriptions($user_id,0);
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    if($post->user_repost_id==0){
                        $html.=Views::Post($post,false,$user_id);
                    }
                    else{
                        $repost_user=new User($post->user_repost_id);
                        $html.=Views::Repost($repost_user,$post,false,$user_id);
                    }
                }
                echo $html;
            }
            break;
        case 'recent_posts':
            if($user_id){
                $res=User::Get_last_subscriptions($user_id,$offset,$time);
                $html="";
                if($res->num_rows>0){
                    while($row=$res->fetch_assoc()){
                        $post=new Post($row);
                        if($post->user_repost_id==0){
                            $html.=Views::Post($post,false,$user_id);
                        }
                        else{
                            $repost_user=new User($post->user_repost_id);
                            $html.=Views::Repost($repost_user,$post,false,$user_id);
                        }
                    }
                }

                echo $html;
            }
            break;
        case 'comments':
            if($user_id){
                $query="select * from comments  where user_id={$user_id} order by add_date desc limit 10";
                $res=MyDatabase::ReadQuery($query);
                $html="";
                while($row=$res->fetch_assoc()){
                    $html.=Views::single_comment($row['id'],$user_id,$row['parent_comment_id'],$row['text'],$row['image_url'],$row['video_url'],$row['post_id']);
                }
                echo $html;
            }
            break;
    }
    exit();
    }
?>