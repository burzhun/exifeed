<?php 
require_once('classes/db.php');
require_once('models/user.php');
require_once('classes/router.php');
require_once('models/post.php');
require_once('models/comment.php');
require_once('functions/sanitize.php');
require_once('functions/other.php');
require_once('Views.php');
session_start(); 
$urltype=Router::urltype();
if(isset($_GET['rating'])&&isset($_POST['post_id'])&&isset($_POST['value'])&&User::isLogin()){
    $db=MyDatabase::getInstance();
    $post_id=$db->real_escape_string($_POST['post_id']);
    $value=$db->real_escape_string($_POST['value']);
    $user_id=$_SESSION['user']['id'];
    $query="select value from users_ratings where user_id={$user_id} and post_id={$post_id}";
    $res=$db->query($query);
    $num=$res->num_rows;
    $res=$res->fetch_row();
    if($num==1&&$res[0]==-1&&$value==1){
        $query="update users_ratings set value=1 where user_id={$user_id} and post_id={$post_id}";
        $db->query($query);
        $query="update post set rating=rating+2,current_rating=current_rating+2 where post_id={$post_id}";
        $db->query($query);
    }
    if($num==1&&$res[0]==1&&$value==-1){
        $query="update users_ratings set value=-1 where user_id={$user_id} and post_id={$post_id}";
        $db->query($query);
        $query="update post set rating=rating-2,current_rating=current_rating-2 where post_id={$post_id}";
        $db->query($query);
    }
    if($num==0){
        $query="insert into users_ratings(user_id,post_id,value) values({$user_id},{$post_id},{$value})";
        $db->query($query);
        $query="update users_ratings set value={$value} where user_id={$user_id} and post_id={$post_id}";
        $db->query($query);
        $query="update post set rating=rating+{$value},current_rating=current_rating+{$value} where post_id={$post_id}";
        $db->query($query);
    }
    exit();
}
if(isset($_GET['rating'])&&isset($_POST['comment_id'])&&isset($_POST['value'])&&User::isLogin()){
    $db=MyDatabase::getInstance();
    $comment_id=$db->real_escape_string($_POST['comment_id']);
    $value=$db->real_escape_string($_POST['value']);
    $user_id=$_SESSION['user']['id'];
    $query="select value from comments_rating where user_id={$user_id} and comment_id={$comment_id}";
    $res=$db->query($query);
    $num=$res->num_rows;
    $res=$res->fetch_row();
    if($num==1&&$res[0]==-1&&$value==1){
        $query="update comments_rating set value=1 where user_id={$user_id} and comment_id={$comment_id}";
        $db->query($query);
        $query="update comments set rating=rating+2 where id={$comment_id}";
        $db->query($query);
    }
    if($num==1&&$res[0]==1&&$value==-1){
        $query="update comments_rating set value=-1 where user_id={$user_id} and comment_id={$comment_id}";
        $db->query($query);
        $query="update comments set rating=rating-2 where id={$comment_id}";
        $db->query($query);
    }
    if($num==0){
        $query="insert into comments_rating(user_id,comment_id,value) values({$user_id},{$comment_id},{$value})";
        $db->query($query);
        $query="update comments_rating set value={$value} where user_id={$user_id} and comment_id={$comment_id}";
        $db->query($query);
        $query="update comments set rating=rating+{$value} where id={$comment_id}";
        $db->query($query);
    }
    echo $num;
    exit();
}
$user_id=null;
if(User::isLogin()){
    $user_id=$_SESSION['user']['id'];
    $time=MyDatabase::GetParameter('user',$user_id,'last_visit_time');
}
if(isset($_GET['delete_post'])&&isset($_POST['post_id'])){
    if(User::isAdmin()){
        Post::delete($_POST['post_id']);
        echo 'done';
    }else{
        echo 'nope';
    }
    exit;
}
if(isset($_POST['offset'])&&(isset($_POST['pagetype']))){
    $db=MyDatabase::getInstance();
    $offset=$db->real_escape_string($_POST['offset']);
    $type=$db->real_escape_string(($_POST['pagetype']));
    switch($type)
    {
        case 'index':
            $query="select * from post ORDER by recent_rating desc limit 10 offset {$offset} ";
            $res=Post::LoadPosts('','recent_rating desc',10,$offset);
            $html="";
            while($row=$res->fetch_assoc()){
                $post=new Post($row);
                $html.=Views::Post($post,false,$user_id);
            }

            echo $html;
            break;
        case 'find':
            $db=MyDatabase::getInstance();
            if(isset($_GET['text'])){
                $text=$db->real_escape_string($_GET['text']);
                if(strlen($text)>2){                    
                    $res=Post::Search($text,$offset,10);
                    while($row=$res->fetch_assoc()){
                        $post=new Post($row);
                        $html="";
                        $html.=Views::Post($post,false,$user_id);
                    }
                }
            }
            echo $html;
            break;
        case 'best_day':
            $query="select * from post where hour(timediff(now(),add_date))<24 order by rating desc limit 10 offset {$offset}";
            $res=Post::LoadPosts('hour(timediff(now(),add_date))<24','rating desc',10,$offset);
            $html="";
            while($row=$res->fetch_assoc()){
                $post=new Post($row);
                $html.=Views::Post($post,false,$user_id);
            }

            echo $html;
            break;

        case 'best_week':
            $query="select * from post where floor(hour(timediff(now(),add_date))/24)<7 order by rating desc limit 10 offset {$offset}";
            $res=Post::LoadPosts('floor(hour(timediff(now(),add_date))/24)<7','rating desc',10,$offset);
            $html="";
            while($row=$res->fetch_assoc()){
                $post=new Post($row);
                $html.=Views::Post($post,false,$user_id);
            }

            echo $html;
            break;


        case 'best_month':
            $query="select * from post where hour(timediff(now(),add_date))*24<31 order by rating desc limit 10 offset {$offset}";
            $res=Post::LoadPosts('floor(hour(timediff(now(),add_date))/24)<31','rating desc',10,$offset);
            $html="";
            while($row=$res->fetch_assoc()){
                $post=new Post($row);
                $html.=Views::Post($post,false,$user_id);
            }

            echo $html;
            break;
        case 'new':
            $query="select * from post  order by add_date desc limit 10 offset {$offset}";
            $res=Post::LoadPosts('','add_date desc',10,$offset);
            $html="";
            while($row=$res->fetch_assoc()){
                $post=new Post($row);
                $html.=Views::Post($post,false,$user_id);
            }

            echo $html;
            break;

        case 'user':
            if(isset($_POST['ajax_user_id'])){
                $id=MyDatabase::real_escape_string($_POST['ajax_user_id']);
                $query="select * from post  where user_id={$id} order by add_date desc limit 10 offset {$offset}";
                $res=Post::LoadPosts('post.user_id='.$id,'add_date desc',10,$offset);
                $html="";
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    $html.=Views::Post($post,false,$user_id);
                }

                echo $html;

            }
            exit;
            break;

        case 'subscriptions':
            echo $type;
            if(isset($_SESSION['user']['id'])){
                $res=User::Get_subscriptions($_SESSION['user']['id'],$offset);
                $html="<div id='posts'>";
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    $html.=Views::Post($post,false,$user_id);
                }
                $html.="</div>";
                return $html;
            }
            else{
                return '';
            }
            break;
        case 'saved':
            if(isset($_SESSION['user']['id'])){
                $res=User::Get_saved_posts($_SESSION['user']['id'],$offset);
                $html="";
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    $html.=Views::Post($post,false,$user_id);
                }
                return $html;
            }
            else{
                return '';
            }
            break;
        case "tags":
            $t=Router::getUrlArray();
            $text=MyDatabase::real_escape_string($t[1]);
            $query="select *,hour(timediff(now(),add_date)) as hours,minute(timediff(now(),add_date)) as minutes,
                        datediff(now(),add_date) as days from post where (tags like '%,{$text},%') or (tags like '{$text},%') or (tags like '%,{$text}')
                          ORDER by add_date desc limit 10";
            $res=Post::LoadPosts("(tags like '%,{$text},%') or (tags like '{$text},%') or (tags like '%,{$text}')",'add_date desc',10,$offset);
            $html="";
            while($row=$res->fetch_assoc()){
                $post=new Post($row);
                $html.=Views::Post($post,false,$user_id);
            }
            return $html;
            break;
        case 'feed':
            if(User::isLogin()){
                $res=User::Get_subscriptions($user_id,0);
                while($row=$res->fetch_assoc()){
                    $post=new Post($row);
                    if($post->user_repost_id==0){
                        $html.=Views::Post($post,false,$user_id);
                    }
                    else{
                        $repost_user=new User($post->user_repost_id);
                        $html.=Views::Repost($repost_user,$post,false,$user_id);
                    }
                }
                echo $html;
            }
            break;
        case 'recent_posts':
            if($user_id){
                $res=User::Get_last_subscriptions($user_id,$offset,$time);
                $html="";
                if($res->num_rows>0){
                    while($row=$res->fetch_assoc()){
                        $post=new Post($row);
                        if($post->user_repost_id==0){
                            $html.=Views::Post($post,false,$user_id);
                        }
                        else{
                            $repost_user=new User($post->user_repost_id);
                            $html.=Views::Repost($repost_user,$post,false,$user_id);
                        }
                    }
                }

                echo $html;
            }
            break;
        case 'comments':
            if($user_id){
                $query="select * from comments  where user_id={$user_id} order by add_date desc limit 10";
                $res=MyDatabase::ReadQuery($query);
                $html="";
                while($row=$res->fetch_assoc()){
                    $html.=Views::single_comment($row['id'],$user_id,$row['parent_comment_id'],$row['text'],$row['image_url'],$row['video_url'],$row['post_id']);
                }
                echo $html;
            }
            break;
    }
    exit();
}
if(isset($_GET['check_user'])&&isset($_POST['email'])&&isset($_POST['password'])){
    $captcha=false;
    if(isset($_SESSION['captcha']['code'])){
        if(isset($_POST['captcha'])&&$_SESSION['captcha']['code']==$_POST['captcha']){
            $_SESSION['login_attempts']=0;
            $captcha=true;            
            unset($_SESSION['captcha']);
        }else{
            $_SESSION['login_attempts']+=1;
            echo 'captcha';
            exit();
        }
    }else{
        if($_SESSION['login_attempts']>5){
            echo 'captcha';
            exit();
        }
        $_SESSION['login_attempts']+=1;
    }
    if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $id=User::checkuser($_POST['email'],$_POST['password'],$captcha);
    }
    else{
        $id=User::checkuser_name($_POST['email'],$_POST['password'],$captcha);          
    }
    if($id){
        User::autorize($id);
        echo 'Ok';
    }else{
        if($_SESSION['login_attempts']>5){
            echo 'captcha';
        }else{
            echo 'no';
        }
    }
    exit();
}
if(isset($_GET['change_pass'])&&isset($_POST['email'])&&isset($_POST['old_password'])&&isset($_POST['new_password'])){
    if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        if(User::checkuser($_POST['email'],$_POST['old_password'])){
            $id=$_SESSION['user']['id'];
            User::change_password($id,$_POST['new_password']);
            echo 'Changed';
        }
        else{
            echo 'No';
        }
    }
    exit();
}
if($urltype=='settings'){

}
if(isset($_GET['save_post'])&&isset($_POST['post_id'])){
    $id=$_SESSION['user']['id'];
    $db=MyDatabase::getInstance();
    $post_id=$db->real_escape_string($_POST['post_id']);
    $query="insert into saved_posts(post_id,user_id) values({$post_id},{$id})";
    $db->query($query);
    exit();
}
if(isset($_GET['user_exists'])&&isset($_POST['email'])&&isset($_POST['name'])&&isset($_POST['password'])){
    $db=MyDatabase::getInstance();
    $email=$db->real_escape_string($_POST['email']);
    $name=$db->real_escape_string($_POST['name']);
    $password=$db->real_escape_string($_POST['password']);
    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        exit('Incorrect email');
    }
    if(preg_match("/[^0-9a-zA-Z]+/i",$password)||strlen($password)<5){
        exit('Incorrect password');
    }
    if(User::username_used($name)){
        exit('This username is already taken');
    }
    if(!User::user_exists($email)){
        echo 'Ok';
    }
    else{
        echo 'This email is already in use';
    }
    exit();
}
if(isset($_GET['name_used'])&&isset($_POST['name'])){
    $db=MyDatabase::getInstance();
    $name=$db->real_escape_string($_POST['name']);
    if(preg_match("/[^0-9a-zA-Z_]+/",$_POST['name'])){
        echo 'Username can only contain numbers,digits,_';
        exit;
    }
    if(!User::username_used($name)){
        echo 'Ok';
    }
    else{
        echo 'This username is already taken';
    }
    exit();
}
if(isset($_GET['user_subscribe'])&&isset($_POST['source_id'])&&isset($_POST['value'])&&User::isLogin()){
    $source_id=(int)$_POST['source_id'];
    $value=$_POST['value'];
    $id=$_SESSION['user']['id'];
    if(preg_match("/[0-9]+/",$source_id)&&($value==-1||$value==1)){
        if($value==1){
            MyDatabase::UpdateQuery("insert into subscribes(subscriber_id,source_id) values({$id},{$source_id})");
        }
        else{
            MyDatabase::UpdateQuery("delete from subscribes where subscriber_id={$id} and source_id={$source_id}");
        }
        echo 'Ok';
    }
    exit();
}
if(User::isLogin() and isset($_POST['comment_text']) and isset($_POST['post_id'])){
    require_once('functions/image_upload.php');
    $db=MyDatabase::getInstance();
    $user=new User($_SESSION['user']['id']);
    $text=str_replace("\n",'<br>',htmlentities($_POST['comment_text']));
    $text=$db->real_escape_string(url_link_comment($text));
    
    $post_id=$_POST['post_id'];    
    $id='';
    $id=Comment::add_comment($text,'','',$user->id,$user->username,$user->image,$post_id,0);
    $ext=null;
    $url='';
    if($_POST['image_source_type']=='upload'&&isset($_FILES['comment_image']))
    {
        $image=$_FILES['comment_image'];
        switch(exif_imagetype($image['tmp_name']))
        {
            case IMAGETYPE_JPEG: $ext = 'jpg'; break;
            case IMAGETYPE_GIF: $ext = 'gif'; break;
            case IMAGETYPE_PNG: $ext = 'jpg'; break;
        }
        if($ext)
        {
            $url=$id.'.'.$ext;
            compress($image['tmp_name'],'images/comments_images/'.$url,90);
            if($ext=='gif'){
                $cmd='ffmpeg -i images/comments_images/'.$url.' -movflags faststart -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" videos/comments/'.$id.'.mp4';    
                exec(escapeshellcmd($cmd));                
                $video_url = 'http://www.exifeed.com/videos/comments/'.$id.'.mp4';  
                MyDatabase::UpdateQuery("update comments set video_url='{$video_url}' where id={$id}");            
                /*$size=filesize('images/comments_images/'.$url);  
                if($size<1024*1024*10){
                    require_once('includes/cloudinary/Cloudinary.php');
                    require_once('includes/cloudinary/Uploader.php');
                    require_once('includes/cloudinary/Api.php');
                    \Cloudinary::config(array( 
                      "cloud_name" => "dwidcgtvq", 
                      "api_key" => "491581117382525", 
                      "api_secret" => "-PvrxVcysPCOpcr0dr2856N8CXg" 
                    ));
                    $url1="http://exifeed.com/images/comments_images/" . $url;
                    $f=\Cloudinary\Uploader::upload($url1,array("format"=>"mp4"));
                    $video_url=$f['url'];
                    MyDatabase::UpdateQuery("update comments set video_url='{$video_url}' where id={$id}");
                }else{
                    MyDatabase::UpdateQuery("update comments set video_url='' where id={$id}");
                }*/
                 MyDatabase::UpdateQuery("update comments set image_url='/images/comments_images/{$url}.jpg' where id={$id}");
            }else{
                 MyDatabase::UpdateQuery("update comments set image_url='/images/comments_images/{$url}' where id={$id}");
            }
        }
    }
    if($_POST['image_source_type']=='giphy' && $_POST['image_url']!='' && $_POST['video_url']!=''){
        $image_url=MyDatabase::real_escape_string($_POST['image_url']);
        $video_url=MyDatabase::real_escape_string($_POST['video_url']);
        if(strpos($_POST['image_url'],'giphy.com')>1&&preg_match('/http\:\/\/media[0-9]+\.giphy\.com\/media\/[0-9a-zA-Z]+\/giphy_s\.gif/',$image_url)&&preg_match('/http\:\/\/media[0-9]+\.giphy\.com\/media\/[0-9a-zA-Z]+\/giphy\.mp4/',$video_url)){
            MyDatabase::UpdateQuery("update comments set image_url='{$image_url}',video_url='{$video_url}' where id={$id}");
        }
        
    }
    
    
    if($id){
      $comment=new Comment($id);
      $html=Views::comment_ajax($comment,$post_id,$user->id,null);
      echo $html;
    }        
    exit();
}
if(User::isLogin() and isset($_POST['subcomment_text']) and isset($_POST['post_id']) and isset($_POST['comment_id'])){
    require_once('functions/image_upload.php');
    $post_id=$_POST['post_id'];
    $user=new User($_SESSION['user']['id']);
    $db=MyDatabase::getInstance();
    $text=url_link_comment(htmlentities($_POST['subcomment_text']));
    $text=str_replace(htmlentities('<span class="quote">'), "<span class='quote'>", $text);
    $text=str_replace(htmlentities("</span>"), "</span>", $text);
    $text=str_replace(htmlentities("<br>"), "<br>", $text);
    $text=$db->real_escape_string($text);
    $id='';
    $parent_comment_id=$db->real_escape_string($_POST['comment_id']);
    $id=Comment::add_comment($text,'','',$user->id,$user->username,$user->image,$post_id,$parent_comment_id);
    $ext=null;
    $url='';
    if($_POST['image_source_type']=='upload'&&isset($_FILES['subcomment_image']))
    {
        $image=$_FILES['subcomment_image'];
        switch(exif_imagetype($image['tmp_name']))
        {
            case IMAGETYPE_JPEG: $ext = 'jpg'; break;
            case IMAGETYPE_GIF: $ext = 'gif'; break;
            case IMAGETYPE_PNG: $ext = 'jpg'; break;
        }
        if($ext)
        {
            $url=$id.'.'.$ext;
             compress($image['tmp_name'],'images/comments_images/'.$url,75);
             if($ext=='gif'){
             $cmd='ffmpeg -i images/comments_images/'.$url.' -movflags faststart -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" videos/comments/'.$id.'.mp4';    
                exec(escapeshellcmd($cmd));                
                $video_url = 'http://www.exifeed.com/videos/comments/'.$id.'.mp4';  
                MyDatabase::UpdateQuery("update comments set video_url='{$video_url}' where id={$id}");                 
                /*require_once('includes/cloudinary/Cloudinary.php');
                require_once('includes/cloudinary/Uploader.php');
                require_once('includes/cloudinary/Api.php');
                \Cloudinary::config(array( 
                  "cloud_name" => "dwidcgtvq", 
                  "api_key" => "491581117382525", 
                  "api_secret" => "-PvrxVcysPCOpcr0dr2856N8CXg" 
                ));
                $url1="http://exifeed.com/images/comments_images/" . $url;
                $f=\Cloudinary\Uploader::upload($url1,array("format"=>"mp4"));
                $video_url=$f['url'];*/
                MyDatabase::UpdateQuery("update comments set image_url='/images/comments_images/{$url}.jpg' where id={$id}");
            }else{
                  MyDatabase::UpdateQuery("update comments set image_url='/images/comments_images/{$url}' where id={$id}");
             }
        }
    }
    if($_POST['image_source_type']=='giphy' && $_POST['image_url']!='' && $_POST['video_url']!=''){
        $image_url=MyDatabase::real_escape_string($_POST['image_url']);
        $video_url=MyDatabase::real_escape_string($_POST['video_url']);
        if(strpos($_POST['image_url'],'giphy.com')>1&&preg_match('/http\:\/\/media[0-9]+\.giphy\.com\/media\/[0-9a-zA-Z]+\/giphy_s\.gif/',$image_url)&&preg_match('/http\:\/\/media[0-9]+\.giphy\.com\/media\/[0-9a-zA-Z]+\/giphy\.mp4/',$video_url)){       
            MyDatabase::UpdateQuery("update comments set image_url='{$image_url}',video_url='{$video_url}' where id={$id}");
        }
        
    }
    $comment=new Comment($id);
    $html=Views::comment_ajax($comment,$post_id,$user->id,$parent_comment_id);
    echo $html;
    exit();
}
if(isset($_GET['delete_comment'])&&isset($_POST['delete_comment_id'])){
    //echo '13';exit;
    if(User::isAdmin()){
        Comment::delete($_POST['delete_comment_id']);
    }
    echo 'ok';
}
if(isset($_POST['subscribe_tag'])&&User::isLogin()){
    $user=new User($_SESSION['user']['id']);
    $user->Add_subscribe_tag($_POST['subscribe_tag']);
    exit();
}
if(isset($_POST['unsubscribe_tag'])&&User::isLogin()){
    $user=new User($_SESSION['user']['id']);
    $user->Delete_subscribe_tag($_POST['unsubscribe_tag']);
    exit();
}
if(isset($_GET['repost'])&&isset($_POST['post_id'])){
    if($user_id){
        $post_id=MyDatabase::real_escape_string($_POST['post_id']);
        echo $user_id;
        User::Repost($user_id,$post_id);
        Post::updateRating($post_id,1);
    }

    exit();
}
if(isset($_GET['ajax_post_comments'])&&isset($_POST['time'])&&isset($_POST['post_id'])){
    $time=$_POST['time'];
    $post_id=$_POST['post_id'];
    $where="";
    if(User::isLogin()){
        $user_id=$_SESSION['user']['id'];
        $where="and user_id<>{$user_id}";
    }
    $query="select * from comments where post_id={$post_id} and add_date>'{$time}' {$where} order by id";
    $comments=array();
    $res=MyDatabase::ReadQuery($query);

    if($res->num_rows==0){
        echo "No comments";
    }
    else{
        if(isMobile()){
            while($row=$res->fetch_assoc()){
                $com=new Comment(1,$row);
                $p_com=null;
                if($com->parent_comment_id){
                    $p_com=Comment::SingleComment($com->parent_comment_id);
                }
                $parent_comment_id=$com->parent_comment_id;
                $comment_html=Views::comment_ajax($com,$post_id,'',$p_com);
                $comments[]=array('html'=>$comment_html,'parent_comment_id'=>$parent_comment_id);
            }
        }else{
             while($row=$res->fetch_assoc()){
                $com=new Comment(1,$row);
                $comment_html=Views::comment_ajax($com,$post_id,'',null);
                $parent_comment_id=$com->parent_comment_id;
                $comments[]=array('html'=>$comment_html,'parent_comment_id'=>$parent_comment_id);
            }                    
        }
        $time=date('Y-m-d H:i:s');    
        echo '{"array":'.json_encode($comments).',"time":"'.$time.'"}';
       
    }
    exit;
}
if(isset($_GET['ajax_pc_list_comments'])&&isset($_POST['time'])&&User::isLogin()){
    $time=str_replace('+', ' ', $_POST['time']);

    $where="";
    $user_id=$_SESSION['user']['id'];
    $where=" and exists(select id from comments as t2
        where t2.id=comments.parent_comment_id and t2.user_id={$user_id})";
    $query="select * from comments where  1=1 {$where} and add_date>'{$time}'   order by id";
    $comments=array();
    $res=MyDatabase::ReadQuery($query);
    if($res->num_rows==0){
        echo "No comments";
    }
    else{
        while($row=$res->fetch_assoc()){
            $com=new Comment(1,$row);
            $comment_html=Views::unresponded_comments($com,$post_id,'');
            $parent_comment_id=$com->parent_comment_id;

            $comments[]=array('html'=>$comment_html,'parent_comment_id'=>$parent_comment_id);
        }
        $time=date('Y-m-d H:i:s');
        echo '{"array":'.json_encode($comments).',"time":"'.$time.'"}';
    }
    exit;
}
if(isset($_GET['ajax_pc_new_comments'])&&isset($_POST['time'])){
    $time=str_replace('+', ' ', $_POST['time']);
    $query="select * from comments where add_date>'{$time}' order by id";
    $comments=array();
    $res=MyDatabase::ReadQuery($query);
    if($res->num_rows==0){
        echo "No comments";
    }
    else{
        while($row=$res->fetch_assoc()){
            $com=new Comment(1,$row);
            $comment_html=Views::unresponded_comments($com,$post_id,'');
            $parent_comment_id=$com->parent_comment_id;

            $comments[]=array('html'=>$comment_html,'parent_comment_id'=>$parent_comment_id);
        }
        $time=date('Y-m-d H:i:s');
        echo '{"array":'.json_encode($comments).',"time":"'.$time.'"}';
    }
    exit;
}
if(isset($_POST['user_idea_id'])){
    require_once('models/UserIdeas.php');
    $id=(int)$_POST['user_idea_id'];
    UserIdeas::UpvoteIdea($id);
    exit;
}
if(isset($_POST['useridea'])){

    if(User::isLogin()){
        require_once('models/UserIdeas.php');
        $user_id=User::id();
        $idea=MyDatabase::real_escape_string($_POST['useridea']);
        if($idea!=''){
            $id=UserIdeas::AddIdea($user_id,$idea);
            $idea=UserIdeas::LoadById($id);
            $html='<div class="user_idea">
            <span>'.htmlentities($idea->idea).'</span>
            <button class="idea_upvote"></button>
        </div>';
            echo $html;
        }

    }
    exit;

}
if(isset($_GET['ajax_comments_div'])&&isset($_POST['post_id'])){
    $user_id=User::id();
    $post_id=(int)MyDatabase::real_escape_string($_POST['post_id']);
    $post_author=MyDatabase::GetParameter('post',$post_id,'user_id');
    $html="";
    $html=Views::Comments($post_id,$post_author);
    $html.=Views::Add_comment_div($user_id,$post_id);
    if($user_id){
        require_once('models/post_views.php');
        Post_views::Add_view($id,$user_id);
    }
    $html.="<style>#comments{margin-left:10px;}#add_comment_div{margin-left:10px;margin-bottom:20px;}</style>";
    echo $html;
}
if(isset($_GET['suggestion'])&&isset($_POST['suggestion'])){
    $text=MyDatabase::real_escape_string($_POST['suggestion']);
    if(User::isLogin()){
        $id=User::id();
        $query="insert into suggestions(user_id,text) values({$id},'{$text}')";
    }else{
        $query="insert into suggestions(user_id,text) values(0,'{$text}')";
    }
    MyDatabase::UpdateQuery($query);
    echo 'ok';
}
if(isset($_GET['other_posts'])&&isset($_POST['post_id'])&&isset($_POST['offset'])){
  $offset=(int)$_POST['offset'];
  $user_id=User::id();
  $res=Post::LoadPosts('hour(timediff(now(),add_date))<150','recent_rating*100+rand() desc, post_views desc, id desc',10,$offset);
  $html="";
  if(User::isFirstTime()){
        $first_post=new Post(488);
        $html.=Views::Post($first_post,false,$user_id);
    }  
  while($row=$res->fetch_assoc()){
      $post=new Post($row);
      $html.=Views::Post($post,false,$user_id);
  }
  echo $html;
    exit;
}
if(isset($_GET['ajax_new_comments'])&&isset($_POST['index'])&&isset($_POST['offset'])){
  if($_POST['index'] == 0){
    echo Comment::Last_comments($_POST['offset']);
  }else{
    echo Comment::Last_comments_responds(User::id(),$_POST['offset']);
  }
}
if(isset($_GET['youtube_name'])&&isset($_GET['youtube_id'])){
   echo getYoutubeName($_GET['youtube_id']);
   exit;
}
if(isset($_GET['get_coub_info'])&&isset($_GET['coub_id'])){
    $json=json_decode(file_get_contents("http://coub.com/api/v2/coubs/".$_GET['coub_id']),true);
    echo $json["picture"];
    exit;
}
if(isset($_GET['get_coub_info'])&&isset($_POST['coub_id'])){
    echo file_get_contents("http://coub.com/api/v2/coubs/".$_POST['coub_id']);
    exit;
}