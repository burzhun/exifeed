CKEDITOR.plugins.add( 'image2', {
    icons: 'image2',
    init: function( editor ) {
        //Plugin logic goes here.
        editor.addCommand( 'insertImage2', {
            exec: function( editor ) {
                $("#add_image_button").trigger('click');
            }
        });
        editor.ui.addButton( 'Image2', {
            label: 'Insert image',
            command: 'insertImage2',
            toolbar: 'insert'
        });
    }
});