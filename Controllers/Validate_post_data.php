<?php
require_once(dirname(__FILE__).'/../functions/image_upload.php');
require_once(dirname(__FILE__).'/../functions/square_image.php');
require_once(dirname(__FILE__).'/../classes/router.php');
require_once(dirname(__FILE__) . '/../models/user.php');
function Validate_post_data($routing){
    $path=Router::pathtofolder();
    if(isset($_POST['email12'])&&isset($_POST['password'])&&(!isset($_SESSION['user']['id'])||$_SESSION['user']['id']=='')){
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            $id=User::checkuser($_POST['email'],$_POST['password']);
            if($id){
                User::autorize($id);
                header('Location:'.$_SESSION['previous_page']);
            }
        }
        else{
            $id=User::checkuser_name($_POST['email'],$_POST['password']);
            if($id){
                User::autorize($id);
                header('Location:'.$_SESSION['previous_page']);
            }
        }
    }
    if(isset($_POST['email'])&&isset($_POST['name'])&&isset($_POST['password']))
    {
        if(!User::user_exists($_POST['email']))
        {
            $id=User::AddUser($_POST['email'],$_POST['name'],$_POST['name'],$_POST['password']);
            if($id!=0) {
                $_SESSION['user']['id'] = $id;
            }
            header('Location:index.php');
        }
        else{
            $_SESSION["registration_confirmed"]="0";
        }
    }
    switch($routing)
    {
        case 'post':

            break;


        case 'settings':
            $id=$_SESSION['user']['id'];
            if(isset($_FILES['user_image'])){
                $image=$_FILES['user_image'];
                $user=new User($id);
                $ext="";
                switch(exif_imagetype($image['tmp_name']))
                {
                    case IMAGETYPE_JPEG: $ext = 'jpg'; break;
                    case IMAGETYPE_GIF: $ext = 'gif'; break;
                    case IMAGETYPE_PNG: $ext = 'jpg'; break;
                }
                if($ext=='jpg'){
                    if($user->image==''){
                        $url=$id.'.jpg';
                        square_image($image['tmp_name'],'images/user_images/'.$url);
                        $query="update user set image='{$url}' where id={$id}";
                        MyDatabase::UpdateQuery($query);

                    }
                    else{
                        @unlink('/images/user_images/'.$id.'.jpg');
                        $url=$id.'.'.$ext;
                        square_image($image['tmp_name'],'images/user_images/'.$url);
                    }
                }
            }
            break;
    }


}

?>