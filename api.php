<?php
session_start();
require_once('models/user.php');
require_once('models/post.php');
require_once('classes/db.php');
require_once('functions/other.php');
require_once('classes/router.php');
require_once('classes/ApiController.php');
$list=[];
//$cmd='ffmpeg -i videos/post/1023/result_repeated_1.mp4  -movflags faststart -acodec copy -vcodec copy videos/post/1023/result_repeated_2.mp4'; 
  //  exec(escapeshellcmd($cmd));	
//https://realm.io/news/360andev-effie-barak-switching-exoplayer-better-video-android/
//https://www.nytimes.com/2016/12/14/magazine/the-great-ai-awakening.html?_r=1
// $id=1025; 
// $post = new Post($id);
// 		$post->android_text=MyDatabase::real_escape_string(json_encode($post->getJsonArray()));
// 		MyDatabase::SetParameter('post',$id,'android_text',$post->android_text,false);
$api_key="sdsgre345345hkb5kh345kj345";
$posts=false;
$routing=Router::getUrlArray();
//print_r($routing);exit;
if(isset($_GET['api_key'])&&$_GET['api_key']==$api_key){
    if(isset($_POST['logout'])){
        $token=str_replace('/[^0-9a-z]/','',$_POST['token']);
        $db=MyDatabase::getInstance(true);        
        $query = "delete from user_token where token='{$token}'";
        $db->query($query);
        unset($_POST['token']);
        exit;
    } 
    Default1();
    if(isset($routing[0])){
        switch($routing[0]){
            case 'login':
                Login();
                exit;
                break;
            case 'posts':
                GetPostsList();exit;
                break;
            case 'post':
                if(isset($_POST['post_id'])){
                    GetPost($_POST['post_id']);
                    exit;
                }
                break;
            case 'all_post':
                GetAllTypesPostsList();
                break;
            case 'comment':
                if(isset($routing[1])){
                     GetComment($routing[1]);
                }
                break;
            case 'comments':
                if(isset($_POST['post_id'])){
                    $offset = isset($_POST['offset']) ? (int)$_POST['offset'] : 0;
                    GetCommentsList($_POST['post_id'],$offset);
                }
                break;
            case 'user':
                if(isset($routing[1])){
                    GetUserInfo($routing[1]);
                }
                break;
            case 'post_user':
                if(isset($routing[1])){
                    GetUserPosts($routing[1]);
                }
                break;
            case 'mycomments':
                if(isset($_POST['token'])){
                    GetMyComments();
                }
                break;
            case 'settings':
                if(isset($_POST['token'])){
                    SaveSettings();
                }
                break;
            case 'save_post':
                if(isset($_POST['post_id'])&&isset($_POST['token'])){
                  SavePost();
                }
                break;
            case 'comment_respond':
                $image=null;
                if(!isset($_POST['token'])) exit;
                if(isset($_POST["Image"])){
                    $image = base64_decode($_POST["Image"]);
                }
                Add_comment($_POST['token'],$_POST['post_id'],$_POST['comment_id'],$_POST['Text'],$image);
                echo 'success';
                break;
            case 'giphy_search':
                if(isset($_POST['query'])){
                    SearchGiphy($_POST['query']);
                }
                break;
            case 'all_comments':
                if(isset($_POST['token'])){
                    GetAllComments();
                }
                break;   
            case 'add_facebook_user':
                if(isset($_POST['facebook_id'])){
                    $username=MyDatabase::real_escape_string($_POST['username']);
                    $email=MyDatabase::real_escape_string($_POST['email']);
                    $image=MyDatabase::real_escape_string($_POST['image']);
                    $facebook_id=MyDatabase::real_escape_string($_POST['facebook_id']);
                    AddFacebookUser($facebook_id,$username,$image,$email);
                }
                break; 
            case 'addpost':
                if(isset($_POST['title'])&&isset($_POST['post'])&&isset($_POST['token'])){
                    AddPost();
                }
                break;
            case 'post_rating':
                if(isset($_POST['rating'])){
                    PostRating();
                }
                break;
            case 'subscribe':
                if(isset($_POST['user_id'])&&isset($_POST['token'])){
                  $user_id=(int)$_POST['user_id'];
                  if(isset($_POST['unsubscribe'])){
                    Subscribe($user_id,true);
                  }else{
                    Subscribe($user_id);
                  }
                  echo 'ok';
                }
                break;
            case 'userimages':
                GetUserImages();
                break;
            case 'about':
                if(isset($_POST['text'])){
                    $text=MyDatabase::real_escape_string($_POST['text']);
                    $user = User::FindByToken($_POST['token']);
                    if($user){
                        $query="insert into suggestions(user_id,text) values({$user->id},'{$text}')";
                    }else{
                        $query="insert into suggestions(user_id,text) values(0,'{$text}')";
                    }
                    MyDatabase::UpdateQuery($query);
                    echo 'ok';
                }
                break;
            case 'registration':
                $email=MyDatabase::real_escape_string($_POST['email']);
                $username=MyDatabase::real_escape_string($_POST['username']);
                $password=MyDatabase::real_escape_string($_POST['password']);
                if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                    echo "{text:'Incorrect email',token:''}";
                    exit;
                }
                if(strlen($password)<5){
                     echo "{text:'Your password should contain at least 5 characters',token:''}";
                     exit;
                }
                if(preg_match("/[^0-9a-zA-Z]+/i",$password)){
                     echo "{text:'Your password can contain only letters,number and _',token:''}";
                     exit;
                }
                if(preg_match("/[^0-9a-zA-Z]+/i",$username)){
                     echo "{text:'Your username can contain only letters,number and _',token:''}";
                     exit;
                }
                if(strlen($username)<3||strlen($username)>15){
                  echo "{text:'Username may contain form 3 to 25 characters',token:''}";
                  exit;
                }
                if(User::user_exists($_POST['email'])){
                  echo "{text:'This email is already used',token:''}";
                  exit;
                }
                if(User::username_used($_POST['username'])){
                  echo "{text:'This username is already used',token:''}";
                  exit;
                }
                RegisterUser($username,$email,$password);
                break;
            case 'new_comments_count':
                $user = User::FindByToken($_POST['token']);
                if($user){
                  echo "{result:{$user->UnreadCommentsCount()}}";
                  exit;
                }
                break;
        }             
      
        if($routing[0]=="test"){
           // print_r($_POST);
            file_put_contents('1.mp4',base64_decode($_POST["video"]));
            echo 'test';
            exit;
        }
    }
    
}

